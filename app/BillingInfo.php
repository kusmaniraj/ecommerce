<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingInfo extends Model
{
    protected $table='billing_infos';
    protected $fillable=['id','userId','firstName','lastName','userEmail','address','region','city','telephone','fax','gender'];
    public function shippingInfos(){
        return $this->hasMany('App\ShippingInfo','userEmail','userEmail');
    }
}
