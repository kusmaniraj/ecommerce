<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categories';
    protected $fillable=['name','id','parent_category_id','sub_category_id','second_sub_category_id','featuredImg','status'];
    public function parentCategories(){
        return $this->hasMany('App\Category','parent_category_id','id');
    }
    public function category(){
        return $this->belongsTo('App\Category','parent_category_id','id');
    }

    public function parentCategory(){
        return $this->belongsTo('App\Category','sub_category_id','id');
    }
    public function subCategories(){
        return $this->hasMany('App\Category','sub_category_id','id');
    }
    public function secondSubCategories(){
        return $this->hasMany('App\Category','second_sub_category_id','id');
    }


    public function category_products(){
        return $this->hasMany('App\Product','category_id','id');
    }
    public function parent_category_products(){
        return $this->hasMany('App\Product','parent_category_id','id');
    }
    public function sub_category_products(){
        return $this->hasMany('App\Product','sub_category_id','id');
    }
    public function second_sub_category_products(){
        return $this->hasMany('App\Product','second_sub_category_id','id');
    }

}