<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table='galleries';
    protected $fillable=['id','product_id','original_image_url','thumb_image_url','status','product_variation_id'];
}
