<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryType;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Auth;

class CategoryController extends Controller
{
    private $data, $admin;
    use shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title'] = 'Category';
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }
        $this->data['parent_category_id'] = "";
        $this->data['sub_category_id'] = "";
        $this->data['second_sub_category_id'] = "";
        $this->data['category_id'] = "";
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.category.list', $this->data);

    }
    public function getParentIndex($category_id){
        $this->data['category_id'] = $category_id;
        $category=Category::find($category_id);
        $this->data['categoryName']=$category->name;
        return view('admin.category.list', $this->data);
    }
    public function getSubIndex($category_id,$parent_category_id){
        $this->data['category_id'] = $category_id;
        $this->data['parent_category_id'] = $parent_category_id;
        $category=Category::find($parent_category_id);
        $this->data['categoryName']=$category->name;
        return view('admin.category.list', $this->data);
    }
    public function getSecondSubIndex($category_id,$parent_category_id,$sub_category_id){
        $this->data['category_id'] = $category_id;
        $this->data['parent_category_id'] = $parent_category_id;
        $this->data['sub_category_id'] = $sub_category_id;
        $category=Category::find($sub_category_id);
        $this->data['categoryName']=$category->name;
        return view('admin.category.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCategories()
    {
        return Category::where(['parent_category_id' => NULL, 'sub_category_id' => NULL, 'second_sub_category_id' => NULL])->with(['parentCategories'=>function($query){
            $query->where(['sub_category_id' => NULL, 'second_sub_category_id' => NULL]);
        }])->get();


    }
    public function getParentCategories($category_id)
    {   return Category::where(['parent_category_id' => $category_id])->with(['subCategories'=>function($query){
    $query->where(['second_sub_category_id' => NULL]);}])->where(['sub_category_id' => NULL, 'second_sub_category_id' => NULL])->get();

    }
    public function getSubCategories($category_id,$parent_category_id)
    {   return Category::where(['parent_category_id' => $category_id, 'sub_category_id' => $parent_category_id])->with('secondSubCategories')->where(['second_sub_category_id' => NULL])->get();

    }
    public function getSecondSubCategories($category_id,$parent_category_id,$sub_category_id)
    {   return Category::where(['parent_category_id' => $category_id, 'sub_category_id' => $parent_category_id,'second_sub_category_id' => $sub_category_id])->get();

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $result = Category::create($request->all());
        if ($result) {
            return ['success' => ' Create Category'];
        } else {
            return ['error' => 'Cannot Create Category'];
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Category::find($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $result = Category::find($id)->update($request->all());
        if ($result) {
            return ['success' => ' Update Category'];
        } else {
            return ['error' => 'Cannot Update Category'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = Category::where('id', $id)->delete();
        if ($result) {
            return ['success' => ' Delete Category'];
        } else {
            return ['error' => 'Cannot Delete Category'];
        }
    }


}
