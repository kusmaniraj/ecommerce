<?php

namespace App\Http\Controllers\Admin;


use App\ColorType;
use App\Http\Requests\ColorRequest;
use App\Product;
use App\ProductColor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Auth;

class ColorController extends Controller
{
    private $data, $admin;
    use shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['title'] = 'Color';
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.productAttribute.color.list', $this->data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getColors()
    {
        return ProductColor::all();


    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ColorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ColorRequest $request)
    {
        $result = ProductColor::create($request->all());
        if ($result) {
            return ['success' => ' Created Color'];
        } else {
            return ['error' => 'Cannot Create Color'];
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return ProductColor::find($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ColorRequest $request, $id)
    {
        $result = ProductColor::find($id)->update($request->all());
        if ($result) {
            return ['success' => ' Updated Color'];
        } else {
            return ['error' => 'Cannot Update Color'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productColor=Product::all();
        foreach($productColor as $pcolor){
            $colorArray=explode(',',$pcolor->color);
            if(in_array($id, $colorArray)){
                return ['error' => 'Oops !! Selected Color is used in Product,Cannot Delete'];
            }



        }





        $result = ProductColor::where('id', $id)->delete();
        if ($result) {
            return ['success' => ' Deleted Color'];
        } else {
            return ['error' => 'Cannot Delete Color'];
        }
    }


}
