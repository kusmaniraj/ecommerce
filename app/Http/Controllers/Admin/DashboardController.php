<?php

namespace App\Http\Controllers\Admin;

use App\BookingCourt;
use App\Time_schedule_court;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\SettingController;
use App\Traits\Shared;

class DashboardController extends Controller
{
    use Shared;


    private $data;

    public function __construct()
    {
//      Traits
//        setting
        $this->data['getSetting'] = $this->getSetting();
//booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();


//        end Traits

        $this->data['title'] = 'Dashboard';
    }

    public function index()
    {


        return view('admin.dashboard', $this->data);
    }
    
}
