<?php

namespace App\Http\Controllers\Admin;

use App\BookingCourt;
use App\Time_schedule_court;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\SettingController;
use App\Traits\Shared;
use Auth;

class FileManagerController extends Controller
{
    use Shared;


    private $data;
    protected $admin;

    public function __construct()
    {


//      Traits
//        setting
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();

//        end Traits
        $this->admin = Auth::user();
        if($this->admin['type']=='0'){
            return redirect()->back();
        }
        $this->data['title'] = 'FileManager';
    }

    public function index()
    {


        return view('admin.fileManager', $this->data);
    }
    
}
