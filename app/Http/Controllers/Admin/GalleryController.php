<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use File;
use Image;
use App\Traits\Shared;

class GalleryController extends Controller
{
    use Shared;
    public  function storeProductVariationGallery(Request $request){
        $request->validate([
            'images' => 'required',

        ]);
        $result=false;
        $formData=$request->all();
        for($i=0;$i<count($request->file('images'));$i++){
            $formData['original_image_url']=$this->uploadImage($request->file('images')[$i]);
            $formData['thumb_image_url']=$this->getThumb($formData['original_image_url']);
            Gallery::create($formData);
            $result=true;
        }




        if($result){
            return ['success'=>' Upload ProductVariation  Gallery'];
        }else{
            return ['success'=>'Cannot Upload ProductVariation  Gallery'];
        }

    }
    public function getProductVariationGallery($id){
        return Gallery::where('product_variation_id',$id)->get();
    }
    public function removeProductVariationGallery($id){
        $galleryDetail=Gallery::find($id);
        $original_path=public_path($galleryDetail['original_image_url']);
        $thumb_path=public_path($galleryDetail['thumb_image_url']);


        if ($galleryDetail->delete()) {
            if(file_exists($original_path)){
                @unlink($original_path);

            }
            if(file_exists($thumb_path)){
                @unlink($thumb_path);

            }
            return ['success'=>' Delete Selected  Gallery'];


        } else {
            return ['error'=>'Cannot Selected Gallery'];
        }


    }
    public function uploadImage($image)
    {
        $path=public_path('files/shares/Gallery/ProductVariations/');
        if (!file_exists($path)) {
             File::makeDirectory($path, 0775, true);

        }
        if (!file_exists($path.'/thumbs')) {

            File::makeDirectory($path.'/thumbs', 0775, true);
        }
        $filename =time().'.'.$image->getClientOriginalName();

        Image::make($image)->resize(300, 300)->save('files/shares/Gallery/ProductVariations/thumbs/'.$filename);
        Image::make($image)->save('files/shares/Gallery/ProductVariations/'.$filename);
        return ('files/shares/Gallery/ProductVariations/'.$filename);

    }
}
