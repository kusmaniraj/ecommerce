<?php

namespace App\Http\Controllers\Admin;

use App\Image_IconSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Traits\Shared;

class Image_IconSettingController extends Controller
{
    protected $data;
    use Shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['title'] = 'Image|Icon Setting';
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }


    }


    public function index()
    {

        $this->data['logoImage']=Image_IconSetting::where('image_icon_type','logo_image')->first();
        $this->data['favicon']=Image_IconSetting::where('image_icon_type','favicon')->first();
        $this->data['noImage']=Image_IconSetting::where('image_icon_type','no_image')->first();
        return view('admin.setting.imageIconSetting', $this->data);

    }


    public function store(Request $request)
    {
       $formInput=$request->all();
        $request->validate(['original_image_url'=>'required'],['original_image_url.required'=>'*Please Choose Image*']);
//        getthumb  from Trait Shared
        $formInput['thumb_image_url']=$this->getThumb($formInput['original_image_url']);
        $result = Image_IconSetting::create($formInput);
        if ($result) {
            return ['success' => ' Uploaded '.$formInput['image_icon_type']];
        } else {
            return ['error' => 'Cannot Upload' .$formInput['image_icon_type']];
        }

    }



    public function update(Request $request, $id)
    {
        $formInput=$request->all();
        $imageIcon=Image_IconSetting::find($id);
        $request->validate(['original_image_url'=>'required']);
//        getthumb  from Trait Shared
        $formInput['thumb_image_url']=$this->getThumb($formInput['original_image_url']);
        $result = Image_IconSetting::find($id)->update($formInput);
        if ($result) {
            return ['success' => ' Uploaded '.$imageIcon->image_icon_type];
        } else {
            return ['error' => 'Cannot Upload '.$imageIcon->image_icon_type];
        }
    }







}
