<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MailerRequest;
use App\MailerSetting;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Traits\Shared;
class MailerSettingController extends Controller
{
    private $data;
    use shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['title'] = 'Mailer Setting';
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mailer=MailerSetting::first();

        $this->data['mailer']=$mailer;
        return view('admin.setting.mailerSetting',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\MailerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MailerRequest $request)
    {
        $formInput=$request->all();

        if(MailerSetting::create($formInput)){
            Session::flash('success','Add Mailer Setting Data Successfully');

        }else{
            Session::flash('error',' Cannot Add Mailer Setting Data ');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\MailerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MailerRequest $request, $id)
    {
        $formInput=$request->all();

        if(MailerSetting::find($id)->update($formInput)){
            Session::flash('success','Updated Mailer Setting Data Successfully');

        }else{
            Session::flash('error',' Cannot Update Mailer Setting Data ');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
