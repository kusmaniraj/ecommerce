<?php

namespace App\Http\Controllers\Admin;

use App\MenuModel;
use App\Traits\Shared;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class MenuController extends Controller
{
    private $data;
    protected $admin;


    use Shared;
    public  function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title']='Menus';

//        middleware for admin
        $this->admin = Auth::user();
        if($this->admin['type']=='0'){
            return redirect()->back();
        }



    }
    public function index(){


        return view('admin.menuManagement',$this->data);
    }
    public function getMenusTree(){
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id','position')->orderBy('position')->get()->toArray();
        $buildTree=new BuildTreeController();
       return $buildTree->buildTree($menuLists);
    }
    public  function delete($id){


        if (MenuModel::find($id)->delete()) {
        return ['success'=>'Delete Menu Successfully'];



        } else {
            return ['error'=>'Cannot Delete Menu '];
        }


    }
    public function store(Request $request){
        $formInput = $request->all();


        unset($formInput['oldFeaturedImg']);
        $validator=Validator::make($request->all(), [
            'name' => 'required|max:20',

        ],['name.required'=>'*Please Input Menu Name*']);
        if($validator->fails()){
            return ['validation_errors'=>$validator->errors()];
        }


        if (MenuModel::create($formInput)) {

            return ['success'=>'Add Menu Data Successfully'];


        } else {
            return ['error'=>'Cannot Add Menu Data '];

        }
    }
    public function edit($id){
        return MenuModel::find($id);
    }

    public function update(Request $request, $id)
    {

        $formData = $request->all();

        Validator::make($request->all(), [
            'name' => 'required|max:20',

        ],['name.required'=>'*Please Input Menu Name*'])->validate();

        if (empty($formData['featuredImg'])) {

            $formData['featuredImg'] = $formData['oldFeaturedImg'];
        }
        unset($formData['oldFeaturedImg']);

        if (MenuModel::find($id)->update($formData)) {

            return ['success'=>'Update Menu Data Successfully'];

        } else {
            return ['error'=>'Cannot Update Menu Data '];

        }

    }

    public function saveMenu(Request $request)
    {
        $menus = $request->input('treeExport');

        foreach ($menus as $key => $menu) {

            $menu['position'] = $key;
            MenuModel::where('id',$menu['id'])->update($menu);

        }

    }
}
