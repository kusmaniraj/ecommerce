<?php

namespace App\Http\Controllers\Admin;

use App\BillingInfo;
use App\PlaceOrder;
use App\Product;
use App\ProductVariation;
use App\ShippingInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Traits\Shared;
use Auth;
use Carbon\Carbon;

class OrderController extends Controller
{
    use Shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title'] = 'Orders';
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }

    }
    public function index(){

        $this->data['orderList']=$this->getOrders();
        return view('admin.orders.list',$this->data);
    }
    public function listOrders($orderStatus){

        $this->data['orderList']=$this->getOrders($orderStatus);
        return view('admin.orders.list',$this->data);
    }

    public function getOrders($orderStatus=null){
        if($orderStatus){
            $orderList=PlaceOrder::where(['status'=>$orderStatus])->with(['billingInformation','shippingInformation'])->get();
        }else{
            $orderList=PlaceOrder::with(['billingInformation','shippingInformation'])->get();
        }


        foreach($orderList as $key=>$order){
            $priceArray=explode(',',$order->price);
            $qtyArray=explode(',',$order->qty);
            $sizeArray=explode(',',$order->size);
            $productIdArray=explode(',',$order->productId);
            $productVariationIdArray=explode(',',$order->productVariationId);
            $productNameArray=[];
            $productVariationNameArray=[];

            $orderList[$key]['orderPrice']=$priceArray;
            $orderList[$key]['orderQty']=$qtyArray;
            $orderList[$key]['orderSizeName']=$sizeArray;
            foreach($productIdArray as $productId){
                array_push($productNameArray,Product::find($productId));
            }
            foreach($productVariationIdArray as $productVariationId){
                array_push($productVariationNameArray,ProductVariation::find($productVariationId));
            }
            $orderList[$key]['product']=$productNameArray;
            $orderList[$key]['productVariation']=$productVariationNameArray;

        };
        return $orderList;
    }
    public function getInfo($infoType,$infoId){
        if($infoType=='billingInfo'){
            return BillingInfo::find($infoId);
        }else if($infoType=='shippingInfo'){
            return ShippingInfo::where(['id'=>$infoId,'default'=>'yes'])->first();
        }else{
                $order= PlaceOrder::find($infoId);
                $priceArray=explode(',',$order->price);
                $qtyArray=explode(',',$order->qty);
                $sizeArray=explode(',',$order->size);
                $productIdArray=explode(',',$order->productId);
                $productVariationIdArray=explode(',',$order->productVariationId);
                $productNameArray=[];
                $productVariationNameArray=[];

                $orderList['orderPrice']=$priceArray;
                $orderList['orderQty']=$qtyArray;
                $orderList['orderSizeName']=$sizeArray;
                foreach($productIdArray as $productId){
                    array_push($productNameArray,Product::find($productId));
                }
                foreach($productVariationIdArray as $productVariationId){
                    array_push($productVariationNameArray,ProductVariation::find($productVariationId));
                }
                $orderList['product']=$productNameArray;
                $orderList['productVariation']=$productVariationNameArray;
            return $orderList;

            };


    }
    public function changeStatus($id,$statusValue){
        $result=PlaceOrder::where('id',$id)->update(['status'=>$statusValue]);
        if($result){
            return ['success'=>'Change Status to ' .$statusValue];
        }else{
            return ['success'=>'Cannot Change Status to '. $statusValue];
        }
    }
}
