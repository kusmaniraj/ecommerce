<?php

namespace App\Http\Controllers\admin;

use App\CategoryType;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\ProductAttribute;

use App\ProductColor;
use App\ProductVariation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Auth;

class ProductController extends Controller
{
    private $data, $admin;
    use shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title'] = 'Product';
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['products'] =$this->getProducts();

        return view('admin.product.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProducts()
    {
        return Product::with(['productVariation'])->get();
    }



    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {


        $formInput = $request->all();
        if($formInput['specialProduct']=='yes' && $this->countProduct('specialProduct')==false){

            return ['errors' => ['special_Product'=>'Special Product Has been Already Selected 10']];
        }
        if($formInput['featuredProduct']=='yes'  && $this->countProduct('featuredProduct')==false){

            return ['errors' => ['featuredProduct'=>'Featured Product Has been Already Selected 10']];
        }
        if($formInput['slider']=='yes'  && $this->countProduct('slider')==false){

            return ['errors' => ['slider'=>'Slider  Has been Already Selected 5']];
        }
        $result = Product::create($formInput);



        if ($result) {
            return ['success' => ' Create Product','insertedProduct'=>Product::find($result->id)];
        } else {
            return ['error' => 'Cannot Create Product'];
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::where('id', $id)->first();
        return $product;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {

        $formInput = $request->all();
        if($formInput['specialProduct']=='yes' && $this->countProduct('specialProduct')==false){

            return ['errors' => ['special_Product'=>'Special Product Has been Already Selected 10']];
        }
        if($formInput['featuredProduct']=='yes'  && $this->countProduct('featuredProduct')==false){

            return ['errors' => ['featuredProduct'=>'Featured Product Has been Already Selected 10']];
        }
        if($formInput['slider']=='yes'  && $this->countProduct('slider')==false){

            return ['errors' => ['slider'=>'Slider  Has been Already Selected 5']];
        }
//        update variation type
        if($formInput['variationType']=='simple'){

            if($count=ProductVariation::where(['product_id'=>$id,'type'=>'advance'])->count()!=0){
                return ['errors' => ['type'=>'Advance Type Variation has  data,to delete Click here <a href="#" class="productVariationBtn btn btn-info btn-sm" data-type="advance" data-id="'.$id.'">Advance Variation('.$count.')</a> ']];
            }
        }else{
            if($count=ProductVariation::where(['product_id'=>$id,'type'=>'simple'])->count()!=0){
                return ['errors' => ['type'=>'Advance Type Variation had data,First delete it <a href="#" class="productVariationBtn " data-type="simple" data-id="'.$id.'">Simple Variation('.$count.')</a>  ']];
            }
        }
        $result = Product::find($id)->update($formInput);

        if ($result) {
            return ['success' => ' Update Product'];
        } else {
            return ['error' => 'Update Create Product'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($count=ProductVariation::where(['product_id'=>$id])->count()!=0){
            return ['error' =>'This Product has Variation,First Delete Selected Variations'];
        }
        $result = Product::where('id', $id)->delete();

        if ($result) {
            return ['success' => ' Delete Product'];
        } else {
            return ['error' => 'Cannot Delete Product'];
        }
    }

    public function countProduct($countProduct)
    {
        if($countProduct=='specialProduct'){
            $specialProduct = Product::where(['status' => 'active', 'specialProduct' => 'yes'])->count();
            if ($specialProduct >= 9) {
                return false;
            }
        }elseif($countProduct=='featuredProduct'){
            $featuredProduct = Product::where(['status' => 'active', 'featuredProduct' => 'yes'])->count();
            if ($featuredProduct >= 9) {
              return false;

            }
        }else{
            $slider = Product::where(['status' => 'active', 'slider' => 'yes'])->count();
            if ($slider >= 5) {
                return false;

            }
        }
        return true;




    }

}
