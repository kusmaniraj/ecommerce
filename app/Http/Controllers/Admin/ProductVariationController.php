<?php

namespace App\Http\Controllers\admin;

use App\CategoryType;
use App\Gallery;
use App\Http\Requests\ProductVariationRequest;
use App\ProductSize;
use App\ProductVariation;
use App\ProductVariationAttribute;

use App\ProductVariationColor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Auth;

class ProductVariationController extends Controller
{
    private $data, $admin;
    use shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title'] = 'ProductVariation';
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['productVariations'] =$this->getProductVariations();


        return view('admin.productVariation.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProductVariations($productId)
    {
        return ProductVariation::where('product_id',$productId)->get();
    }
    public function getSizes(){
        return ProductSize::all();
    }



    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ProductVariationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductVariationRequest $request)
    {


        $formInput = $request->all();
        if(isset($formInput['size'])){
            $formInput['size']=implode(',',$formInput['size']);
        }
        if(isset($formInput['qty'])){
            $formInput['qty']=implode(',',$formInput['qty']);
        }


        $result = ProductVariation::create($formInput);



        if ($result) {
            return ['success' => ' Create ProductVariation'];
        } else {
            return ['error' => 'Cannot Create ProductVariation'];
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productVariation=ProductVariation::where('id', $id)->first();
        $productVariation['quantities']=explode(',',$productVariation['qty']);
        $productVariation['sizes']=explode(',',$productVariation['size']);

        return $productVariation;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductVariationRequest $request, $id)
    {
        $formInput = $request->all();

        if(isset($formInput['size'])){
            $formInput['size']=implode(',',$formInput['size']);
        }
        if(isset($formInput['qty'])){
            $formInput['qty']=implode(',',$formInput['qty']);
        }
        ProductVariation::find($id)->delete();


        $result = ProductVariation::create($formInput);

        if ($result) {
            return ['success' => ' Update ProductVariation'];
        } else {
            return ['error' => 'Update Create ProductVariation'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        // delete galleries
        $galleries=Gallery::where('product_variation_id',$id)->get();
        if($galleries){
            foreach($galleries as $gallery){
                $original_path=public_path($gallery['original_image_url']);
                $thumb_path=public_path($gallery['thumb_image_url']);
                if(file_exists($original_path)){
                    @unlink($original_path);
                }
                if(file_exists($thumb_path)){
                    @unlink($thumb_path);
                }
            }

        }
        $result = ProductVariation::where('id', $id)->delete();
        if ($result) {
            return ['success' => ' Delete ProductVariation'];
        } else {
            return ['error' => 'Cannot Delete ProductVariation'];
        }
    }



}
