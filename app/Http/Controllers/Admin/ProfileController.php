<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin;
use Validator;
use Image;
use Session;
use Hash;
use App\Traits\Shared;

class ProfileController extends Controller
{
    private $data;
    use Shared;
    public  function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title']='Profile';

    }
    public function index(){
        return view('admin.profile',$this->data);
    }
    public function update(Request $request){
        $inputData=$request->all();
        $image_name=$inputData['old_profileImg'];
        $validator=Validator::make($inputData, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|',

        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }
        if (empty($inputData['profileImg'])) {
            $inputData['profileImg']=$inputData['old_profileImg'];
            unset($inputData['old_profileImg']);
        
        }

        if(empty($inputData['current_password'])&&empty($inputData['password'])&&empty($inputData['password_confirmation'])){
            $inputData['password']=Auth::user()->password;

        }else{

            if(empty($inputData['current_password'])){
                Session::flash('incorrect', 'Required Current Password');
                return redirect()->back();
            }
            if(!Hash::check($inputData['current_password'],Auth::user()->password)){
                Session::flash('incorrect', 'Incorrect Current Password');
                return redirect()->back();
            }
            $validator=Validator::make($inputData, [
                'password' => 'required|string|min:6|confirmed',

            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }

            if($inputData['password']==$inputData['password_confirmation']){
                $inputData['password']=bcrypt($inputData['password']);
                unset($inputData['current_password']);
                unset($inputData['password_confirmation']);

            }
        }


        $user_data=Admin::find(Auth::user()->id)->update($inputData);
        if($user_data){


            Session::flash('success', 'Update profile Data Successfully');


        }else{
            Session::flash('error', 'Update Profile Data Unsuccessfully');
        }
        return redirect()->back();



    }

}
