<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SettingRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Setting;
use File;
use Image;
class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $data;
    public function __construct()
    {

        $this->data['title']=' Setting';
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();


    }

    public function index()
    {

        $this->data['getSetting']=$this->getSetting();
        return view('admin.setting.generalEmailContactSetting',$this->data);
    }
    public function getSetting(){
        return Setting::first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingRequest $request)
    {


        $formInput=$request->all();
////        upload controller
//        $img=new UploadController();



        if(Setting::create($formInput)){
            Session::flash('success','Add Setting Data Successfully');



        }else{
            Session::flash('error',' Cannot Add Setting Data ');
        }
        return redirect('admin/setting');
    }


    public function update(SettingRequest $request, $id)
    {
         $formInput=$request->all();

//        if(empty($formInput['logo'])){
//
//            $formInput['logo']=$formInput['oldLogo'];
//        }
        if(Setting::find($id)->update($formInput)){

            Session::flash('success','Update Setting Data Successfully');



        }else{
            Session::flash('error',' Cannot Update Setting Data ');
        }
        return redirect('admin/setting');
    }



}
