<?php

namespace App\Http\Controllers\Admin;


use App\Product;
use App\ProductAttribute;
use App\SizeType;
use App\Http\Requests\SizeRequest;
use App\ProductSize;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Auth;

class SizeController extends Controller
{
    private $data, $admin;
    use shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title'] = 'Size';
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.productAttribute.size.list', $this->data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSizes()
    {
        return ProductSize::all();


    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\SizeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SizeRequest $request)
    {
        $result = ProductSize::create($request->all());
        if ($result) {
            return ['success' => ' Created Size'];
        } else {
            return ['error' => 'Cannot Create Size'];
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return ProductSize::find($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SizeRequest $request, $id)
    {
        $result = ProductSize::find($id)->update($request->all());
        if ($result) {
            return ['success' => ' Updated Size'];
        } else {
            return ['error' => 'Cannot Update Size'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(ProductAttribute::where('size',$id)->count()!=0){
            return ['error' => 'Oops !! Selected Size is used in Product,Cannot Delete'];
        }
        $result = ProductSize::where('id', $id)->delete();
        if ($result) {
            return ['success' => ' Deleted Size'];
        } else {
            return ['error' => 'Cannot Deleted Size'];
        }
    }


}
