<?php

namespace App\Http\Controllers\Admin;


use App\Category;
use App\Product;

use App\Http\Requests\SliderRequest;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Auth;

class SliderController extends Controller
{
    private $data, $admin;
    use shared;

    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title'] = 'Slider';
        //        middleware for admin
        $this->admin = Auth::user();
        if ($this->admin['type'] == '0') {
            return redirect()->back();
        }

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['categories']=Category::where(['parent_category_id'=>null,'sub_category_id'=>null,'second_sub_category_id'=>null,'status'=>'active'])->get();
        return view('admin.slider.list', $this->data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSliders()
    {
        return Slider::all();


    }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\SliderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {
        $result = Slider::create($request->all());
        if ($result) {
            return ['success' => ' Created Slider'];
        } else {
            return ['error' => 'Cannot Create Slider'];
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Slider::find($id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderRequest $request, $id)
    {
        $result = Slider::find($id)->update($request->all());
        if ($result) {
            return ['success' => ' Updated Slider'];
        } else {
            return ['error' => 'Cannot Update Slider'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Slider::where('id', $id)->delete();
        if ($result) {
            return ['success' => ' Deleted Slider'];
        } else {
            return ['error' => 'Cannot Deleted Slider'];
        }
    }


}
