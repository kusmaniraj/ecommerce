<?php

namespace App\Http\Controllers\Admin;

use App\BillingInfo;
use App\ShippingInfo;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;

class UserController extends Controller
{

    protected $data;
    use Shared;
    public  function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        //booking notifications
        $this->data['booking_notifications']=$this->getBookingNotifications();
        $this->data['title']='User Manage';

    }
    public function index($userType){
        if($userType=='guest'){
            $this->data['users']=$this->getUsers('guest');
        }else{
            $this->data['users']=$this->getUsers('registered');
        }

        return view('admin.user.list',$this->data);
    }
    public function getUsers($userType){
        if($userType=='guest'){
            return BillingInfo::where('userId',NULL)->with('shippingInfos')->get();

        }else{
            return User::with('shippingInfos')->get();
        }

    }
    public function showAccountInfo($userEmail){
        return BillingInfo::where('userEmail',$userEmail)->first();

    }
    public function delete($userType,$id){
        if($userType=='guest'){
            $result=BillingInfo::find($id)->delete();
        }else{
            $result=User::find($id)->delete();
        }


        if($result){
            return ['success'=>'delete user '];
        }else{
            return ['success'=>' error to delete '];
        }

    }
    public function getShippingInfo($userEmail){
        return ShippingInfo::where('userEmail',$userEmail)->get();

    }
    public function showShippingInfo($id){
        return ShippingInfo::find($id);

    }
}
