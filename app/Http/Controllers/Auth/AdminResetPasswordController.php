<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Password;
use Illuminate\Http\Request;
use Auth;
use App\Traits\Shared;
class AdminResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    protected $redirectTo = '/admin';

    use ResetsPasswords;
    use Shared;
    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();

        //end Traits
    }

    public function showResetForm(Request $request, $token = null)
    {
        $this->data['title']='Password Reset';
        return view('auth.admin.passwords.reset',$this->data)->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function broker()
    {
        return Password::broker('admins');
    }



}
