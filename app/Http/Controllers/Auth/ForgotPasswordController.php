<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Controller;
use App\MenuModel;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Traits\Shared;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $data;
    use Shared;
    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);

        //shared listCategories function
        $this->data['listCategories'] = $this->listCategories();

        //end Traits
        $this->data['title']='Forgot Password';
        $this->middleware('guest');
    }
    public function showLinkRequestForm()
    {
        return view('auth.passwords.email',$this->data);
    }
}
