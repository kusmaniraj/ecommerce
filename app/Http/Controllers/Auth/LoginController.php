<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Controller;
use App\MenuModel;
use App\User;
use Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Traits\Shared;
use Illuminate\Http\Request;
use Socialite;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use Shared;
    private $data;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);

        //shared listCategories function
        $this->data['listCategories'] = $this->listCategories();
        $this->data['title'] = 'Login ';

        //end Traits
        $this->middleware('guest')->except('logout');

    }
    public function showLoginForm()
    {
        return view('auth.login',$this->data);
    }
    public function ajaxLogin(Request $request)
    {
        $formInput=$request->all();
        $validator= Validator::make($formInput, [

            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',

        ]);
        if ($validator->fails()) {
            return ['errors'=>$validator->errors()];
        }
        // Attempt to log the user in
        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return response(['success'=>'login success','redirectUrl'=>url()->previous()]);
        }else{
            $data['errors']=['email'=>'These credentials do not match our records.'];
            // if unsuccessful, then redirect back to the login with the form data
            return response(['error'=>'cannot login ']);
        }

    }
    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($providerType)
    {
        return Socialite::driver($providerType)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($providerType)
    {
        $user = Socialite::driver($providerType)->user();

        $authUser = $this->findOrCreateUser($user, $providerType);
        Auth::login($authUser, true);
        return redirect('checkout');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $providerType)
    {
        $authUser = User::where('providerId', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'providerType' => $providerType,
            'providerId' => $user->id
        ]);
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->back();
    }
}
