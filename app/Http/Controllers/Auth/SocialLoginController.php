<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\MenuModel;
use App\User;
use Validator;
use Auth;
use App\Traits\Shared;
use Illuminate\Http\Request;
use Socialite;


class SocialLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use Shared;
    private $data;




    public function __construct()
    {
        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);

        //shared listCategories function
        $this->data['listCategories'] = $this->listCategories();
        $this->data['title'] = 'Login ';

        //end Traits
        $this->middleware('guest')->except('logout');

    }

    public function redirectToProvider($providerType)
    {
        return Socialite::driver($providerType)->fields([
        'first_name', 'last_name', 'email', 'gender', 'location','hometown'
    ])->scopes([
        'email', 'user_location','user_gender','user_hometown'
    ])->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($providerType)
    {
//        $user = Socialite::with($providerType)->fields([
//            'first_name', 'last_name', 'email', 'gender','location','hometown'
//        ])->user();
//
        $user = Socialite::driver($providerType)->user();

        $authUser = $this->findOrCreateUser($user, $providerType);

        Auth::login($authUser, true);
        return redirect()->back();
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $providerType)
    {
        $authUser = User::where('providerId', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }

        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'providerType' => $providerType,
            'providerId' => $user->id
        ]);
    }


}
