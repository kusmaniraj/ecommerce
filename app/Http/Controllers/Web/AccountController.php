<?php

namespace App\Http\Controllers\Web;

use App\MenuModel;
use App\BillingInfo;
use App\PlaceOrder;
use App\Product;
use App\ProductRated;
use App\ProductSize;
use App\ProductVariation;
use App\ShippingInfo;
use App\Wishlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use Hash;
use App\User;
use Carbon\Carbon;

class AccountController extends Controller
{
    private $data;

    use Shared;

    public function __construct()
    {

        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);

        //shared listCategories function
        $this->data['listCategories'] = $this->listCategories();
        $this->data['title'] = 'My Account ';


    }

    public function index()
    {
        $this->data['headerTitle'] = 'Account Dashboard';
        return view('web.account.dashboard', $this->data);
    }

    public function accountInformation()
    {
        $this->data['headerTitle'] = 'Account Information';
        $this->data['accountInformation'] = BillingInfo::where('userId', Auth::user()->id)->first();
        return view('web.account.information', $this->data);

    }

    public function storeAccountInformation(Request $request, $id = null)
    {
        $formInput = $request->all();
        $messagePasswordChange = "";
        $formInput['userId'] = Auth::user()->id;
        $validator = Validator::make($formInput, [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:billing_infos,userEmail,' . $id,
            'gender' => 'required',
            'address' => 'required',
            'city' => 'required',
            'region' => 'required',
            'telephone' => 'required|numeric|digits:10',


        ]);
        $formInput['userEmail'] = $formInput['email'];
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        if ($formInput['current_password'] != "" || $formInput['password'] != "" || $formInput['password_confirmation'] != "") {

            if (empty($formInput['current_password'])) {
                $validator = ['current_password' => 'Current Password is Required !'];
                return redirect()->back()->withErrors($validator);
            }
            if (!Hash::check($formInput['current_password'], Auth::user()->password)) {

                $validator = ['current_password' => ' Incorrect Current Password  !'];
                return redirect()->back()->withErrors($validator);
            }
            $validator = Validator::make($formInput, [
                'password' => 'required|string|min:6|confirmed',

            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }

            if ($formInput['password'] == $formInput['password_confirmation']) {
                $formInput['password'] = bcrypt($formInput['password']);
                unset($formInput['current_password']);
                unset($formInput['password_confirmation']);
                User::find(Auth::user()->id)->update(['password' => $formInput['password']]);
                $messagePasswordChange = 'Password Also Change';

            }


        }


        if ($id) {
            $result = BillingInfo::find($id)->update($formInput);
            if ($result) {

                Session::flash('success', 'Account Information Updated ,Successfully !!' . $messagePasswordChange);
            } else {
                Session::flash('error', ' Cannot Update Account Information ');
            }
        } else {
            $result = BillingInfo::create($formInput);
            if ($result) {
                Session::flash('success', 'Account Information Created ,Successfully !!' . $messagePasswordChange);
            } else {
                Session::flash('error', ' Cannot Store Account Information ');
            }
        }
        return redirect()->back();


    }

    public function addressBook()
    {
        $this->data['headerTitle'] = 'Address Book';
        $this->data['shippingInfo'] = ShippingInfo::where('userId', Auth::user()->id)->orderBy('id', 'asc')->paginate(9);
        $this->data['accountInfo'] = BillingInfo::where('userId', Auth::user()->id)->first();
        return view('web.account.addressBook', $this->data);

    }

    public function removeAddressBook($id)
    {
        $result = ShippingInfo::find($id)->delete();

        if ($result) {
            Session::flash('success', ' Remove Shipping Info');
        } else {
            Session::flash('error', 'Cannot Remove Shipping Info');

        }
        return redirect()->back();
    }

    public function changeShippingInfoDefault($id)
    {
//        set all default value to no
        ShippingInfo::where(['userId' => Auth::user()->id])->update(['default' => 'no']);
        $result = ShippingInfo::find($id)->update(['default' => 'yes']);
        if ($result) {
            Session::flash('success', ' Change Shipping Info Default');
        } else {
            Session::flash('error', 'Cannot Change Shipping Info Default');

        }
        return redirect()->back();

    }

    public function editAddressBook($id)
    {
        return ['shippingInfo' => ShippingInfo::find($id)];


    }

    public function storeAddressBook(Request $request, $id = null)
    {
        $formInput = $request->all();
        $formInput['userId'] = Auth::user()->id;
        $formInput['userEmail'] = Auth::user()->email;
        $validator = Validator::make($formInput, [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:shipping_infos,email,' . $formInput['id'],
            'gender' => 'required',
            'address' => 'required',
            'city' => 'required',
            'region' => 'required',
            'telephone' => 'required|numeric|digits:10',


        ]);
        if ($validator->fails()) {
            return ['validationError' => $validator->errors()];
        }

        if ($id) {
            $result = ShippingInfo::find($id)->update($formInput);
            if ($result) {
                Session::flash('success', 'Shipping Information Updated!');
                return ['success' => 'Successfully update Address book', 'url' => url()->previous()];
            } else {
                return ['success' => 'Cannot update Address book'];
            }
        } else {
            $result = ShippingInfo::create($formInput);
            if ($result) {
                Session::flash('success', 'Shipping Information Added ');
                return ['success' => 'Successfully Added Address book', 'url' => url()->previous()];
            } else {
                return ['success' => 'Cannot Added Address book'];
            }
        }


    }

    public function wishlist()
    {
        $this->data['headerTitle'] = 'My WishList';
        $this->data['wishLists'] = Wishlist::with(['product', 'productVariation', 'productSize'])->where(['userId' => Auth::user()->id])->paginate(9);


        return view('web.account.wishlist', $this->data);

    }

    public function getWishlist($productId, $productVariationId, $sizeId = null)
    {
        $wishlistData = Wishlist::where(['userId' => Auth::user()->id, 'productId' => $productId, 'productVariationId' => $productVariationId, 'size' => $sizeId])->first();

        if ($wishlistData) {
            return ['success' => "Added to wishlist", 'id' => $wishlistData->id];
        } else {
            return ['error' => "Not product Added to wishlist"];
        }

    }

    public function storeToWishlist(Request $request)
    {
        $formInput = $request->all();
        $wishlistData = [
            'userId' => Auth::user()->id,
            'productId' => $formInput['product_id'],
            'productVariationId' => $formInput['product_variation_id'],
            'maxQty' => $formInput['maxQty'],
            'size' => $formInput['size_id'],
            'qty' => $formInput['qty']

        ];
        if ($formInput['id']) {
            return ['success' => 'already added to wishlist', 'id' => $formInput['id']];
        } else {
            $result = Wishlist::create($wishlistData);
        }


        if ($result) {
            return ['success' => 'success added to wishlist', 'id' => $result->id];
        } else {
            return ['error' => 'cannot added to wishlist'];
        }
    }

    public function removeFromWishList($id)
    {
        $result = Wishlist::find($id)->delete();
        if ($result) {
            Session::flash('success', ' Remove product from wishlist');

        } else {
            Session::flash('error', 'Cannot Remove product from  wishlist');

        }
        return redirect()->back();
    }

//    order
    public function orders()
    {
        $this->data['headerTitle'] = 'My Orders';
        $orderList=PlaceOrder::with(['billingInformation','shippingInformation'])->where(['userId' => Auth::user()->id])->get();
        $this->data['orderList']=$orderList;
        foreach( $this->data['orderList'] as $key=>$order){
            $priceArray=explode(',',$order->price);
            $qtyArray=explode(',',$order->qty);
            $sizeArray=explode(',',$order->size);
            $productIdArray=explode(',',$order->productId);
            $productVariationIdArray=explode(',',$order->productVariationId);
            $productNameArray=[];
            $productVariationNameArray=[];

            $orderList[$key]['orderPrice']=$priceArray;
            $orderList[$key]['orderQty']=$qtyArray;
            $orderList[$key]['orderSizeName']=$sizeArray;
            foreach($productIdArray as $productId){
               array_push($productNameArray,Product::find($productId));
            }
            foreach($productVariationIdArray as $productVariationId){
                array_push($productVariationNameArray,ProductVariation::find($productVariationId));
            }
            $orderList[$key]['product']=$productNameArray;
            $orderList[$key]['productVariation']=$productVariationNameArray;

        };



        return view('web.account.order.list', $this->data);
    }

    public function viewOrder(Request $request){

        $this->data['headerTitle'] = 'My Orders';
        $this->data['orderInfo']['date']=$request->date;

        $orderList=PlaceOrder::with(['billingInformation','shippingInformation'])->where(['created_at'=>$request->date,'email'=>$request->email])->where(['userId' => Auth::user()->id])->first();
        $priceArray=explode(',',$orderList->price);
        $qtyArray=explode(',',$orderList->qty);
        $sizeArray=explode(',',$orderList->size);
        $productIdArray=explode(',',$orderList->productId);
        $productVariationIdArray=explode(',',$orderList->productVariationId);
        $productNameArray=[];
        $productVariationNameArray=[];

        $orderList['orderPrice']=$priceArray;
        $orderList['orderQty']=$qtyArray;
        $orderList['orderSizeName']=$sizeArray;

        foreach($productIdArray as $productId){
            array_push($productNameArray,Product::find($productId));
        }
        foreach($productVariationIdArray as $productVariationId){
            array_push($productVariationNameArray,ProductVariation::find($productVariationId));
        }
        $orderList['product']=$productNameArray;
        $orderList['productVariation']=$productVariationNameArray;

        $this->data['orderInfo']=$orderList;

        return view('web.account.order.detail', $this->data);



    }
//    review
    public function reviews()
    {
        $this->data['headerTitle'] = 'My Product Reviews';

        $this->data['reviews']=ProductRated::with(['product'])->where(['userId' => Auth::user()->id])->paginate(1);

        return view('web.account.productReview.list', $this->data);
    }
    public function editReview($id){
        $this->data['headerTitle'] = 'My Product Reviews';
        $this->data['review']=ProductRated::with(['product'])->where(['userId' => Auth::user()->id,'id'=>$id])->first();
        return view('web.account.productReview.form', $this->data);
    }
    public function updateReview(Request $request,$id){

        $request->validate([
            'ratedStar'=>'required',
            'title'=>'required',
            'review'=>'required',
            'productId'=>'required',

        ]);

        $formInput=$request->all();
        $formInput['userId']=Auth::user()->id;
        $result=ProductRated::find($id)->update($formInput);
        if($result){
            Session::flash('success',' Change Review of selected product ');
        }else{
            Session::flash('error',' Cannot Change Review of selected product');
        }
        return redirect()->back();
    }
    public function removeReview($id){
        $result=ProductRated::find($id)->delete();
        if($result){
            Session::flash('success',' Removed Review of selected product ');
        }else{
            Session::flash('error',' Removed Change Review of selected product');
        }
        return redirect()->back();
    }
}
