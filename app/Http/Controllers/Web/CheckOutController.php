<?php

namespace App\Http\Controllers\Web;

use App\Mail\OrderMail;
use App\MenuModel;
use App\BillingInfo;
use App\PlaceOrder;
use App\ShippingInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use App\User;
use Mail;

class CheckOutController extends Controller
{
    private $data;

    use Shared;

    public function __construct()
    {

        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);

        //shared listCategories function
        $this->data['listCategories'] = $this->listCategories();
        $this->data['title'] = 'Checkout ';


    }

    public function index()
    {


        if (Session::has('cart')) {

            if(count(Session::get('cart')) >0){
                $this->data['sessionCart']=Session::get('cart');
            }else{
                return redirect('cart');
            }
        } else {
            return redirect('cart');
        }
        if (Auth::guard('web')->check()) {
            $this->data['user'] = Auth::user();

            $this->data['userDetails'] = BillingInfo::where('userId', $this->data['user']['id'])->first();
        }


        return view('web.check_out.index', $this->data);
    }

    public function storeBillingInformation(Request $request)
    {
        $formInput = $request->all();


        $validator = Validator::make($formInput, [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'gender' => 'required',
            'address' => 'required',
            'city' => 'required',
            'region' => 'required',
            'telephone' => 'required|numeric|digits:10',


        ]);
        if (Auth::guard('web')->check()) {
            $formInput['userId'] = Auth::user()->id;
        }

        if ($validator->fails()) {
            return ['validationError' => $validator->errors()];
        }


//check for signin or not
        if ($user = $this->checkSignIn($formInput)) {

            Auth::login($user, true);
            $formInput['userEmail'] = $user->email;
            $formInput['userId'] = $user->id;

        }
//        insert billing information to shipping Address

        $shippingInfoData=[
            'firstName' => $formInput['firstName'],
            'lastName' => $formInput['lastName'],
            'userEmail'=>$formInput['userEmail'],
            'email'=>$formInput['userEmail'],
            'gender' => $formInput['gender'],
            'address' => $formInput['address'],
            'city' =>$formInput['city'],
            'region' =>$formInput['region'],
            'telephone' => $formInput['telephone'],



        ];
        if (Auth::guard('web')->check()) {
            $shippingInfoData['userId'] = Auth::user()->id;
        }

        $shippingInfo = ShippingInfo::where(['userEmail' => $formInput['userEmail'], 'email' => $formInput['userEmail']])->first();
        if ($shippingInfo) {

            ShippingInfo::where(['userEmail' => $formInput['userEmail'], 'email' => $formInput['userEmail']])->update($shippingInfoData);


        } else {
            $formInput['default'] = 'yes';
            ShippingInfo::create($shippingInfoData);
        }


//check create or update billing info

        if ($formInput['id']) {
            $resultUpdate = BillingInfo::find($formInput['id'])->update($formInput);
            if ($resultUpdate) {
                $billingDetail = BillingInfo::find($formInput['id']);


                return ['success' => 'Successfully update', 'billingDetail' => $billingDetail];
            } else {
                return ['error' => 'Cannot Updated'];
            }

        } else {

            $resultInsert = BillingInfo::create($formInput);
            if ($resultInsert) {


                return ['success' => 'Successfully created', 'billingDetail' => $resultInsert];
            } else {
                return ['error' => 'Cannot Created'];
            }
        }


    }

    public function getShippingInfos(Request $request)
    {
        $formInput = $request->all();
        if (Auth::guard('web')->check()) {
            $formInput['userEmail'] = Auth::user()->email;
        }

        return ['shippingInfos' => ShippingInfo::where(['userEmail' => $formInput['userEmail']])->orderBy('id', 'asc')->get()];

    }

    public function storeShippingInformation(Request $request)
    {
        $formInput = $request->all();


        if (Auth::guard('web')->check()) {
            $formInput['userId'] = Auth::user()->id;
            $formInput['userEmail'] = Auth::user()->email;
        }
        $validator = Validator::make($formInput, [
            'firstName' => 'required|string|max:255',
            'lastName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:shipping_infos,email,' . $formInput['id'],
            'gender' => 'required',
            'address' => 'required',
            'city' => 'required',
            'region' => 'required',
            'telephone' => 'required|numeric|digits:10',


        ]);

        if ($validator->fails()) {
            return ['validationError' => $validator->errors()];
        }


        if ($formInput['id']) {
            $resultUpdate = ShippingInfo::find($formInput['id'])->update($formInput);

            if ($resultUpdate) {
                $shippingDetail = ShippingInfo::find($formInput['id']);

                return ['success' => 'Successfully update', 'shippingDetail' => $shippingDetail];
            } else {
                return ['error' => 'Cannot Updated'];
            }

        } else {
            $resultInsert = ShippingInfo::create($formInput);
            if ($resultInsert) {

                return ['success' => 'Successfully created', 'id' => $resultInsert->id, 'shippingDetail' => $resultInsert];
            } else {
                return ['error' => 'Cannot Created'];
            }
        }

    }

    public function removeShippingInformation($id)
    {

        $result = ShippingInfo::find($id)->delete();

        if ($result) {
            return ['success' => 'Successfully Remove Shipping Info'];
        } else {
            return ['error' => 'Cannot Remove Shipping Info'];
        }

    }

    public function editShippingInformation($id)
    {
        return ['shippingInfo' => ShippingInfo::find($id)];


    }

    public function changeShippingInfoDefault($userEmail, $id)
    {
//        set all default value to no
        ShippingInfo::where(['userEmail' => $userEmail])->update(['default' => 'no']);
        $result = ShippingInfo::find($id)->update(['default' => 'yes']);
        if ($result) {
            return ['success' => 'Successfully Change Shipping Info Default'];
        } else {
            return ['error' => 'Cannot Change Shipping Info Default'];
        }

    }

    public function checkSignIn($formInput)
    {
        if (isset($formInput['logIn']) && $formInput['logIn'] == 'yes' && $formInput['password'] && $formInput['password_confirmation']) {

            $validator = Validator::make($formInput, [

                'email' => 'required|string|email|max:255|unique:users,userEmail',
                'password' => 'required|string|min:6|confirmed',

            ]);
            if ($validator->fails()) {
                return ['validationError' => $validator->errors()];
            }
            $user = User::create([
                'name' => $formInput['firstName'] . $formInput['firstName'],
                'email' => $formInput['userEmail'],
                'password' => bcrypt($formInput['password']),
            ]);
            return $user;


        }
    }

    public function checkEmail(Request $request)
    {
        $formInput = $request->all();
        $validator = Validator::make($formInput, [
            'email' => 'required|string|email|max:255|unique:users',
        ]);
        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        } else {
            return ['success' => 'success'];
        }
    }


    public function placeOrder(Request $request)
    {
        $formInput = $request->all();
        $userId="";





        if (Auth::guard('web')->check()) {
            $userId= Auth::user()->id;
        }

        $orderData = [
            'email' => $formInput['email'],
            'billingInfoId' => $formInput['billingId'],
            'shippingInfoId' => $formInput['shippingId'],
            'productId'=> implode(',',$formInput['productId']),
            'productVariationId'=>implode(',',$formInput['productVariationId']) ,
            'price'=> implode(',',$formInput['price']),
            'qty'=> implode(',',$formInput['qty']),
            'size'=> implode(',',$formInput['size']),
            'userId'=>$userId


        ];
        $sendUser['email']=$formInput['email'];
        //        insert or update into table
        $resultInsert = PlaceOrder::updateorcreate($orderData);
        Mail::to(  $this->data['getSetting']['owner_email'])->send(new OrderMail($sendUser, $orderData));

        if (Mail::failures()) {
            return ['error' => 'Cannot Confirmed Booked ,Mail has not been Sent'];
        }



        if ($resultInsert) {
//            destroy cart session
            Session::forget('cart');
            $orderMessage=['status'=>'success','message'=>'Successfully Order shopping Cart Product'];
             Session::flash('orderMessage',$orderMessage);
            if(Auth::guard('web')->check()){
                $url=url('myAccount/orders');
            }else{
                $url=url('/');
            }


            return ['success' => 'Successfully Order shopping Cart Product','url'=>$url];
        } else {
            $orderMessage=['status'=>'error','message'=>'Cannot Order shopping Cart Product'];
            Session::flash('orderMessage',$orderMessage);
            return ['error' => 'Cannot Order shopping Cart Product','url'=>url()->previous()];
        }


    }

}
