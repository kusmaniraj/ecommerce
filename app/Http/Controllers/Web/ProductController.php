<?php

namespace App\Http\Controllers\Web;


use App\MenuModel;
use App\PlaceOrder;
use App\Product;
use App\ProductAttribute;
use App\ProductRated;
use App\ProductSize;
use App\ProductVariation;
use App\Category;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Illuminate\Http\Request;
use Session;
use Auth;

class   ProductController extends Controller
{
    private $data;

    use Shared;

    public function __construct()
    {

        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);

        //shared listCategories function
        $this->data['listCategories'] = $this->listCategories();
        $this->data['title'] = 'Product ';


    }

    public function getCategoriesProduct($listType, $categoryId)
    {
        $this->data['title'] = 'Product List';
        $paginateAndSearch = [
            'showPage' => request()->showPage,
            'orderBy' => request()->orderBy,
            'rating' => request()->rating,
            'price' => request()->price,
        ];
        if ($paginateAndSearch) {
            $this->data['categoryProducts'] = $this->pagination($categoryId, $paginateAndSearch);

        } else {
            $this->data['categoryProducts'] = $this->getProducts($categoryId);
        }


        if ($listType == 'list') {
            return view('web.product.list', $this->data);
        } else {
            return view('web.product.grid', $this->data);
        }


    }

    public function getProducts($categoryId)
    {
        $category = Category::where(['id' => $categoryId])->first();

        if ($category['parent_category_id'] == null && $category['sub_category_id'] == null && $category['second_sub_category_id'] == null) {
            $categoryProducts = $this->categoryProductList($category['id']);
        } elseif ($category['parent_category_id'] && $category['sub_category_id'] == null && $category['second_sub_category_id'] == null) {
            $categoryProducts = $this->parentCategoryProductList($category['parent_category_id'], $category['id']);
        } elseif ($category['parent_category_id'] && $category['sub_category_id'] && $category['second_sub_category_id'] == null) {

            $categoryProducts = $this->subCategoryProductList($category['parent_category_id'], $category['sub_category_id'], $category['id']);
        } elseif ($category['parent_category_id'] && $category['parent_category_id'] && $category['sub_category_id'] && $category['second_sub_category_id']) {
            $categoryProducts = $this->secondSubCategoryProductList($category['parent_category_id'], $category['sub_category_id'], $category['second_sub_category_id'], $category['id']);
        }
        return $categoryProducts;


    }

    function categoryProductList($categoryId, $paginateAndSearch = null)
    {

        $data['parent_category'] = "";
        $data['sub_category'] = "";
        $data['second_sub_category'] = "";

        $searchData = [
            'category_id' => $categoryId,
            'status' => 'active',

        ];
//        price filter
        $data['price']=[
            'min'=> Product::where($searchData)->orderBy('oldPrice','asc')->first()['oldPrice'],
            'max'=>Product::where($searchData)->orderBy('oldPrice','desc')->first()['oldPrice'],
        ];
//        paginate
        $paginateData = [];
        if ($paginateAndSearch) {
            $searchData['rating'] = $paginateAndSearch['rating'];
            $searchData['price'] = $paginateAndSearch['price'];

            $paginateData = [
                'showPage' => $paginateAndSearch['showPage'],
                'orderBy' => $paginateAndSearch['orderBy'],
            ];
        }
        $data['products'] = $this->getPaginateAndSearchProducts($searchData, $paginateData);


        $data['category'] = Category::find($categoryId);
        $data['title'] = $data['category']->name;

        $data['parent_categories'] = Category::find($categoryId)->parentCategories()->where(['sub_category_id' => null, 'second_sub_category_id' => null, 'status' => 'active'])->with(['parent_category_products' => function ($query) {
            $query->where(['status' => 'active']);
        }])->get();


       return $data;
    }

    function parentCategoryProductList($categoryId, $parentCategoryId, $paginateAndSearch = null)
    {

        $data['sub_category'] = "";
        $data['second_sub_category'] = "";

        $searchData = [
            'category_id' => $categoryId,
            'parent_category_id' => $parentCategoryId,
            'status' => 'active',

        ];
        //        price filter
        $data['price']=[
            'min'=> Product::where($searchData)->orderBy('oldPrice','asc')->first()['oldPrice'],
            'max'=>Product::where($searchData)->orderBy('oldPrice','desc')->first()['oldPrice'],
        ];
        $paginateData = [];
        if ($paginateAndSearch) {
            $searchData['rating'] = $paginateAndSearch['rating'];
            $searchData['price'] = $paginateAndSearch['price'];
            $paginateData = [
                'showPage' => $paginateAndSearch['showPage'],
                'orderBy' => $paginateAndSearch['orderBy'],
            ];
        }
        $data['products'] = $this->getPaginateAndSearchProducts($searchData, $paginateData);
        $data['category'] = Category::find($categoryId);
        $data['parent_category'] = Category::find($parentCategoryId);
        $data['title'] = $data['parent_category']->name;
        $data['sub_categories'] = Category::find($parentCategoryId)->subCategories()->where(['second_sub_category_id' => null, 'status' => 'active'])->with(['sub_category_products' => function ($query) {
            $query->where(['status' => 'active']);
        }])->get();
        return $data;
    }

    function subCategoryProductList($categoryId, $parentCategoryId, $subCategoryId, $paginateAndSearch = null)
    {


        $data['second_sub_category'] = "";

        $searchData = [
            'category_id' => $categoryId,
            'parent_category_id' => $parentCategoryId,
            'sub_category_id' => $subCategoryId,
            'status' => 'active',

        ];
        //        price filter
        $data['price']=[
            'min'=> Product::where($searchData)->orderBy('oldPrice','asc')->first()['oldPrice'],
            'max'=>Product::where($searchData)->orderBy('oldPrice','desc')->first()['oldPrice'],
        ];
        $paginateData = [];
        if ($paginateAndSearch) {
            $searchData['rating'] = $paginateAndSearch['rating'];
            $searchData['price'] = $paginateAndSearch['price'];
            $paginateData = [
                'showPage' => $paginateAndSearch['showPage'],
                'orderBy' => $paginateAndSearch['orderBy'],
            ];
        }

        $data['products'] = $this->getPaginateAndSearchProducts($searchData, $paginateData);


        $data['category'] = Category::find($categoryId);
        $data['parent_category'] = Category::find($parentCategoryId);
        $data['sub_category'] = Category::find($subCategoryId);
        $data['title'] = $data['sub_category']->name;
        $data['second_sub_categories'] = Category::find($subCategoryId)->secondSubCategories()->where(['status' => 'active'])->with(['second_sub_category_products' => function ($query) {
            $query->where(['status' => 'active']);
        }])->get();


        return $data;
    }

    function secondSubCategoryProductList($categoryId, $parentCategoryId, $subCategoryId, $secondSubCategoryId, $paginateAndSearch = null)
    {


        $searchData = [
            'category_id' => $categoryId,
            'parent_category_id' => $parentCategoryId,
            'sub_category_id' => $subCategoryId,
            'second_sub_category_id' => $secondSubCategoryId,
            'status' => 'active',

        ];
        //        price filter
        $data['price']=[
            'min'=> Product::where($searchData)->orderBy('oldPrice','asc')->first()['oldPrice'],
            'max'=>Product::where($searchData)->orderBy('oldPrice','desc')->first()['oldPrice'],
        ];
        $paginateData = [];
        if ($paginateAndSearch) {
            $searchData['rating'] = $paginateAndSearch['rating'];
            $searchData['price'] = $paginateAndSearch['price'];
            $paginateData = [
                'showPage' => $paginateAndSearch['showPage'],
                'orderBy' => $paginateAndSearch['orderBy'],
            ];
        }
        $data['products'] = $this->getPaginateAndSearchProducts($searchData, $paginateData);

        $data['category'] = Category::find($categoryId);
        $data['parent_category'] = Category::find($parentCategoryId);
        $data['sub_category'] = Category::find($subCategoryId);
        $data['second_sub_category'] = Category::find($secondSubCategoryId);
        $data['title'] = $data['second_sub_category']->name;
        return $data;

    }

    public function pagination($categoryId, $paginationAndSearch)
    {
        $category = Category::where(['id' => $categoryId])->first();

        if ($category['parent_category_id'] == null && $category['sub_category_id'] == null && $category['second_sub_category_id'] == null) {
            $categoryProducts = $this->categoryProductList($category['id'], $paginationAndSearch);
        } elseif ($category['parent_category_id'] && $category['sub_category_id'] == null && $category['second_sub_category_id'] == null) {
            $categoryProducts = $this->parentCategoryProductList($category['parent_category_id'], $category['id'], $paginationAndSearch);
        } elseif ($category['parent_category_id'] && $category['sub_category_id'] && $category['second_sub_category_id'] == null) {

            $categoryProducts = $this->subCategoryProductList($category['parent_category_id'], $category['sub_category_id'], $category['id'], $paginationAndSearch);
        } elseif ($category['parent_category_id'] && $category['parent_category_id'] && $category['sub_category_id'] && $category['second_sub_category_id']) {
            $categoryProducts = $this->secondSubCategoryProductList($category['parent_category_id'], $category['sub_category_id'], $category['second_sub_category_id'], $category['id'], $paginationAndSearch);
        }
        return $categoryProducts;
    }

    public function getPaginateAndSearchProducts($search, $paginate)
    {
        if (isset($search['price']) && $search['rating']) {
            $priceRange = explode('-', $search['price']);
            unset($search['price']);

            if ($paginate['showPage'] || $paginate['orderBy']) {
                if ($paginate['showPage'] && $paginate['orderBy']) {

                    $data = Product::where($search)->whereBetween('oldPrice', $priceRange)->orderBy($paginate['orderBy'], 'asc')->paginate($paginate['showPage']);
                } elseif ($paginate['showPage']) {

                    $data = Product::where($search)->whereBetween('oldPrice', $priceRange)->paginate($paginate['showPage']);
                } else {

                    $data = Product::where($search)->whereBetween('oldPrice', $priceRange)->orderBy($paginate['orderBy'], 'asc')->paginate(9);
                }

            } else {
                $data = Product::where($search)->whereBetween('oldPrice', $priceRange)->paginate(9);

            }
        } elseif (isset($search['price'])) {
            $priceRange = explode('-', $search['price']);
            unset($search['price']);
            unset($search['rating']);
            if ($paginate['showPage'] || $paginate['orderBy']) {
                if ($paginate['showPage'] && $paginate['orderBy']) {

                    $data = Product::where($search)->whereBetween('newPrice', $priceRange)->orderBy($paginate['orderBy'], 'asc')->paginate($paginate['showPage']);
                } elseif ($paginate['showPage']) {

                    $data = Product::where($search)->whereBetween('newPrice', $priceRange)->paginate($paginate['showPage']);
                } else {

                    $data = Product::where($search)->whereBetween('newPrice', $priceRange)->orderBy($paginate['orderBy'], 'asc')->paginate(9);
                }

            } else {
                $data = Product::where($search)->whereBetween('newPrice', $priceRange)->paginate(9);

            }
        } elseif (isset($search['rating'])) {
            $ratedStar=$search['rating'];
            unset($search['price']);
            unset($search['rating']);
            if ($paginate['showPage'] || $paginate['orderBy']) {
                if ($paginate['showPage'] && $paginate['orderBy']) {

                    $data = Product::where($search)->with('rates')->whereHas('rates',function($query) use ($ratedStar){$query->where('ratedStar',$ratedStar);})->orderBy($paginate['orderBy'], 'asc')->paginate($paginate['showPage']);

                } elseif ($paginate['showPage']) {

                    $data = Product::where($search)->with('rates')->whereHas('rates',function($query) use ($ratedStar){$query->where('ratedStar',$ratedStar);})->paginate($paginate['showPage']);
                } else {


                    $data = Product::where($search)->with('rates')->whereHas('rates',function($query) use ($ratedStar){$query->where('ratedStar',$ratedStar);})->orderBy($paginate['orderBy'], 'asc')->paginate(9);
                }

            } else {
                $data = Product::where($search)->with('rates')->whereHas('rates',function($query) use ($ratedStar){$query->where('ratedStar',$ratedStar);})->paginate(9);

            }
        } else {
            unset($search['price']);
            unset($search['rating']);

            if ($paginate['showPage'] || $paginate['orderBy']) {
                if ($paginate['showPage'] && $paginate['orderBy']) {

                    $data = Product::where($search)->orderBy($paginate['orderBy'], 'asc')->paginate($paginate['showPage']);
                } elseif ($paginate['showPage']) {

                    $data = Product::where($search)->paginate($paginate['showPage']);
                } else {

                    $data = Product::where($search)->orderBy($paginate['orderBy'], 'asc')->paginate(9);
                }


            } else {
                $data = Product::where($search)->paginate(9);
            }
        }


        return $data;

    }

    public function viewProduct($product_id)
    {


        $this->data['detailProduct'] = $this->getDetailProduct($product_id);
        return view('web.product.quickView', $this->data);


    }

    public function getDetailProduct($product_id, $selectedVariationId = null)
    {

        $data = Product::where('id', $product_id)->with(['category', 'parentCategory', 'subCategory', 'secondSubCategory', 'productVariation', 'productVariation.galleries'])->first();
        $data['title'] = 'Product Detail';

        if ($selectedVariationId) {
            $productVariation = ProductVariation::where(['product_id' => $product_id, 'id' => $selectedVariationId, 'status' => 'active'])->with(['galleries' => function ($query) {
                $query->where('status', 'active');
            }])->first();
        } else {
            $productVariation = ProductVariation::where(['product_id' => $product_id, 'status' => 'active'])->with(['galleries' => function ($query) {
                $query->where('status', 'active');
            }])->first();
        }
        $sizes = [];
        if ($productVariation['size']) {
            foreach (explode(',', $productVariation['size']) as $key => $productSize) {
                $size = ProductSize::find($productSize);
                $sizes[$key]['name'] = $size->name;
                $sizes[$key]['id'] = $size->id;
            }

        }
        $data['sizes'] = $sizes;
        $data['selectedVariationDetail'] = $productVariation;
        $data['relatedProducts'] = Product::where(['status' => 'active', 'category_id' => $data->category_id, 'parent_category_id' => $data->parent_category_id, 'sub_category_id' => $data->sub_category_id])->where('id','!=',$product_id)->orderByRaw("RAND()")->get()->take(3);
        return $data;

    }

    public function detailProduct($product_id, $selectedVariationId = null)
    {

        $this->data['detailProduct'] = $this->getDetailProduct($product_id, $selectedVariationId);
        $this->data['title'] = 'Product Detail';
        $this->data['detailProduct']['reviews']=$this->selectRatingStar($product_id);

//        rated data
        if(Auth::guard('web')->check()){
            $ratedStar=ProductRated::where(['userId'=>Auth::user()->id,'productId'=>$product_id])->first();
            $this->data['ratedProduct']=$ratedStar;
        }
        return view('web.product.detail', $this->data);
    }
    public function selectRatingStar($productId){
        $data['total']=ProductRated::where('productId',$productId)->count();

            $data['relatedReviews']=ProductRated::with('user')->where('productId',$productId)->take(3)->orderBy('ratedStar','desc')->get();




//        $star_1=$this->countStar($productId,1);
//        $star_2=$this->countStar($productId,2);
//        $star_3=$this->countStar($productId,3);
//        $star_4=$this->countStar($productId,4);
//        $star_5=$this->countStar($productId,5);
//        if($star_5 > $star_4 && $star_5 > $star_3 &&  $star_5 >$star_2 && $star_5>$star_1){
//            $data['ratedStar']=5;
//        }elseif($star_4>$star_5 && $star_4>$star_3 &&  $star_4>$star_2 && $star_4>$star_1){
//            $data['ratedStar']=4;
//        }elseif($star_3>$star_5 && $star_3>$star_4 &&  $star_3>$star_2 && $star_3>$star_1){
//            $data['ratedStar']=3;
//        }elseif($star_2>$star_5 && $star_2>$star_4 &&  $star_2>$star_3 && $star_2>$star_1){
//            $data['ratedStar']=2;
//        }elseif($star_1>$star_5 && $star_1>$star_4 &&  $star_1>$star_3 && $star_1>$star_2){
//            $data['ratedStar']=1;
//        }else{
//            $data['ratedStar']=0;
//        }
       return $data;




    }
    public function countStar($productId,$ratedStar){
        return ProductRated::where(['productId'=>$productId,'ratedStar'=>$ratedStar])->count();
    }
    public function ratingProduct(Request $request){
        $request->validate([
            'ratedStar'=>'required',
            'title'=>'required',
            'review'=>'required',
            'productId'=>'required',

        ]);

        $formInput=$request->all();

        $formInput['userId']=Auth::user()->id;
        $countOrderProduct=PlaceOrder::where(['userId'=>$formInput['userId'],'productId'=>$formInput['productId']])->count();

        if($countOrderProduct > 0){
            //            insert  rating in product table
            $repeatedStar=ProductRated::where(['productId'=>$formInput['productId']])->orderBy('ratedStar','desc')->get()->groupBy('ratedStar',function($values){

                return $values->ratedStar;
            });
            $ratedStar=1;
            foreach($repeatedStar as $star=>$ratedStar){
                $ratedStar=$star;
                break;
            }
            Product::find($formInput['productId'])->update(['rating'=>$ratedStar]);
            if($formInput['id']){

                $result=ProductRated::find($formInput['id'])->update($formInput);
            }else{
                $result=ProductRated::create($formInput);
            }
        }else{
            Session::flash('error','Sorry You hav not Order this product yet....So cannot Review it');
            return redirect()->back();
        }



        if($result){

            Session::flash('success',' Review it');
        }else{
            Session::flash('error',' Review it');
        }
        return redirect()->back();
    }
}
