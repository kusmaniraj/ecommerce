<?php

namespace App\Http\Controllers\Web;


use App\MenuModel;
use App\ProductAttribute;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Session;

class SearchController extends Controller
{
    private $data;

    use Shared;

    public function __construct()
    {

        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);
        //shared listCategories function
        $this->data['listCategories'] = $this->listCategories();
        $this->data['title'] = 'Search ';


    }

    public function search($listType, $searchWord, $type = null, $categoryId = null)
    {
        $paginate = [
            'showPage' => request()->showPage,
            'orderBy' => request()->orderBy,

        ];
        if ($paginate) {
            $this->data['categoryProducts'] = $this->searchList($searchWord, $type, $categoryId, $paginate);
        } else {
            $this->data['categoryProducts'] = $this->searchList($searchWord, $type, $categoryId);
        }


        if ($listType == 'list') {
            return view('web.product.list', $this->data);
        } else {
            return view('web.product.grid', $this->data);
        }


    }
    public function searchList($searchWord, $type = null, $selectedCategoryId = null, $paginate = null)
    {
        $data['category'] = "";
        $data['parent_category'] = "";
        $data['sub_category'] = "";
        $data['categoryType'] = "";
        $data['second_sub_category'] = "";
        $data['searchCategoryName'] = $selectedCategoryId;
        $data['searchWord'] = $searchWord;
        $this->data['title'] = 'Search';

        if ($type && $selectedCategoryId) {
            if ($type == 'category') {
                if ($paginate['showPage'] && $paginate['orderBy']) {
                    $data['products'] = Product::where(['status' => 'active', 'category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->orderBy($paginate['orderBy'], 'asc')->paginate($paginate['showPage']);
                } elseif ($paginate['showPage']) {

                    $data['products'] = Product::where(['status' => 'active', 'category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->paginate($paginate['showPage']);
                } elseif ($paginate['orderBy']) {
                    $data['products'] = Product::where(['status' => 'active', 'category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->orderBy($paginate['orderBy'], 'asc')->paginate(9);
                } else {
                    $data['products'] = Product::where(['status' => 'active', 'category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->paginate(9);
                }

            } elseif ('parent_category') {
                if ($paginate['showPage'] && $paginate['orderBy']) {
                    $data['products'] = Product::where(['status' => 'active', 'parent_category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->orderBy($paginate['orderBy'], 'asc')->paginate($paginate['showPage']);
                } elseif ($paginate['showPage']) {

                    $data['products'] = Product::where(['status' => 'active', 'parent_category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->paginate($paginate['showPage']);
                } elseif ($paginate['orderBy']) {
                    $data['products'] = Product::where(['status' => 'active', 'parent_category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->orderBy($paginate['orderBy'], 'asc')->paginate(9);
                } else {
                    $data['products'] = Product::where(['status' => 'active', 'parent_category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->paginate(9);
                }

            } elseif ('sub_category') {
                if ($paginate['showPage'] && $paginate['orderBy']) {
                    $data['products'] = Product::where(['status' => 'active', 'sub_category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->orderBy($paginate['orderBy'], 'asc')->paginate($paginate['showPage']);
                } elseif ($paginate['showPage']) {

                    $data['products'] = Product::where(['status' => 'active', 'sub_category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->paginate($paginate['showPage']);
                } elseif ($paginate['orderBy']) {
                    $data['products'] = Product::where(['status' => 'active', 'sub_category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->orderBy($paginate['orderBy'], 'asc')->paginate(9);
                } else {
                    $data['products'] = Product::where(['status' => 'active', 'sub_category_id' => $selectedCategoryId])->where('name', 'like', '%' . $searchWord . '%')->paginate(9);
                }

            }


        } else {

            if ($paginate['showPage'] && $paginate['orderBy']) {
                $data['products'] = Product::where(['status' => 'active'])->where('name', 'like', '%' . $searchWord . '%')->orderBy($paginate['orderBy'], 'asc')->paginate($paginate['showPage']);
            } elseif ($paginate['showPage']) {

                $data['products'] = Product::where(['status' => 'active'])->where('name', 'like', '%' . $searchWord . '%')->paginate($paginate['showPage']);
            } elseif ($paginate['orderBy']) {
                $data['products'] = Product::where(['status' => 'active'])->where('name', 'like', '%' . $searchWord . '%')->orderBy($paginate['orderBy'], 'asc')->paginate(1);
            } else {
                $data['products'] = Product::where(['status' => 'active'])->where('name', 'like', '%' . $searchWord . '%')->paginate(9);
            }


        }
        $data['categories'] = Category::where(['status' => 'active', 'parent_category_id' => null])->with(['category_products' => function ($query) {
            $query->where(['status' => 'active']);
        }])->get();

        return $data;
    }
}
