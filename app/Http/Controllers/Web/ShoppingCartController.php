<?php

namespace App\Http\Controllers\Web;


use App\MenuModel;
use App\Product;
use App\ProductAttribute;
use App\ProductSize;
use App\ProductVariation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;
use Illuminate\Support\Facades\Auth;
use Session;

class ShoppingCartController extends Controller
{
    private $data;

    use Shared;

    public function __construct()
    {

        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);

        //shared listCategories function
        $this->data['listCategories'] = $this->listCategories();
        $this->data['title']='Shopping Cart ';


    }

    public function index()
    {
        if (Session::has('cart')) {
            if(count(Session::get('cart')) >0){
                $this->data['sessionCart']=Session::get('cart');
            }



        }



        return view('web/shopping_cart/list', $this->data);

    }




    public function getQty($productVariationId,$sizeId=null)
    {
         $productQty['selectedQty']="";
        if($sizeId){

            $productVariation = ProductVariation::where('id', $productVariationId)->first();
            $sizes=explode(',',$productVariation['size']);
            $quantities=explode(',',$productVariation['qty']);

          foreach ($sizes as $key => $size) {
             if($size==$sizeId){
                   $productQty['selectedQty']=$quantities[$key];
                   
               }

          }

           
        }else{
            $productVariation = ProductVariation::where('id', $productVariationId)->first();
             $productQty['selectedQty']=$productVariation['qty'];
        }

        return $productQty;

    }

    public function addToCart(Request $request)
    {
        $cartData['selectedQty']=1;
        $cart_list_array = [];
        $formData = $request->all();
        $productVariation = ProductVariation::where('id',$formData['product_variation_id'])->with('product')->first();
        $cartData['variationWithProduct']=$productVariation;
        $cartData['product_id']=$formData['product_id'];
        $cartData['product_variation_id']=$formData['product_variation_id'];
        $cartData['maxQty']=$formData['maxQty'];
        $cartData['selectedQty']=$formData['qty'];
        $cartData['selectedSizeId']=$formData['size_id'];
       if(Auth::guard('web')->check()){
           $cartData['userId']=Auth::user()->id;
       }
        if( $cartData['selectedSizeId']){
            $cartData['selectedSizeName']=ProductSize::find( $cartData['selectedSizeId'])->name;
        }


        if (isset($productVariation['product']['newPrice'])) {
            $cartData['sub_total'] = $formData['qty'] * $productVariation['product']['newPrice'];
        } else {
            $cartData['sub_total'] = $formData['qty'] * $productVariation['product']['oldPrice'];
        }


        if (Session::has('cart')) {
            $sessionCart = Session::get('cart');
            if (isset($sessionCart)) {
                foreach ($sessionCart as $cart) {
                    if(isset($cart['selectedSizeId']) && isset($cartData['selectedSizeId']) ){
                        if ( $cart['selectedSizeName'] ==  $cartData['selectedSizeName']) {
                            $messageData=['status'=>'error','message'=>'You had Already  Added Selected Size  Product to cart ','cart'=>$cartData];
                            Session::flash('cartMessage',$messageData);
                            return ['url'=>url()->previous()];

                        }

                    }else{
                        if ($cart['product_variation_id'] ==  $cartData['product_variation_id']) {
                            $messageData=['status'=>'error','message'=>' You had Already  Added Selected Size  Product to cart ','cart'=>$cartData];
                            Session::flash('cartMessage',$messageData);
                            return ['url'=>url()->previous()];


                        }
                    }







                }
            }
            Session::push('cart', $cartData);
        } else {
            array_push($cart_list_array, $cartData);
            Session::put('cart', $cart_list_array);
        }
        $messageData=['status'=>'success','message'=>'Added to Cart','cart'=>$cartData];
        Session::flash('cartMessage',$messageData);
        return ['url'=>url()->previous()];

    }
    public function removeFromCart($index){
        if(Session::has('cart')){
            $sessionList=Session::get('cart');
            unset($sessionList[$index]);
            Session::put('cart', $sessionList);
        }
        return redirect()->back();

    }
    public function changeQty(Request $request){

        if(Session::has('cart')){
            $sessionList=Session::get('cart');
           foreach($sessionList as $key=>$list){
               if($list['product_id']==$request->product_id && $list['product_variation_id']==$request->product_variation_id){

                   $sessionList[$key]['selectedQty']=$request->qty;
                   if($sessionList[$key]['variationWithProduct']['product']['newPrice']){
                       $sessionList[$key]['sub_total']=$request->qty *$sessionList[$key]['variationWithProduct']['product']['newPrice'];
                   }else{
                       $sessionList[$key]['sub_total']=$request->qty *$sessionList[$key]['variationWithProduct']['product']['oldPrice'];
                   }

                   Session::put('cart', $sessionList);
                   return ['success'=>'change Qty','url'=>url()->previous()];
                   break;


               }

           }

        }

    }

    public function destroySession()
    {
        Session::forget('cart');
    }
}
