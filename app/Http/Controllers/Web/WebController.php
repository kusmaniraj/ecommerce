<?php

namespace App\Http\Controllers\Web;

use App\Category;

use App\MenuModel;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Shared;


class WebController extends Controller
{
    private $data;

    use Shared;

    public function __construct()
    {

        //Traits
        $this->data['getSetting'] = $this->getSetting();
        $menuLists = MenuModel::select('id', 'name as text', 'parent_id', 'position', 'url')->where(['status' => 'active'])->orderBy('position')->get()->toArray();
        $menus = $this->buildTree($menuLists);
        $this->data['menuTree'] = $this->buildMenu($menus);
//shared listCategories function
        $this->data['listCategories'] = $this->listCategories();


    }

    public function index()
    {
        $this->data['title'] = 'Home';
        $this->data['specialProducts'] = Product::where(['specialProduct' => 'yes', 'status' => 'active'])->get();
        $this->data['featuredProducts'] = Product::where(['featuredProduct' => 'yes', 'status' => 'active'])->get();
        $this->data['sliders'] = Product::where(['slider' => 'yes', 'status' => 'active'])->get();
        return view('web.index', $this->data);
    }








}