<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'driver'=>'required',
            'host'=>'required',
            'port'=>'required|numeric',
            'username'=>'required',
            'password'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'driver.required' => 'Mail Driver is required',
            'host.required' => 'Mail Host is required',
            'port.required' => 'Mail Port is required',
            'username.required' => 'Mailer Username is required',
            'password.required' => 'Mailer Password is required',

        ];
    }
}
