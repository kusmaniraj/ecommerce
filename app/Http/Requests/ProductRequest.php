<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_category_id'=>'required',
            'sub_category_id'=>'required',
            'name'=>'required|max:50',
            'description'=>'required',
            'oldPrice'=>'required',
            'variationType'=>'required',
            'featuredImg'=>'required',



        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Product Name is required',
            'description.required'  => 'Product Description is required',
            'sub_category_id.required' =>'Sub Category Need be Select',
            'parent_category_id.required' =>'Parent Category Need to be Select',
            'variationType.required'=>'Please Select Variation',
            'oldPrice.required'  => 'Product Price is required',
            'featuredImg.required'  => 'Featured Image is required',



        ];
    }
}
