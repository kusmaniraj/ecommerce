<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductVariationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qty'=>'required',
            'featuredImg'=>'required',







        ];
    }
    public function messages()
    {
        return [
            'featuredImg.required' => 'ProductVariation Featured Image is required',

            'qty.required' =>'Qty is required',




        ];
    }
}
