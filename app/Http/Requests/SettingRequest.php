<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'website_name'=>'required|string|max:125',
            'website_name_short'=>'required|regex:/[A-Z]/',
            'owner_contact'=>'required|numeric|digits:10',
            'version'=>'required|numeric|digits:2',
            'owner_email'=>'required|email',
            'owner_name'=>'required|max:50',
            'owner_address'=>'required|max:50',

        ];
    }
//    public function messages()
//    {
//        return [
//            'contact.required' => 'Size Name is required',
//
//        ];
//    }
}
