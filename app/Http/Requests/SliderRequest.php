<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'shortDescription'=>'required',
            'featuredImg'=>'required',
            'category_id'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'category_id.required' => 'Category Name is required',

        ];
    }
}
