<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image_IconSetting extends Model
{
protected $table="image_icon_settings";
    protected $fillable=['id','original_image_url','thumb_image_url','image_icon_type'];

}
