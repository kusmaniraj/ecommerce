<?php

namespace App\Mail;

use App\BillingInfo;
use App\Product;
use App\ProductVariation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Traits\Shared;
use Carbon\Carbon;

class OrderMail extends Mailable
{
    use Shared;
    use Queueable, SerializesModels;
protected  $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sendUser,$orderProduct)
    {
        $setting=$this->getSetting();
        $this->data['receiver']=$setting;
        $this->data['sender']=BillingInfo::where('userEmail',$sendUser['email'])->first();;
        $this->data['orderProducts']=$orderProduct;

      $this->data['orderList']=$this->orderProductList($orderProduct);

    }
    public function orderProductList($order){

            $orderList=$order;
        $orderList['date'] = Carbon::now()->toDateTimeString();
            $priceArray=explode(',',$order['price']);
            $qtyArray=explode(',',$order['qty']);
            $sizeArray=explode(',',$order['size']);
            $productIdArray=explode(',',$order['productId']);
            $productVariationIdArray=explode(',',$order['productVariationId']);
            $productNameArray=[];
            $productVariationNameArray=[];

            $orderList['orderPrice']=$priceArray;
            $orderList['orderQty']=$qtyArray;
            $orderList['orderSizeName']=$sizeArray;
            foreach($productIdArray as $productId){
                array_push($productNameArray,Product::find($productId));
            }
            foreach($productVariationIdArray as $productVariationId){
                array_push($productVariationNameArray,ProductVariation::find($productVariationId));
            }
            $orderList['product']=$productNameArray;
            $orderList['productVariation']=$productVariationNameArray;
             return $orderList;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->data['subject']='Order Products';
        return $this->from($this->data['sender']['userEmail'])
                     ->view('mail.order',$this->data);
    }
}
