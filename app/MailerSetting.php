<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailerSetting extends Model
{
    protected $table='mailer_settings';
    protected $fillable=['driver','host','port','encryption','username','password'];
    protected $hidden = [
        'password'
    ];
}
