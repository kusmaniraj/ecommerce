<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaceOrder extends Model
{
    protected $table='place_orders';
    protected $fillable=['id','productId','productVariationId','size','qty','email','userId','price','paymentMethod','billingInfoId','shippingInfoId','status'];
    public function product(){
        return $this->belongsTo('App\Product','productId','id');
    }
    public function productVariation(){
        return $this->belongsTo('App\ProductVariation','productVariationId','id');
    }
    public function productSize(){
        return $this->belongsTo('App\ProductSize','size','id');
    }
    public function billingInformation(){
        return $this->belongsTo('App\BillingInfo','billingInfoId','id');
    }
    public function shippingInformation(){
        return $this->belongsTo('App\shippingInfo','shippingInfoId','id');
    }
}

