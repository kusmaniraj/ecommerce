<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';
    protected $fillable=['id','category_id','parent_category_id','sub_category_id','second_sub_category_id','name','description','oldPrice','newPrice','specification','featuredImg','rating','variationType','metaKeywords','metaDescription','status','specialProduct','featuredProduct','slider'];
    public function productVariation(){
        return $this->hasMany('App\ProductVariation','product_id','id');
    }

    public function category(){
        return $this->belongsTo('App\Category','category_id','id');
    }

    public function parentCategory(){
        return $this->belongsTo('App\Category','parent_category_id','id');
    }

    public function subCategory(){
        return $this->belongsTo('App\Category','sub_category_id','id');
    }
    public function secondSubCategory(){
        return $this->belongsTo('App\Category','second_sub_category_id','id');
    }
    public function rates(){
        return $this->hasMany('App\ProductRated','productId','id');
    }



}
