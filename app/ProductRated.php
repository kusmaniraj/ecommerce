<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRated extends Model
{
    protected $table='product_rateds';
    protected $fillable=['id','userId','ratedStar','review','title','productId'];
    public function user(){
        return $this->belongsTo('App\User','userId','id');

    }
    public function product(){
        return $this->belongsTo('App\Product','productId','id');
    }
}
