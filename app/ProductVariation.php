<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
    protected $table="product_variations";
    protected  $fillable=['id','featuredImg','product_id','size','qty','status','type'];
    public function product(){
        return $this->belongsTo('App\Product','product_id','id');
    }
    public function galleries(){
        return $this->hasMany('App\Gallery','product_variation_id','id');
    }
}
