<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingInfo extends Model
{
    protected $table='shipping_infos';
    protected $fillable=['id','userId','userEmail','firstName','lastName','email','address','region','city','telephone','fax','gender','default'];
}
