<?php namespace App\Traits;

use App\Category;
use App\Http\Controllers\Admin\NotificationController;
use App\PlaceOrder;
use App\Product;
use App\ProductColor;
use App\ProductSize;
use App\ProductVariation;
use App\Setting;


trait Shared
{
    public function getSetting()
    {
        return Setting::first();
    }

    public function getBookingNotifications(){
        $orderList=PlaceOrder::where(['status'=>'requested'])->with(['billingInformation','shippingInformation'])->get();


        foreach($orderList as $key=>$order){
            $priceArray=explode(',',$order->price);
            $qtyArray=explode(',',$order->qty);
            $sizeArray=explode(',',$order->size);
            $productIdArray=explode(',',$order->productId);
            $productVariationIdArray=explode(',',$order->productVariationId);
            $productNameArray=[];
            $productVariationNameArray=[];

            $orderList[$key]['orderPrice']=$priceArray;
            $orderList[$key]['orderQty']=$qtyArray;
            $orderList[$key]['orderSizeName']=$sizeArray;
            foreach($productIdArray as $productId){
                array_push($productNameArray,Product::find($productId));
            }
            foreach($productVariationIdArray as $productVariationId){
                array_push($productVariationNameArray,ProductVariation::find($productVariationId));
            }
            $orderList[$key]['products']=$productNameArray;
            $orderList[$key]['productVariations']=$productVariationNameArray;

        };
        return $orderList;
    }

    public function buildTree(array $elements, $parentId = 0)
    {
        $branch = array();
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    function printNav($menu, $sub = false)
    {
        foreach ($menu as $nav) {
            $link_attrs = "";
            $caret = "";
            $drop_menu = "";

            if (isset($nav['children'])) {
                // $link_attrs = 'class="dropdown-toggle hvr-bounce-to-bottom" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"';
                // $caret = '<span class="caret"></span>';
                $drop_menu = '<ul>' . $this->printNav($nav['children'], true) . '</ul>';
            }

            echo '<li>';
            echo '<a href="' . $nav['url'] . '" ' . $link_attrs . ' >' . '<i class="icon-circle"></i>' . $nav['text'] . ' ' . $caret . '</a>';
            echo $drop_menu;
            echo '</li>';
        }

    }

    function buildMenu(array $menu_array)
    {

        $menu = '<ul>';
        $parent_ul_attr='';
        $sub_ul_attr='';
        foreach ($menu_array as $i => $parent_attrs) {
            $parent_ul_attr.= '<li class="mt-root demo_custom_link_cms">';
            $parent_ul_attr .= '<div class="mt-root-item">' .
                '<a href="' . $parent_attrs["url"] . '">' .
                '<div class="title title_font"><span class="title-text">' . $parent_attrs["text"] . '</span></div>' .
                '</a>' .
                '</div>';

            if (isset($parent_attrs['children'])) {
                $sub_ul_attr .= '<ul class="menu-items col-md-3 col-sm-4 col-xs-12">';
                foreach ($parent_attrs['children'] as $j => $sub_attrs) {
                    $sub_ul_attr .= '<li class="menu-item depth-1">';
                    $sub_ul_attr .= '<div class="title"> <a href="' . $sub_attrs["url"] . '"><span>' . $sub_attrs["text"] . '</span></a></div>';
                    $sub_ul_attr .= '</li>';
                }
                $parent_ul_attr .=$sub_ul_attr .'</ul>';
            }


            $parent_ul_attr .= '</li>';




        }
        $menu .= $parent_ul_attr . '</ul>';


        return $menu;
    }

    function productColors(){
        return ProductColor::all();
    }
    function productSizes(){
        return ProductSize::all();
    }
    function getThumb($path)
    {
        $str_to_insert = '/thumbs';
        $oldstr = $path;
        $pos = strrpos($path,"/");

        return substr_replace($oldstr, $str_to_insert, $pos, 0);
    }
    public function listCategories()
    {
        return Category::with(['parentCategories' => function ($query) {
            $query->where('status', 'active')->where('parent_category_id', '!=',null)->where(['sub_category_id'=>null,'second_sub_category_id'=>null]);
        }, 'parentCategories.subCategories' => function ($query) {
            $query->where('status', 'active')->where('parent_category_id', '!=', null)->where('sub_category_id','!=', null)->where(['second_sub_category_id'=>null,'status'=>'active']);
        },'subCategories.secondSubCategories' => function ($query) {
            $query->where('status', 'active')->where('parent_category_id', '!=', null)->where('sub_category_id','!=', null)->where('second_sub_category_id','!=',null);
        }])->where(['parent_category_id'=>null,'sub_category_id'=>null,'second_sub_category_id'=>null,'status'=>'active'])->get();
    }






}
