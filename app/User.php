<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','providerType','providerId',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function AccountInformation(){
        return $this->hasOne('App\BillingInfo','userId','id');
    }
    public function shippingInfos(){
        return $this->hasMany('App\ShippingInfo','userEmail','email');
    }

}
