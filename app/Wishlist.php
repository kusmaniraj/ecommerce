<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table='wishlists';
    protected $fillable=['id','userId','productId','productVariationId','maxQty','qty','size'];
    public function product(){
        return $this->belongsTo('App\Product','productId','id');
    }
    public function productVariation(){
        return $this->belongsTo('App\ProductVariation','productVariationId','id');
    }
    public function productSize(){
        return $this->belongsTo('App\ProductSize','size','id');
    }
}
