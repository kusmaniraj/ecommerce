<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('category_id')->unsigned();
            $table->integer('parent_category_id')->unsigned();
            $table->integer('sub_category_id')->unsigned();
            $table->integer('second_sub_category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sub_category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('parent_category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('second_sub_category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name');
            $table->longText('description');
            $table->longText('specification')->nullable();
            $table->string('featuredImg')->nullable();
            $table->integer('rating')->nullable();
            $table->double('oldPrice');
            $table->double('newPrice')->nullable();
            $table->string('metaKeywords')->nullable();
            $table->string('metaDescription')->nullable();
            $table->enum('slider',['yes','no'])->default('no');
            $table->enum('specialProduct',['yes','no'])->default('no');
            $table->enum('featuredProduct',['yes','no'])->default('no');
            $table->enum('variationType',['simple','advance']);
            $table->enum('status',['active','inactive'])->default('active');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
