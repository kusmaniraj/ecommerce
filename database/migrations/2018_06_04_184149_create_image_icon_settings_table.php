<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageIconSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_icon_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('image_icon_type');
            $table->string('original_image_url');
            $table->string('thumb_image_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_icon_settings');
    }
}
