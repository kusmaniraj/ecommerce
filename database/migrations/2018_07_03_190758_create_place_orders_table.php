<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('email');
            $table->string('size')->nullable();
            $table->string('price');
            $table->string('qty');

            $table->integer('userId')->nullable();
            $table->string('productId');
            $table->string('productVariationId');
            $table->integer('billingInfoId')->unsigned();
            $table->integer('shippingInfoId')->nullable();
            $table->enum('status',['requested','pending','delivered','canceled'])->default('requested');




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place_orders');
    }
}
