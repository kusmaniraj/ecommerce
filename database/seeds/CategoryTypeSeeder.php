<?php

use Illuminate\Database\Seeder;

class CategoryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryTypeData=[
            0=>[
            'name' => 'Computer and Laptop',
                'icon_name'=>'desktop'

            ],
            1=>[
                'name' => 'Men',
                'icon_name'=>'male'

            ],
            ];
        foreach($categoryTypeData as $value){
            DB::table('category_types')->insert($value);
        }

    }
}
