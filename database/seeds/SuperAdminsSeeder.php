<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperAdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('admins')->insert([
            'name' => 'admin',
            'email' => 'superadmin@superadmin.com',
            'type'	=>'0',
            'password' => bcrypt('password'),
        ]);
    }
}
