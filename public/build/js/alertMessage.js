var alertMessage = function (alertStatus, data) {



    this.printMessage = function () {
        let status = alertStatus;
        let messageName = data;

        var alert_msg = '';
        if (status == 'success') {
            $('#alertMessage').removeClass('alert-danger');
            alertify.success("Successfully!!" + data);
            alert_msg = '<strong>Successfully !!</strong> &nbsp;' +messageName;
        } else {
            $('#alertMessage').removeClass('alert-success');
            status='danger';
            alertify.error("Error!!" + data);
            alert_msg = '<strong>Error !!</strong> &nbsp;' + messageName;
        }


        $('#alertMessage').show().addClass('alert-' + status).html(alert_msg);
        window.setTimeout(function () {
            $('#alertMessage').slideUp(1000, function () {
                window.scrollTo(10, 10);
                $(this).hide();

            });
        }, 3000);

    }

}
