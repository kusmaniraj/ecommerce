var category_module = new category();
function category() {


    var $self = this;
    var dataTable="";
    var dataTableSelector='#firstCategoryTable';
    var csrfToken=$('meta[name="csrf-token"]').attr('content');
    var $firstTableName=$('#firstCategoryTable');
    var $secondTableName=$('#secondCategoryTable');
    var $thirdTableName=$('#thirdCategoryTable');
    var $fourthTableName=$('#fourthCategoryTable');
    var $formModal=$('#categoryModal');
    var $formName=$('#categoryForm');
    var categoryId;
    var parentCategoryId;
    var subCategoryId;




    this.bindFunction = function () {


        // *******************First Category Bind Function*********************




        $firstTableName.on('click','#showSecondCategoryPage',function(){
            categoryId=$(this).data('id');
            $self.showPage('#secondCategoryPage','#firstCategoryPage');

            dataTableSelector='#secondCategoryTable';
            $self.getCategory(categoryId);

            $formName.find('input[name="parent_category_id"]').val(categoryId);

        })



        $firstTableName.on('click','.editCategoryBtn',function(){
            var id=$(this).data('id');

            $self.hideError();
            //    change form Status
            $self.changeFormStatus('Update');
            $self.edit(id);
            $formModal.modal('show');


        })
        $firstTableName.on('click','.delCategoryBtn',function(){
            var id=$(this).data('id');
            if(confirm('Are You sure want to remove ?')==false){
                return false;
            }else{
                $self.delete(id);
            }


        })

    // *******************Second Category Bind Function*********************

        $secondTableName.on('click','#showThirdCategoryPage',function(e){
            e.preventDefault();
            categoryId=$(this).data('category_id');
            parentCategoryId=$(this).data('id');



            $self.showPage('#thirdCategoryPage','#secondCategoryPage');

            dataTableSelector='#thirdCategoryTable';
            $self.getCategory(categoryId,parentCategoryId);


            $formName.find('input[name="parent_category_id"]').val(categoryId);
            $formName.find('input[name="sub_category_id"]').val(parentCategoryId);



        })
        $('#backFirstCategoryPage').on('click',function(){
            categoryId="";
            parentCategoryId="";
            subCategoryId='';
            $formName.find('input[name="parent_category_id"]').val("");
            $formName.find('input[name="sub_category_id"]').val("");
            $formName.find('input[name="second_sub_category_id"]').val("");

            $self.showPage('#firstCategoryPage','#secondCategoryPage');

            dataTableSelector='#firstCategoryTable';

            $self.getCategory();


        })



        $secondTableName.on('click','.editCategoryBtn',function(){
            var id=$(this).data('id');

            $self.hideError();
            //    change form Status
            $self.changeFormStatus('Update');
            $self.edit(id);
            $formModal.modal('show');


        })
        $secondTableName.on('click','.delCategoryBtn',function(){
            var id=$(this).data('id');
            if(confirm('Are You sure want to remove ?')==false){
                return false;
            }else{
                $self.delete(id);
            }


        })

        // *******************Third Category Bind Function*********************

        $thirdTableName.on('click','#showFourthCategoryPage',function(){

            categoryId=$(this).data('category_id');
            parentCategoryId=$(this).data('parent_category_id');
            subCategoryId=$(this).data('id');

            $self.showPage('#fourthCategoryPage','#thirdCategoryPage');

            dataTableSelector='#fourthCategoryTable';
            $self.getCategory(categoryId,parentCategoryId,subCategoryId);
            $formName.find('input[name="parent_category_id"]').val(categoryId);
            $formName.find('input[name="sub_category_id"]').val(parentCategoryId);
            $formName.find('input[name="second_sub_category_id"]').val(subCategoryId);



        })
        //back to second Page
        $('#backSecondCategoryPage').on('click',function(){
            parentCategoryId="";
            subCategoryId='';
            $formName.find('input[name="sub_category_id"]').val("");
            $formName.find('input[name="second_sub_category_id"]').val("");
            $self.showPage('#secondCategoryPage','#thirdCategoryPage');

            dataTableSelector='#secondCategoryTable';

            $self.getCategory(categoryId);


        })



        $thirdTableName.on('click','.editCategoryBtn',function(){
            var id=$(this).data('id');

            $self.hideError();
            //    change form Status
            $self.changeFormStatus('Update');
            $self.edit(id);
            $formModal.modal('show');


        })
        $thirdTableName.on('click','.delCategoryBtn',function(){
            var id=$(this).data('id');
            if(confirm('Are You sure want to remove ?')==false){
                return false;
            }else{
                $self.delete(id);
            }


        })
        // *******************Fourth Category Bind Function*********************


        //back to third page
        $('#backThirdCategoryPage').on('click',function(){
            subCategoryId='';
            $formName.find('input[name="second_sub_category_id"]').val("");
            $self.showPage('#thirdCategoryPage','#fourthCategoryPage');
            dataTableSelector='#thirdCategoryTable';
            $self.getCategory(categoryId,parentCategoryId);


        })



        $fourthTableName.on('click','.editCategoryBtn',function(){
            var id=$(this).data('id');

            $self.hideError();
            //    change form Status
            $self.changeFormStatus('Update');
            $self.edit(id);
            $formModal.modal('show');


        })
        $fourthTableName.on('click','.delCategoryBtn',function(){
            var id=$(this).data('id');
            if(confirm('Are You sure want to remove ?')==false){
                return false;
            }else{
                $self.delete(id);
            }


        })

    //    **************************Common Bind Function****************************************
        $('.addCategoryBtn').on('click',function(){


            $formModal.modal('show');
            //    change form Status
            $self.changeFormStatus('Create');
            $self.emptyForm();

            $self.hideError();

        });

        $formName.on('submit',function(e){
            e.preventDefault();
            $self.hideError();
            var id=$formName.find('input[name="id"]').val();

            if(id){
                let formData = $(this).serialize() + '&_token=' + csrfToken;
                $self.update(formData,id);
            }else{
                let formData = $(this).serialize() + '&_token=' + csrfToken;
                $self.store(formData);
            }


        })



    }

    this.setDataTable=function(selector){
         dataTable= $(selector).DataTable({
            processing: true,
        });
    }

    //*******Show Pages********************************//
   this.showPage=function(nexPageId,previousPageId){
       $(previousPageId).hide('slide', {direction: 'left'}, 500);
       window.setTimeout(function () {
           $(nexPageId).show('slide', {direction: 'left'}, 500);





       }, 300);
       return true;
   }
    //*********************End of Show Pages*******************************
    //************************Crud Category***********
    this.getCategory=function(categoryId,parentCategoryId,subCategoryId){
        var url;
        if(categoryId && parentCategoryId && subCategoryId){
            url=base_url + "/admin/category/getSecondSubCategories/"+categoryId+'/'+parentCategoryId+'/'+subCategoryId;
        }else if(categoryId && parentCategoryId){
            url=base_url + "/admin/category/getSubCategories/"+categoryId+'/'+parentCategoryId;

        }else if(categoryId){
            url=base_url + "/admin/category/getParentCategories/"+categoryId;


        }else{
            url=base_url + "/admin/category/getCategories/";

        }
        new ajaxHelper([url, "", "get", ""]).makeAjaxCall().then(function (data) {
            if(data){
                var categoryTableData="";
                $.each(data,function(i,v){
                    categoryTableData +='<tr class="category_'+v.id+'">' +
                        '<td>'+(i+1)+'</td>'+
                        '<td>'+v.name+'</td>';
                    if(v.parent_categories){
                        categoryTableData += '<td> <a data-id="'+v.id+'" id="showSecondCategoryPage" href="#"><i class="fa fa-plus"></i>&nbsp;Add Second Categories&nbsp;<label class="badge badge-dark">'+v.parent_categories.length+'</label></a> </td>';
                    }else if(v.sub_categories){
                        categoryTableData += '<td> <a  id="showThirdCategoryPage" data-id="'+v.id+'" data-category_id="'+v.parent_category_id+'" href="#"><i class="fa fa-plus"></i>&nbsp;Add Third Categories&nbsp;<label class="badge badge-dark">'+v.sub_categories.length+'</label></a> </td>';
                    }else if(v.second_sub_categories){
                        categoryTableData += '<td> <a id="showFourthCategoryPage" data-id="'+v.id+'" data-category_id="'+v.parent_category_id+'" data-parent_category_id="'+v.sub_category_id+'" href="#"><i class="fa fa-plus"></i>&nbsp;Add Fourth Categories&nbsp;<label class="badge badge-dark">'+v.second_sub_categories.length+'</label></a> </td>';
                    }else{
                        categoryTableData +='<td>None</td>';
                    }

                    categoryTableData +='<td><a href="#" data-id="'+v.id+'" class="btn btn-primary editCategoryBtn"><i class="fa fa-edit"></i></a>'+
                        '<a href="#"  data-id="'+v.id+'" class="btn btn-danger delCategoryBtn"><i class="fa fa-remove"></i></a></td>'+
                        '</tr>';
                })
                if(dataTable !=""){
                    dataTable.destroy();
                }

                $(dataTableSelector).find('tbody').html(categoryTableData);
                $self.setDataTable(dataTableSelector);
            }

        });

    }
    this.store=function(formData){


        //console.log(formData);
        new ajaxHelper([base_url + "/admin/category/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
               $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                if(categoryId && parentCategoryId && subCategoryId){
                    $self.getCategory(categoryId,parentCategoryId,subCategoryId);
                }else if(categoryId && parentCategoryId){
                    $self.getCategory(categoryId,parentCategoryId);
                }else if(categoryId){
                    $self.getCategory(categoryId);
                }else{
                    $self.getCategory();
                }
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        },function(response){
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }
    this.delete=function(id){


        new ajaxHelper([base_url + "/admin/category/delete/"+id, {_token:csrfToken,id:id}, "DELETE", ""]).makeAjaxCall().then(function (data) {
            if(data.success){

                new alertMessage('success', data.success).printMessage();
                if(categoryId && parentCategoryId && subCategoryId){
                    $self.getCategory(categoryId,parentCategoryId,subCategoryId);
                }else if(categoryId && parentCategoryId){
                    $self.getCategory(categoryId,parentCategoryId);
                }else if(categoryId){
                    $self.getCategory(categoryId);
                }else{
                    $self.getCategory();
                }
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        })
    }
    this.edit=function(id){


        new ajaxHelper([base_url + "/admin/category/"+id+"/edit", "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data);
            $formName.find('input[name="id"]').val(data.id);
            $formName.find('input[name="name"]').val(data.name);

            $formName.find('input[name="status"][value="' + data.status + '"]').prop('checked', true);

        })
    }
    this.update=function(formData,id){

        //console.log(formData);
        new ajaxHelper([base_url + "/admin/category/"+id, formData, "put", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                if(categoryId && parentCategoryId && subCategoryId){
                    $self.getCategory(categoryId,parentCategoryId,subCategoryId);
                }else if(categoryId && parentCategoryId){
                    $self.getCategory(categoryId,parentCategoryId);
                }else if(categoryId){
                    $self.getCategory(categoryId);
                }else{
                    $self.getCategory();
                }
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        },function(response){
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }



    //************End of Crud Function****************

    this.changeFormStatus=function(type){
        if(categoryId && parentCategoryId && subCategoryId){
            $formModal.find('.modal-title').text(type+' Second Sub Category Form');
            $formModal.find('button[type="submit"]').text(type+' Second Sub Category');
        }else if(categoryId && parentCategoryId){
            $formModal.find('.modal-title').text(type+' Sub Category Form');
            $formModal.find('button[type="submit"]').text(type+' Sub Category');
        }else if(categoryId){
            $formModal.find('.modal-title').text(type+' Parent Category Form');
            $formModal.find('button[type="submit"]').text(type+'Parent Category');
        }else{
            $formModal.find('.modal-title').text(type+' Category Form');
            $formModal.find('button[type="submit"]').text(type+' Category');
        }

    }

    //form validation error print
    this.formValidations = function (data) {
        var errorMsg='';
        $.each(data, function (i, value) {
            errorMsg+='<li>'+value+'</li>';
        })
        $formName.prepend('<div class="alert alert-danger form_errors text-danger">'+errorMsg+'</div>');


    }
    this.hideError=function(){
        $formName.find('.form_errors').hide();

    }
    this.emptyForm=function(){

        $formName.find('input[name="id"]').val('');
        $formName.find('input[name="name"]').val('');
        $formName.find('input[name="status"]').prop('active');
        if(categoryId && parentCategoryId && subCategoryId){
           return true;
        }else if(categoryId && parentCategoryId){
            $formName.find('input[name="second_sub_category_id"]').val('');
        }else if(categoryId){
            $formName.find('input[name="second_sub_category_id"]').val('');
            $formName.find('input[name="sub_category_id"]').val('');

        }else{
            $formName.find('input[name="second_sub_category_id"]').val('');
            $formName.find('input[name="sub_category_id"]').val('');
            $formName.find('input[name="parent_category_id"]').val('');
        }





    }


    this.init = function () {
        $self.bindFunction();
        $self.getCategory();



    }();

}

