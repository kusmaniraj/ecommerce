var color_module = new color();
function color() {


    var $self = this;
    var dataTable;
    var colorPicker;
    var csrfToken=$('meta[name="csrf-token"]').attr('content');
    var $tableName=$('#colorTable');
    var $formModal=$('#colorModal');
    var $formName=$('#colorForm');





    this.bindFunction = function () {


        $self.getColor();


        $('#addColorBtn').on('click',function(){
            $formModal.modal('show');
            //    change form Status
            $self.changeFormStatus('Create');
            $self.emptyForm();
            $self.hideError();




        })

        $formName.on('submit',function(e){
            e.preventDefault();
            $self.hideError();
            var id=$formName.find('input[name="id"]').val();
            let formData = $formName.serialize() + '&_token=' + csrfToken;
            if(id){
                $self.update(id,formData);
            }else{
                $self.store(formData);
            }


        })
        $tableName.on('click','.editColorBtn',function(){
            var id=$(this).data('id');

            $self.hideError();
            //    change form Status
            $self.changeFormStatus('Update');
            $self.edit(id);
            $formModal.modal('show');


        })
        $tableName.on('click','.delColorBtn',function(){
            var id=$(this).data('id');
            if(confirm('Are You sure want to remove ?')==false){
                return false;
            }else{
                $self.delete(id);
            }


        })



    }

    this.setDataTable=function(){
        dataTable= $tableName.DataTable({
            processing: true,
        });
    }
    this.initalizeColorPicker=function(){
        colorPicker=$formName.find('.attr_color').colorpicker();
    }
    //store
    this.getColor=function(){

        new ajaxHelper([base_url + "/admin/color/getColors/", "", "get", ""]).makeAjaxCall().then(function (data) {
            if(data){
                var colorTableData="";
                $.each(data,function(i,v){
                    colorTableData +='<tr>' +
                        '<td>'+(i+1)+'</td>'+
                        '<td>'+v.code+'</td>'+
                        '<td>'+v.name+'</td>';



                    colorTableData +='<td><a href="#" data-id="'+v.id+'" class="btn btn-primary editColorBtn"><i class="fa fa-edit"></i></a>'+
                        '<a href="#"  data-id="'+v.id+'" class="btn btn-danger delColorBtn"><i class="fa fa-remove"></i></a></td>'+
                        '</tr>';
                });
                dataTable.destroy();
                $tableName.find('tbody').html(colorTableData);
                $self.setDataTable();
            }

        });

    }
    this.store=function(formData){


        //console.log(formData);
        new ajaxHelper([base_url + "/admin/color/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                $self.getColor();
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        },function(response){
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }
    this.delete=function(id){


        new ajaxHelper([base_url + "/admin/color/delete/"+id, {_token:csrfToken,id:id}, "DELETE", ""]).makeAjaxCall().then(function (data) {
            if(data.success){

                new alertMessage('success', data.success).printMessage();
                $self.getColor();
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        })
    }
    this.edit=function(id){


        new ajaxHelper([base_url + "/admin/color/"+id+"/edit", "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data);
            $formName.find('input[name="id"]').val(data.id);
            $formName.find('input[name="code"]').val(data.code);
            $formName.find('input[name="name"]').val(data.name);

            $formName.find('input[name="status"][value="' + data.status + '"]').prop('checked', true);

        })
    }
    this.update=function(id,formData){

        //console.log(formData);
        new ajaxHelper([base_url + "/admin/color/"+id, formData, "put", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                $self.getColor();
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        },function(response){
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }





    this.changeFormStatus=function(type){
        $formModal.find('.modal-title').text(type+' Color Form');
        $formModal.find('button[type="submit"]').text(type+' Color');

    }

    //form validation error print
    this.formValidations = function (data) {
        var errorMsg='';
        $.each(data, function (i, value) {
            errorMsg+='<li>'+value+'</li>';
        })
        $formName.prepend('<div class="alert alert-danger form_errors text-danger">'+errorMsg+'</div>');


    }
    this.hideError=function(){
        $formName.find('.form_errors').hide();

    }
    this.emptyForm=function(){
        $formName[0].reset();
        $formName.find('input[name="id"]').val('');




    }


    this.init = function () {
        $self.bindFunction();
        $self.setDataTable();
        $self.initalizeColorPicker();


    }();

}

