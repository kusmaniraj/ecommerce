var imageIconSetting_module = new imageIconSetting();
function imageIconSetting() {


    var $self = this;
    var csrfToken=$('meta[name="csrf-token"]').attr('content');







    this.bindFunction = function () {






        $('#logoImageForm').on('submit',function(e){
            e.preventDefault();
            $self.hideError(this);
            var logo_id=$(this).find('input[name="id"]').val();

            if(logo_id){
                $self.update(logo_id,this);
            }else{
                $self.store(this);
            }


        })
        $('#noImageForm').on('submit',function(e){
            e.preventDefault();
            $self.hideError(this);
            var id=$(this).find('input[name="id"]').val();

            if(id){
                $self.update(id,this);
            }else{
                $self.store(this);
            }


        })
        $('#faviconForm').on('submit',function(e){
            e.preventDefault();
            $self.hideError(this);
            var id=$(this).find('input[name="id"]').val();

            if(id){
                $self.update(id,this);
            }else{
                $self.store(this);
            }


        })




    }




    this.store=function(formSelector){
        let formData = $(formSelector).serialize() + '&_token=' + csrfToken;

        //console.log(formData);
        new ajaxHelper([base_url + "/admin/setting/image_icon/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if(data.success){

                new alertMessage('success', data.success).printMessage();

            }else{
                new alertMessage('error', data.error).printMessage();
            }

        },function(response){
            //console.log(response.responseJSON.errors);
            $self.formValidations(formSelector,response.responseJSON.errors);
        })
    }


    this.update=function(id,formSelector){
        let formData = $(formSelector).serialize() + '&_token=' + csrfToken;

        //console.log(formData);
        new ajaxHelper([base_url + "/admin/setting/image_icon/"+id, formData, "put", ""]).makeAjaxCall().then(function (data) {
            if(data.success){

                new alertMessage('success', data.success).printMessage();

            }else{
                new alertMessage('error', data.error).printMessage();
            }

        },function(response){
            //console.log(response.responseJSON.errors);
            $self.formValidations(formSelector,response.responseJSON.errors);
        })
    }







    //form validation error print
    this.formValidations = function (selector,data) {
        var errorMsg='';
        $.each(data, function (i, value) {
            errorMsg+='<li>'+value+'</li>';
        })
        $(selector).prepend('<div class="alert alert-danger form_errors text-danger">'+errorMsg+'</div>');


    }
    this.hideError=function(selector){
        $(selector).find('.form_errors').hide();

    }



    this.init = function () {
      $self.bindFunction();

    }();

}

