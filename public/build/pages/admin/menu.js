var menu_module = new menu();
function menu() {


    var $self = this;
    var $jstree = $('#tree');


    this.bindFunction = function () {
        //Dynamic intialized tree
        $self.getMenusTree();
        //expand tree
        $('#openAllTree').on('click', function () {

            $jstree.jstree('open_all');
        });
        //expand tree
        $('#closeAllTree').on('click', function () {
            $jstree.jstree('close_all');
        });
        $('#closeAllTree').on('click', function () {
            $jstree.jstree(true).refresh(true);
        });
        //reset Menu form
        $('.menuTitle').on('click','#resetForm' ,function () {
            $('.menuSubmitBtn').text('Add Menu');
            $('.menuTitle').html('Add Menu Form');
            //remove error
            $self.hideError();
        //    empty form input/img
            $self.emptyForm('#menuForm');


        })
        //search
        $("#searchTree").keyup(function () {

            var searchString = $(this).val();
            $jstree.jstree('search', searchString);
        });
        $('#saveChangesBtn').on('click', function (e) {
            var menu = $jstree.jstree(true).get_json('#', {flat: false});
            treeExport = [];
            debunkTree(menu, 0);
//        console.log(treeExport);

            var params = {treeExport: treeExport, _token: $('meta[name="csrf-token"]').attr('content')}
            new ajaxHelper([base_url + "/admin/menu/saveMenu", params, "post", ""]).makeAjaxCall().then(function (data) {
                //window.location.href = base_url+"/admin/menus";

                $self.getMenusTree();

            });
        })

    }

    //store Menu
    $('#menuForm').on('submit', function (e) {
        e.preventDefault();
        var formData = $(this).serialize();
        var id = $('#menuForm input[name="id"]').val();
        if (id) {
            $self.updateMenu(id, formData);

        } else {
            $self.storeMenu(formData);
        }

    })

    //end bind function
    this.getMenusTree = function () {
        $jstree.jstree('destroy');
        new ajaxHelper([base_url + "/admin/getMenusTree", "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data);

            $(function () {

                $jstree.jstree({
                    'plugins': ['contextmenu', 'search', 'dnd'],
                    'core': {
                        'data': data,
                        'themes': {
                            'icons': true
                        },
                        check_callback: true
                    },
                    'contextmenu': {
                        'items': $self.customMenu
                    },
                    "search": {
                        "case_insensitive": true,
                        "show_only_matches": true
                    },

                }).on('loaded.jstree', function () {
                    $jstree.jstree('open_all');
                });
            });
        })
    }

    this.customMenu = function (node) {
        var items = {

            editItem: {
                label: "Edit",
                action: function () {
                    new ajaxHelper([base_url + "/admin/menu/" + node.id + '/edit', "", "get", ""]).makeAjaxCall().then(function (resp) {

                        //console.log(data);
                        $self.editMenu(resp)

                    });
                }
            },
            deleteItem: {
                label: "Delete",
                action: function () {
                    if (node.children.length) {
                        alert('Can not delete the category which have child categories.');
                        return;
                    }
                    new ajaxHelper([base_url + "/admin/menu/delete/" + node.id, "", "get", ""]).makeAjaxCall().then(function (data) {

                        //console.log(data);
                        if (confirm('Are you Sure Want to delete menu') == true) {

                            $self.getMenusTree();
                            if(data.success){
                                new alertMessage('success', data.success).printMessage();
                            }else{
                                new alertMessage('error', data.error).printMessage();
                            }

                            return true;
                        } else {
                            return false;
                        }
                    });


//
                }
            }
        };
        return items;
    }


    //edit Selected Menu
    this.editMenu = function (data) {
        $('#menuForm input[name="name"]').val(data.name);
        $('#menuForm input[name="url"]').val(data.url);
        $('#menuForm input[name="id"]').val(data.id);
        $('#menuForm input[name="status"][value="' + data.status + '"]').prop('checked', true);
        $('#menuForm input[name="oldFeaturedImg"]').val(data.featuredImg);

        $('.menuSubmitBtn').text('Edit Menu');
        var menuTitle='Edit Menu Form  '+
            '<span><a href="#" id="resetForm" class="btn btn-info btn-sm">' +
            '<i class="fa fa-plus-circle"></i>&nbsp;New Menu</a></span>';
        $('.menuTitle').html(menuTitle);
        if (data.featuredImg) {
            $('#holder').removeClass('hidden').attr('src', base_url + data.featuredImg).attr('class','thumbnail');
        }


    }

    var treeExport = [];
    var debunkTree = function (menu, parentID) {

        $.each(menu, function (index, data) {
            if (data.children.length) {
                debunkTree(data.children, data.id);
            }

            treeExport.push({
                id: data.id,
                name: data.text,
                parent_id: parentID,


            });
        });
    };

    this.storeMenu = function (formData) {
        new ajaxHelper([base_url + "/admin/menu/store/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
                $self.getMenusTree();
               $self.hideError();
                //    empty form input/img
                $self.emptyForm('#menuForm');
                new alertMessage('success', data.success).printMessage();
            }else if(data.error){
                new alertMessage('error', data.error).printMessage();
            }else{
                $self.formValidations('#menuForm', data.validation_errors);
            }



        });
    }

    this.updateMenu = function (id, formData) {
        new ajaxHelper([base_url + "/admin/menu/update/" + id, formData, "post", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
                $self.getMenusTree();
               $self.hideError();
                new alertMessage('success', data.success).printMessage();
            }else if(data.error){
                new alertMessage('error', data.error).printMessage();
            }else{
                $self.formValidations('#menuForm', data.validation_errors);
            }

        });
    }

    //form validation error print
    this.formValidations = function (selector, data) {
       var errorMsg='';
        $.each(data, function (i, value) {
            errorMsg+='<li>'+value+'</li>';
        })
        $(selector +' .form_errors').show().html(errorMsg);


    }
    this.hideError=function(){
        $('#menuForm .form_errors').hide();
    }
    this.emptyForm=function(selector){
        $(selector+' input[name="name"],input[name="url"],input[name="id"],input[name="featuredImg"],input[name="oldFeaturedImg"]').val('');
        $(selector+' img').attr('src', '');
    }


    this.init = function () {


        $self.bindFunction();

    }();

}

