var order = new Order();
function Order() {


    var $self = this;
    var dataTable="";

    var pageName=$('#orderPage');
    var orderStatus=['pending','requested','delivered','canceled']

    $tableName = $('#orderTable');
    var $billingOrShippingInfoModal = $('#billingOrShippingInfoModal');
    var $productOrderInfoModal = $('#productOrderInfoModal');

    this.bindFunction = function() {

        $tableName.on('click', '.orderProductInfoBtn', function () {
            var orderProductInfoId = $(this).data('info_id');
            var orderProductInfoType = $(this).data('type');
            $self.showInfo(orderProductInfoType, orderProductInfoId);
        });
        $tableName.on('click', '.billingInfoBtn', function () {
            var billingInfoId = $(this).data('info_id');
            var billingInfoType = $(this).data('type');
            $self.showInfo(billingInfoType, billingInfoId);
        });
        $tableName.on('click', '.shippingInfoBtn', function () {
            var shippingInfoId = $(this).data('info_id');
            var shippingInfoType = $(this).data('type');
            $self.showInfo(shippingInfoType, shippingInfoId);
        })

        $tableName.on('change','select[name="status"]',function(){

            var id=$(this).data('id');
            var statusVal=$(this).val();
            $self.changeStatus(id,statusVal);


        })
        //    requested orders
        pageName.on('click','#all',function(){
            $self.getOrders('all');
            pageName.find('.page-title .title_left').html('<h3>Orders </h3>');
        })
    //    requested orders
       pageName.on('click','#requestedOrders',function(){
           $self.getOrders('requested');
           pageName.find('.page-title .title_left').html('<h3>Orders | <small>Requested</small></h3>');
       })

        //    pending orders
        pageName.on('click','#pendingOrders',function(){
            $self.getOrders('pending');
            pageName.find('.page-title .title_left').html('<h3>Orders | <small>Pending</small></h3>');
        })
        //    delivered orders
        pageName.on('click','#deliveredOrders',function(){
            $self.getOrders('delivered');
            pageName.find('.page-title .title_left').html('<h3>Orders | <small>Delivered</small></h3>');
        })
        //    canceled orders
        pageName.on('click','#canceledOrders',function(){
            $self.getOrders('canceled');
            pageName.find('.page-title .title_left').html('<h3>Orders | <small>Canceled</small></h3>');
        })

    }
    this.getOrders = function (status) {

        var url = "";
        if (status == "" || status == 'all') {
            url = base_url + "/admin/orders/getOrders/";

        } else {
            url = base_url + "/admin/orders/getOrders/" + status;
        }
        if (dataTable !="") {
            dataTable.destroy();
            $tableName.find('tbody').html('');
        }
        var orderListHtml = '';
        new ajaxHelper([url, "", "get", ""]).makeAjaxCall().then(function (data) {


            //console.log(data);

            $.each(data, function (i, v) {
                orderListHtml += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +
                    '<td>' + v.created_at + '</td>'+
                '<td>' + v.email + '</td>';
                orderListHtml += '<td>';
                for (var j = 0; j < v.productVariation.length; j++) {
                    orderListHtml += '<img height="20px" width="20px" src="' + base_url + v.productVariation[j]['featuredImg'] + '" alt="' + v.product[j]['name'] + '">  <hr>';
                }
                orderListHtml += '</td>';

                orderListHtml += '<td>';
                for (var j = 0; j < v.product.length; j++) {
                    orderListHtml += '<p>' + v.product[j]['name'] + '</p> <hr>';
                    if (v.orderSizeName[j]) {
                        orderListHtml += '<p> Size:&nbsp;' + v.orderSizeName[j] + '</p> <hr>';
                    }
                }
                orderListHtml += '</td>';

                orderListHtml += '<td>';
                for (var j = 0; j < v.orderQty.length; j++) {
                    orderListHtml += '<p>' + v.orderQty[j] + '</p> <hr>';

                }
                orderListHtml += '</td>';

                orderListHtml += '<td>';
                for (var j = 0; j < v.orderPrice.length; j++) {
                    orderListHtml += '<p> Rs: &nbsp;' + v.orderPrice[j] + '</p> <hr>';

                }
                orderListHtml += '</td>';
                orderListHtml += ' <td>' +
                    '<div class="form-group">' +
                    '<select name="status" class="form-control" data-id="'+v.id+'">';
                orderStatus.forEach(e=>{

                    if(v.status==e){
                        orderListHtml += '<option selected value="'+e+'">'+e+'</option>';

                    }else{
                        orderListHtml += '<option value="'+e+'">'+e+'</option>';
                    }
                })






                orderListHtml +=  '</select>' +
                    '</div>' +
                    '</td>';
                orderListHtml += ' <td>' +
                    ' <a href="#" class="orderProductInfoBtn" data-type="orderProductInfo" data-info_id="' + v.id + '"><i class="fa fa-eye text-info">&nbsp;ProductInformation</i></a>' +
                    '</br>' +
                    '<a href="#" class="billingInfoBtn" data-type="billingInfo"data-info_id="' + v.billingInfoId + '"><i class="fa fa-book text-info">&nbsp;BillingInformation</i></a>' +
                    '</br>' +
                    '<a href="#" class="shippingInfoBtn" data-type="shippingInfo"data-info_id="' + v.shippingInfoId + '"><i class="fa fa-book text-info">&nbsp;ShippingInformation</i></a>' +

                    '</td>';

                orderListHtml += '</tr>';


            })

            $tableName.find('tbody').html(orderListHtml);

            $self.setDataTable();

        });


    }
    this.showInfo = function (infoType, id) {


        new ajaxHelper([base_url + "/admin/orders/getInfo/" + infoType + "/" + id, "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data)
            if (infoType == 'orderProductInfo') {
                var productOrderHtml = '';
                $productOrderInfoModal.modal('show');
                $productOrderInfoModal.find('.modal-title').text('Product Order Information ');
                var totalQty = 0;
                var totalPrice = 0;


                for (var i = 0; i < data.productVariation.length; i++) {
                    totalQty += parseInt(data.orderQty[i]);
                    totalPrice += parseInt(data.orderPrice[i]);
                    productOrderHtml += '<tr>';
                    productOrderHtml += '<td><img src="' + base_url + data.productVariation[i]['featuredImg'] + '" height="100px" width="100px" alt="' + data.product[i]['name'] + '"></br></td>';
                    productOrderHtml += '<td><p>' + data.product[i]['name'] + '</p></br>';
                    if (data.orderSizeName[i]) {
                        productOrderHtml += '<p> Size=&nbsp;' + data.orderSizeName[0] + '</p>';
                    }
                    productOrderHtml += '</td>';

                    productOrderHtml += '<td><p>' + data.orderQty[i] + '</p></td>';
                    productOrderHtml += '<td><p> Rs: ' + data.orderPrice[i] + '</p></td>';
                    productOrderHtml += '</tr>';

                }

                productOrderHtml += '<tr>' +
                    '<td colsmall="2" ><p class="pull-right"><strong>Total</strong></p></td>' +
                    '<td>' + totalQty + '</td>' +
                    '<td> Rs: ' + totalPrice + '</td>' +

                    '</tr>';


                $productOrderInfoModal.find('#productOrderTable tbody').html(productOrderHtml);

            } else {
                $billingOrShippingInfoModal.modal('show');
                if (infoType == 'billingInfo') {
                    $billingOrShippingInfoModal.find('.modal-title').text('Billing Information ');
                } else if (infoType == 'shippingInfo') {
                    $billingOrShippingInfoModal.find('.modal-title').text('Shipping Information ');
                }

                $billingOrShippingInfoModal.find('p.name').html(data.firstName + '&nbsp;' + data.lastName);
                $billingOrShippingInfoModal.find('p.address').text(data.address);
                if (data.userEmail) {
                    $billingOrShippingInfoModal.find('p.email').text(data.userEmail);
                } else {
                    $billingOrShippingInfoModal.find('p.email').text(data.email);
                }
                $billingOrShippingInfoModal.find('p.gender').text(data.gender);
                $billingOrShippingInfoModal.find('p.telephone').text(data.telephone);
                $billingOrShippingInfoModal.find('p.region').text(data.region);
                $billingOrShippingInfoModal.find('p.city').text(data.city);
                $billingOrShippingInfoModal.find('p.fax').text(data.fax);
            }


        });


    }
    this.changeStatus=function(id,statusValue){
        new ajaxHelper([base_url + "/admin/orders/changeStatus/" +id+'/'+statusValue, "", "get", ""]).makeAjaxCall().then(function (data) {
            if(data.error){
                new alertMessage('error', data.error).printMessage();
            }else{
                new alertMessage('success', data.success).printMessage();
            }

        })


    }
    $self.setDataTable = function () {
        dataTable = $tableName.DataTable({
            processing: true,
        });
    }
    this.hideError = function () {
        $formName.find('.form_errors').hide();


    }

    this.init = function () {
        $self.bindFunction();

        $self.getOrders("all");


    }();


}

