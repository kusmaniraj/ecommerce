var parentCategory_module = new menu();
function menu() {


    var $self = this;
    var dataTable;
    var selectOptionPicker
    var csrfToken=$('meta[name="csrf-token"]').attr('content');
    var $tableName=$('#parentCategoryTable');
    var $formModal=$('#parentCategoryModal');
    var $formName=$('#parentCategoryForm');

    this.bindFunction = function () {
        $self.setDataTable();
        $self.getParentCategory();
        $('#addParentCategoryBtn').on('click',function(){
            $formModal.modal('show');
            //    change form Status
            $self.changeFormStatus('Create');
            $self.emptyForm();
            $self.hideError();
            $self.getCategoryList();




        })

        $formName.on('submit',function(e){
            e.preventDefault();
            $self.hideError();
            var id=$formName.find('input[name="id"]').val();
            if(id){
                $self.update(id);
            }else{
                $self.store();
            }


        })
        $tableName.on('click','.editParentCategoryBtn',function(){
            var id=$(this).data('id');

            $self.hideError();
            //    change form Status
            $self.changeFormStatus('Update');
            $self.getCategoryList();
            $self.edit(id);
            $formModal.modal('show');



        })
        $tableName.on('click','.delParentCategoryBtn',function(){
            var id=$(this).data('id');
            if(confirm('Are You sure want to remove ?')==false){
                return false;
            }else{
                $self.delete(id);
            }


        })



    }
    this.setSelectPicker=function(){
        selectOptionPicker=$('.selectpicker').selectpicker('refresh');
    }
    this.getCategoryList=function(){
        new ajaxHelper([base_url + "/admin/category/getCategories/", "", "get", ""]).makeAjaxCall().then(function (data) {
            if(data){
                var categoryTableData="<option value='0' disabled selected>Select Category</option>";
                $.each(data,function(i,v){
                    categoryTableData +='<option value="'+v.id+'">'+v.name+'</option> ';
                })
                $formName.find('select[name="parent_category_id"]').html(categoryTableData);
                $self.setSelectPicker();

            }

        });
    }
    this.setDataTable=function(){
        dataTable= $tableName.DataTable({
            processing: true,
        });
    }
    //store
    this.getParentCategory=function(){

        new ajaxHelper([base_url + "/admin/category/parentCategory/getParentCategories/", "", "get", ""]).makeAjaxCall().then(function (data) {
            if(data){
                var categoryTableData="";
                $.each(data,function(i,v){
                    categoryTableData +='<tr class="category_'+v.id+'">' +
                        '<td>'+(i+1)+'</td>'+
                        '<td>'+v.category.name+'</td>'+
                        '<td>'+v.name+'</td>'+

                        '<td><a href="#" data-id="'+v.id+'" class="btn btn-primary editParentCategoryBtn"><i class="fa fa-edit"></i></a>'+
                        '<a href="#"  data-id="'+v.id+'" class="btn btn-danger delParentCategoryBtn"><i class="fa fa-remove"></i></a></td>'+
                        '</tr>';
                })
                dataTable.destroy();
                $tableName.find('tbody').html(categoryTableData);
                $self.setDataTable();
            }

        });

    }
    this.store=function(){
        let formData=$formName.serialize()+'&_token='+csrfToken+'&category_id='+category_id;
        //console.log(formData);
        new ajaxHelper([base_url + "/admin/category/parentCategory/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                $self.getParentCategory();
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        },function(response){
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }
    this.delete=function(id){


        new ajaxHelper([base_url + "/admin/category/parentCategory/delete/"+id, {_token:csrfToken,id:id}, "DELETE", ""]).makeAjaxCall().then(function (data) {
            if(data.success){

                new alertMessage('success', data.success).printMessage();
                $self.getParentCategory();
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        })
    }
    this.edit=function(id){


        new ajaxHelper([base_url + "/admin/category/parentCategory/"+id+"/edit", "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data);
            $formName.find('input[name="id"]').val(data.id);
            $formName.find('select[name="parent_category_id"]').val(data.parent_category_id);
            $formName.find('input[name="name"]').val(data.name);
            $formName.find('textarea[name="description"]').text(data.description);
            $formName.find('input[name="status"][value="' + data.status + '"]').prop('checked', true);
            $self.setSelectPicker();

        })
    }
    this.update=function(id){
        let formData=$formName.serialize()+'&_token='+csrfToken;
        //console.log(formData);
        new ajaxHelper([base_url + "/admin/category/parentCategory/"+id, formData, "put", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                $self.getParentCategory();
            }else{
                new alertMessage('error', data.error).printMessage();
            }

        },function(response){
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }





    this.changeFormStatus=function(type){
        $formModal.find('.modal-title').text(type+' ParentCategory Form');
        $formModal.find('button[type="parentmit"]').text(type+' ParentCategory');
    }

    //form validation error print
    this.formValidations = function (data) {
        var errorMsg='';
        $.each(data, function (i, value) {
            errorMsg+='<li>'+value+'</li>';
        })
        $formName.prepend('<div class="alert alert-danger form_errors text-danger">'+errorMsg+'</div>');


    }
    this.hideError=function(){
        $formName.find('.form_errors').hide();

    }
    this.emptyForm=function(){
        $formName[0].reset();
        $formName.find('input[name="id"]').val('');
        $formName.find('textarea[name="description"]').text('');

    }


    this.init = function () {
        $self.bindFunction();


    }();

}

