var product_module = new product();
function product() {


    var $self = this;
    var dataTable;
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    var $tableName = $('#productTable');
    var $formModal = $('#productModal');
    var $formName = $('#productForm');
    var selectOptionPicker;
   $self.productName;


    this.bindFunction = function () {
        $self.setDataTable();


        $self.getProduct();
        $('#addProductBtn').on('click', function () {
            //set Ckeditor
            $self.setCkeditor('#description_textarea');
            $self.setCkeditor('#specification_textarea');


            //    change form Status
            $self.changeFormStatus('Create');
            $self.emptyForm();
            $self.hideError();


            $self.getCategoryList();
            $self.setSelectPicker();
            $formModal.modal('show');


        })

        $formName.on('submit', function (e) {
            e.preventDefault();
            $self.hideError();
            var id = $formName.find('input[name="id"]').val();
            if (id) {
                $self.update(id);
            } else {
                $self.store();
            }


        })
        $tableName.on('click', '.editProductBtn', function () {
            var id = $(this).data('id');

            $self.hideError();
            //set Ckeditor
            $self.setCkeditor('#description_textarea');
            $self.setCkeditor('#specification_textarea');
            //    change form Status
            $self.changeFormStatus('Update');


            $self.edit(id);

            $formModal.modal('show');


        })
        $tableName.on('click', '.delProductBtn', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == false) {
                return false;
            } else {
                $self.delete(id);
            }


        })
        //show product Variation page
        $tableName.on('click', '.productVariationBtn', function () {
            var productId = $(this).data('id');
            var variationType = $(this).data('type');
            $self.productName=$(this).data('name');
            $self.showVariationPage(productId,variationType);
        })
        //$formName.on('click', '.productVariationBtn', function ()// {
        //    var productId = $(this).data('id'//);
        //    var variationType = $(this).data('type'//);
        //    var productName=$(this).data('name'//);
        //    $self.showVariationPage(productId, productName,variationType//);
        //    $formModal.modal('hide'//);
        //})


        $formName.on('change', 'select[name="category_id"]', function () {
            var category_id = $(this).val();
            $self.getParentCategoryList(category_id);
        })

        $formName.on('change', 'select[name="parent_category_id"]', function () {
            var category_id = $formName.find('select[name="category_id"]').val();
            var parent_category_id = $(this).val();
            $self.getSubCategoryList(category_id, parent_category_id);
        })
        $formName.on('change', 'select[name="sub_category_id"]', function () {
            var category_id = $formName.find('select[name="category_id"]').val();
            var parent_category_id = $formName.find('select[name="parent_category_id"]').val();
            var sub_category_id = $(this).val();
            $self.getSecondSubCategoryList(category_id, parent_category_id, sub_category_id);
        })


        //add Category
        $formName.find('.addCategoryBtn').on('click', function () {
            $('.addCategoryBtnDiv').hide('slide', {direction: 'left'}, 500);
            window.setTimeout(function () {
                $('.addCategoryFormDiv').show('slide', {direction: 'left'}, 500);
            }, 300);

        })
        $('.addProductCategoryBtn').on('click', function () {
            var name = $('#productCategoryForm').find('input[name="categoryName"]').val();
            if (name == "") {
                let errorMsg = '*Input Category Name*';
                $formName.prepend('<div class="alert alert-danger form_errors text-danger"><ul><li>' + errorMsg + '</li></ul></div>');

                return false;
            } else {
                let formData = '&_token=' + csrfToken + '&name=' + name;
                category_module.store(formData);
                $('#productCategoryForm').find('input').val('');
                $self.getCategoryList();
                return true
            }


        })

        $formName.find('.removeCategoryBtn').on('click', function () {
            $('.addCategoryFormDiv').hide('slide', {direction: 'left'}, 500);
            window.setTimeout(function () {
                $('.addCategoryBtnDiv').show('slide', {direction: 'left'}, 500);
                //productGallery_module.getGalleryList(id);


            }, 300);
        })
        //    end of add Category

        //add Parent Category
        $formName.find('.addProductParentCategoryBtn').on('click', function () {
            $('.addParentCategoryBtnDiv').hide('slide', {direction: 'left'}, 500);
            window.setTimeout(function () {
                $('.addParentCategoryFormDiv').show('slide', {direction: 'left'}, 500);
            }, 300);

        })
        $('.addParentCategoryBtn').on('click', function () {
            var name = $('#productParentCategoryForm').find('input[name="parentCategoryName"]').val();
            var categoryId = $formName.find('select[name="category_id"]').val();
            if (categoryId == null) {
                let errorMsg = '*Select  Category *';
                $formName.prepend('<div class="alert alert-danger form_errors text-danger"><ul><li>' + errorMsg + '</li></ul></div>');

                return false;
            } else if (name == "") {
                let errorMsg = '*Input Parent Category Name*';
                $formName.prepend('<div class="alert alert-danger form_errors text-danger"><ul><li>' + errorMsg + '</li></ul></div>');
                return false;
            } else {
                let formData = '&_token=' + csrfToken + '&name=' + name + '&parent_category_id=' + categoryId;
                category_module.store(formData);
                $('#productParentCategoryForm').find('input').val('');
                $self.getParentCategoryList(categoryId);
                return true
            }


        })

        $formName.find('.removeParentCategoryBtn').on('click', function () {
            $('.addParentCategoryFormDiv').hide('slide', {direction: 'left'}, 500);
            window.setTimeout(function () {
                $('.addParentCategoryBtnDiv').show('slide', {direction: 'left'}, 500);


            }, 300);
        })
        //    end of Parent Category

        //add Sub Category
        $formName.find('.addProductSubCategoryBtn').on('click', function () {
            $('.addSubCategoryBtnDiv').hide('slide', {direction: 'left'}, 500);
            window.setTimeout(function () {
                $('.addSubCategoryFormDiv').show('slide', {direction: 'left'}, 500);
            }, 300);

        })
        $('.addSubCategoryBtn').on('click', function () {
            var name = $('#productSubCategoryForm').find('input[name="subCategoryName"]').val();
            var categoryId = $formName.find('select[name="category_id"]').val();
            var parentCategoryId = $formName.find('select[name="parent_category_id"]').val();
            if (categoryId == null) {
                alert('*Select  Category *');
                return false;
            } else if (parentCategoryId == null) {
                alert('*Select  Parent Category  *');
                return false;
            } else if (name == "") {
                alert('*Input Sub Category Name*');
                return false;
            } else {
                let formData = '&_token=' + csrfToken + '&name=' + name + '&parent_category_id=' + categoryId + '&sub_category_id=' + parentCategoryId;
                category_module.store(formData);
                $('#productSubCategoryForm').find('input').val('');
                $self.getSubCategoryList(categoryId, parentCategoryId);
                return true
            }


        })

        $formName.find('.removeSubCategoryBtn').on('click', function () {
            $('.addSubCategoryFormDiv').hide('slide', {direction: 'left'}, 500);
            window.setTimeout(function () {
                $('.addSubCategoryBtnDiv').show('slide', {direction: 'left'}, 500);


            }, 300);
        })
        //    end of Sub Category

        //add Second Sub Category
        $formName.find('.addProductSecondSubCategoryBtn').on('click', function () {
            $('.addSecondSubCategoryBtnDiv').hide('slide', {direction: 'left'}, 500);
            window.setTimeout(function () {
                $('.addSecondSubCategoryFormDiv').show('slide', {direction: 'left'}, 500);
            }, 300);

        })
        $('.addSecondSubCategoryBtn').on('click', function () {
            var name = $('#productSecondSubCategoryForm').find('input[name="secondSubCategoryName"]').val();
            var categoryId = $formName.find('select[name="category_id"]').val();
            var parentCategoryId = $formName.find('select[name="parent_category_id"]').val();
            var subCategoryId = $formName.find('select[name="sub_category_id"]').val();

            if (categoryId == null) {
                alert('*Select  Category* ');
                return false;
            } else if (parentCategoryId == null) {
                alert('*Select  Parent Category *');
                return false;
            } else if (subCategoryId == null) {
                alert('*Select  Parent Category  *');
                return false;
            } else if (name == "") {
                alert('*Input Second Sub Category Name*');
                return false;
            } else {
                let formData = '&_token=' + csrfToken + '&name=' + name + '&parent_category_id=' + categoryId + '&sub_category_id=' + parentCategoryId + '&second_sub_category_id=' + subCategoryId;
                ;
                category_module.store(formData);
                $('#productSecondSubCategoryForm').find('input').val('');
                $self.getSecondSubCategoryList(categoryId, parentCategoryId, subCategoryId);
                return true
            }


        })

        $formName.find('.removeSecondSubCategoryBtn').on('click', function () {
            $('.addSecondSubCategoryFormDiv').hide('slide', {direction: 'left'}, 500);
            window.setTimeout(function () {
                $('.addSecondSubCategoryBtnDiv').show('slide', {direction: 'left'}, 500);


            }, 300);
        })
        //    end Second of Sub Category


    }


    this.setCkeditor = function (selector) {
        $(selector).ckeditor({
            toolbar: 'Full',
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P
        })

    }


    this.getCategoryList = function (editVal) {

        new ajaxHelper([base_url + "/admin/category/getCategories/", "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var categoryTableData = "<option value='0' disabled selected>Select  Category</option>";
                $.each(data, function (i, v) {
                    categoryTableData += '<option value="' + v.id + '">' + v.name + '</option> ';
                })
                $formName.find('select[name="category_id"]').html(categoryTableData);
                if (editVal) {
                    $formName.find('select[name="category_id"]').val(editVal)
                }
                $self.setSelectPicker();


            }

        });
    }
    this.getParentCategoryList = function (categoryId, editVal) {
        new ajaxHelper([base_url + "/admin/category/getParentCategories/" + categoryId, "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var categoryTableData = "<option value='0' disabled selected>Select Parent Category</option>";
                $.each(data, function (i, v) {
                    categoryTableData += '<option value="' + v.id + '">' + v.name + '</option> ';
                })
                $formName.find('select[name="parent_category_id"]').html(categoryTableData);
                if (editVal) {
                    $formName.find('select[name="parent_category_id"]').val(editVal)
                }
                $self.setSelectPicker();

            }

        });
    }
    this.getSubCategoryList = function (categoryId, parentCategoryId, editVal) {
        new ajaxHelper([base_url + "/admin/category/getSubCategories/" + categoryId + "/" + parentCategoryId, "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var categoryTableData = "<option value='0' disabled selected>Select Sub Category</option>";
                $.each(data, function (i, v) {
                    categoryTableData += '<option value="' + v.id + '">' + v.name + '</option> ';

                })
                $formName.find('select[name="sub_category_id"]').html(categoryTableData);
                if (editVal) {
                    $formName.find('select[name="sub_category_id"]').val(editVal)
                }
                $self.setSelectPicker();


            }

        });
    }
    this.getSecondSubCategoryList = function (categoryId, parentCategoryId, subCategoryId, editVal) {
        new ajaxHelper([base_url + "/admin/category/getSecondSubCategories/" + categoryId + "/" + parentCategoryId + '/' + subCategoryId, "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var categoryTableData = "<option value='0' disabled selected>Select Second Sub Category</option>";
                $.each(data, function (i, v) {
                    categoryTableData += '<option value="' + v.id + '">' + v.name + '</option> ';

                })
                $formName.find('select[name="second_sub_category_id"]').html(categoryTableData);
                if (editVal) {
                    $formName.find('select[name="second_sub_category_id"]').val(editVal)
                }
                $self.setSelectPicker();

            }

        });
    }
    this.setDataTable = function () {
        dataTable = $tableName.DataTable({
            processing: true,
        });
    }
    this.setSelectPicker = function () {
        selectOptionPicker = $('.selectpicker').selectpicker('refresh');

    }
    //store
    this.getProduct = function () {

        new ajaxHelper([base_url + "/admin/product/getProducts/", "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var productTableData = "";
                $.each(data, function (i, v) {
                    productTableData += '<tr class="product_' + v.id + '">' +
                        '<td>' + (i + 1) + '</td>' +
                            '<td><img src="'+base_url+v.featuredImg.replace('/shares/','/shares/thumbs/')+'" alt=" +v.name'+'" height="50px" width="50px"></td>'+
                        '<td>' + v.name + '</td>' +
                        '<td><a href="#" class="productVariationBtn btn btn-info btn-sm" data-name="'+v.name+'" data-type="' + v.variationType + '" data-id="' + v.id + '"><i class="fa fa-plus"></i>&nbsp;' + v.variationType + ' Variation('+v.product_variation.length+')</a></td>';
                    if (v.slider == 'yes') {
                        productTableData += '<td> <label class="label label-success" for="slider">' + v.slider + '</label></td>';
                    } else {
                        productTableData += '<td> <label class="label label-danger" for="slider">' + v.slider + '</label></td>';
                    }
                    if (v.featuredProduct == 'yes') {
                        productTableData += '<td> <label class="label label-success" for="featuredProduct">' + v.featuredProduct + '</label></td>';
                    } else {
                        productTableData += '<td> <label class="label label-danger" for="featuredProduct">' + v.featuredProduct + '</label></td>';
                    }
                    if (v.specialProduct == 'yes') {
                        productTableData += '<td> <label class="label label-success" for="specialProduct">' + v.specialProduct + '</label></td>';
                    } else {
                        productTableData += '<td> <label class="label label-danger" for="specialProduct">' + v.specialProduct + '</label></td>';
                    }
                    if (v.status == 'active') {
                        productTableData += '<td> <label class="label label-success" for="status">' + v.status + '</label></td>';
                    } else {
                        productTableData += '<td> <label class="label label-danger" for="status">' + v.status + '</label></td>';
                    }
                    productTableData += '<td><a  data-id="' + v.id + '" class="btn btn-primary editProductBtn"><i class="fa fa-edit"></i></a>' +
                        '<a  data-id="' + v.id + '" class="btn btn-danger delProductBtn"><i class="fa fa-remove"></i></a>' +
                        '</td>' +
                        '</tr>';
                })
                dataTable.destroy();
                $tableName.find('tbody').html(productTableData);
                $self.setDataTable();
            }

        });

    }
    this.store = function () {
        let formData = $formName.serialize() + '&_token=' + csrfToken;
        //console.log(formData);
        new ajaxHelper([base_url + "/admin/product/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                //shoe variation page

                $self.showVariationPage(data.inserteddata.id, data.insertedProduct.variationType);
                $formModal.modal('hide');
            } else {
                if (data.errors) {
                    $self.formValidations(data.errors);
                } else {
                    new alertMessage('error', data.error).printMessage();
                }
            }

        }, function (response) {
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }
    this.delete = function (id) {


        new ajaxHelper([base_url + "/admin/product/delete/" + id, {
            _token: csrfToken,
            id: id
        }, "DELETE", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {

                new alertMessage('success', data.success).printMessage();
                $self.getProduct();
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        })
    }
    this.edit = function (id) {


        new ajaxHelper([base_url + "/admin/product/" + id + "/edit", "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data);
            $formName.find('input[name="id"]').val(data.id);
            $formName.find('input[name="name"]').val(data.name);
            $formName.find('input[name="oldPrice"]').val(data.oldPrice);
            $formName.find('input[name="newPrice"]').val(data.newPrice);

            CKEDITOR.instances['description_textarea'].setData(data.description);
            $self.getCategoryList(data.category_id);
            $self.getParentCategoryList(data.category_id, data.parent_category_id);
            $self.getSubCategoryList(data.category_id, data.parent_category_id, data.sub_category_id);
            $self.getSecondSubCategoryList(data.category_id, data.parent_category_id, data.sub_category_id, data.second_sub_category_id);
            $formName.find('input[name="featuredImg"]').val(data.featuredImg);
            if (data.featuredImg) {
                $formName.find('#holder').attr('src', base_url + data.featuredImg).addClass('thumbnail');

            } else {
                $formName.find('#holder').attr('src', '').removeClass('thumbnail');
            }


            if (data.product_attributes) {
                $self.editProductAttributeList(data.product_attributes);
            }
            if (data.specification) {
                CKEDITOR.instances['specification_textarea'].setData(data.specification);
            }


            $formName.find('input[name="status"][value="' + data.status + '"]').prop('checked', true);
            $formName.find('input[name="featuredProduct"][value="' + data.featuredProduct + '"]').prop('checked', true);
            $formName.find('input[name="specialProduct"][value="' + data.specialProduct + '"]').prop('checked', true);
            $formName.find('input[name="slider"][value="' + data.slider + '"]').prop('checked', true);
            $formName.find('select[name="variationType"]').val(data.variationType);


        })
    }
    this.update = function (id) {
        let formData = $formName.serialize() + '&_token=' + csrfToken;
        //console.log(formData);
        new ajaxHelper([base_url + "/admin/product/" + id, formData, "put", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                $self.getProduct();
            } else {

                if (data.errors) {
                    $self.formValidations(data.errors);
                } else {
                    new alertMessage('error', data.error).printMessage();
                }

            }

        }, function (response) {
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }


    this.showVariationPage = function (id,type) {
        this.productId = id;


        $('#listProductsPage').hide('slide', {direction: 'left'}, 500);
        window.setTimeout(function () {
            $('#listProductVariationsPage').show('slide', {direction: 'left'}, 500);

            productVariation_module.getProductVariation(id,type);


        }, 300);
    }


    this.changeFormStatus = function (type) {
        $formModal.find('.modal-title').text(type + ' Product Form');
        $formModal.find('button[type="submit"]').text(type + ' Product');
    }

    //form validation error print
    this.formValidations = function (data) {
        var errorMsg = '';
        $.each(data, function (i, value) {
            errorMsg += '<li>' + value + '</li>';
        })
        $formName.prepend('<div class="alert alert-danger form_errors text-danger">' + errorMsg + '</div>');


    }
    this.hideError = function () {
        $formName.find('.form_errors').hide();


    }
    this.emptyForm = function () {
        $formName[0].reset();
        $formName.find('input[name="id"]').val('');
        $formName.find('input[name="featuredImg"]').val('');
        CKEDITOR.instances['description_textarea'].setData('');
        CKEDITOR.instances['specification_textarea'].setData('');
        $formName.find('select[name="sub_category_id"]').html("<option value='0' disabled selected>Select Sub Category</option>");
        $formName.find('select[name="variationType"]').val("0");
        $formName.find('#holder').attr('src', '').removeClass('thumbnail');


    }


    this.init = function () {
        $self.bindFunction();


    }();

}

