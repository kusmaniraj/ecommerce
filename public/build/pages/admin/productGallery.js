var productVariationGallery_module = new menu();
function menu() {


    var $self = this;
    var csrfToken = $('meta[name="csrf-token"]').attr('content');


    var $formName = $('#productVariationGalleryForm');
    var $galleryListSelector = $('#productVariationGalleryList');
    var $pageName=$('#productVariationGalleryPage');
    $self.product_variation_id;


    this.bindFunction = function () {


        $('.backToListProductVariationPage').on('click', function () {
            $self.showProductList();
        })
        $formName.on('submit', function (e) {

            e.preventDefault();
            $self.hideError();
            $self.store();


        })


        $galleryListSelector.on('click', '.removeSelectedGallery', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == false) {
                return false;
            } else {
                $self.delete(id);
            }


        })


    }
    this.getGalleryList = function (id) {

        $self.product_variation_id = id;
        $self.setTitle(product_module.pr);
        new ajaxHelper([base_url + "/admin/product/variation/gallery/getProductVariationGallery/" + id, "", "get", ""]).makeAjaxCall().then(function (data) {
            var galleryList = "";
            $.each(data, function (i, v) {
                galleryList += ' <div class="col-md-55" > <div style="height:131px;" class="thumbnail">' +
                    '<div class="image view view-first">' +
                    '<img style="width: 100%; display: block;" src="' + base_url + '/' + v.thumb_image_url + '" alt="product variation image">' +

                    '<div class="mask no-caption">' +


                    '<div class="tools tools-bottom">' +
                    '<a href="#"><i class="fa fa-eye"></i></a>' +

                    '<a href="#" data-id=" ' + v.id + '" class="removeSelectedGallery"><i class="fa fa-times"></i></a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div></div>';
            })


            if(galleryList==""){
                $galleryListSelector.html('<h5>   No Images</h5>');
            }else{
                $galleryListSelector.html(galleryList);
            }
        });
    }
    this.setTitle=function(productName){
        var productName=product_module.productName
        $pageName.find('.page-title h3').html('Gallery of  <small> Product Variation of '+productName+'</small>');

    }

    this.store = function () {
        var formData = new FormData();
        formData.append('_token', csrfToken);
       ;

        $.each($("input[type='file']")[0].files, function (i, file) {
            formData.append('images[' + i + ']', file);
        });

        formData.append('product_variation_id', $self.product_variation_id)

        //console.log(formData);
        $.ajax({
            url: base_url + "/admin/product/variation/gallery/storeProductVariationGallery/",
            data: formData,
            type: 'POST',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            success: function (data) {

                new alertMessage('success', data.success).printMessage();
                $self.emptyForm();
                $self.getGalleryList($self.product_variation_id);
            },
            error: function (response) {
                $self.formValidations(response.responseJSON.errors);
            }
        })

    }
    this.delete = function (id) {


        new ajaxHelper([base_url + "/admin/product/variation/gallery/removeProductVariationGallery/" + id, "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {

                new alertMessage('success', data.success).printMessage();
                $self.getGalleryList($self.product_variation_id);
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        })
    }
    this.showProductList = function () {
        $('#productVariationGalleryPage ').hide('slide', {direction: 'left'}, 500);
        window.setTimeout(function () {
            $('#listProductVariationsPage').show('slide', {direction: 'left'}, 500);


        }, 300);
    }


    this.changeFormStatus = function (type) {

    }

    //form validation error print
    this.formValidations = function (data) {
        var errorMsg = '';
        $.each(data, function (i, value) {
            errorMsg += '<li>' + value + '</li>';
        })
        $formName.prepend('<div class="alert alert-danger form_errors text-danger">' + errorMsg + '</div>');


    }
    this.hideError = function () {
        $formName.find('.form_errors').hide();

    }
    this.emptyForm = function () {
        $formName[0].reset();


    }


    this.init = function () {
        $self.bindFunction();


    }();

}

