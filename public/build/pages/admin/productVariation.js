var productVariation_module = new productVariation();
function productVariation() {


    var $self = this;
    $self.dataTable;
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    var $tableName = $('#productVariationTable');
    var $formModal = $('#productVariationModal');
    var $formName = $('#productVariationForm');
    var $pageName = $('#listProductVariationsPage');
    $self.productId;
    $self.variationType;


    this.bindFunction = function () {
        $self.setDataTable();




        $('#addProductVariationBtn').on('click', function () {

            //    change form Status
            $self.changeFormStatus('Create');
            $self.emptyForm();
            $self.hideError();
            $self.getSizes();
            $self.showVariationType();
            $formModal.modal('show');


        })

        $formName.on('submit', function (e) {
            e.preventDefault();
            $self.hideError();


            var id = $formName.find('input[name="id"]').val();
            if (id) {
                $self.update(id);
            } else {
                $self.store();
            }


        })
        $tableName.on('click', '.editProductVariationBtn', function () {
            var id = $(this).data('id');
            //    change form Status
            $self.changeFormStatus('Update');
            $self.edit(id);
            $self.showVariationType();
            $formModal.modal('show');


        })
        $tableName.on('click', '.delProductVariationBtn', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == false) {
                return false;
            } else {
                $self.delete(id);
            }


        })
        //show product page
        $tableName.on('click', '.imgProductVariationBtn', function () {
            var productVariationId = $(this).data('id');
            $self.showGalleryPage(productVariationId);
        })
        //Back to Product List product pagetabl
        $pageName.on('click', '.backToListProductPage', function () {

            $self.showProductListPage();
        })


        //    append Qty and size
        $formName.on('click','.add_size_qty_attr_btn', function (e) {
            e.preventDefault();
            $self.addSizeQty();
        })
        $formName.on('click', '.remove_size_qty_attr_btn', function (e) {
            e.preventDefault();
            if (confirm('Are you sure want to remove') == false) {
                return false;
            } else {
                $(this).closest('.subSizeQtyList').remove();
            }

        })


    }


    this.setDataTable = function () {
        $self.dataTable = $tableName.DataTable({
            processing: true,
        });
    }

    this.showGalleryPage = function (id) {

        $('#listProductVariationsPage').hide('slide', {direction: 'left'}, 500);
        window.setTimeout(function () {
            $('#productVariationGalleryPage').show('slide', {direction: 'left'}, 500);
            productVariationGallery_module.getGalleryList(id);


        }, 300);
    }
    this.showProductListPage = function () {
        $pageName.hide('slide', {direction: 'left'}, 500);
        window.setTimeout(function () {
            $('#listProductsPage').show('slide', {direction: 'left'}, 500);
            product_module.getProduct();


        }, 300);
    }
    this.showVariationType=function(){

        if ($self.variationType == 'simple') {
            $('.showQty').show(()=> {
                $('.showSizeQty').hide(()=>{
                    $formName.find('.showQty input[name="qty[]"]').attr('disabled',false);
                    $formName.find('.showSizeQty input[name="qty[]"]').attr('disabled',true);
                });


            });
        } else {
            $('.showSizeQty').show(()=> {
                $('.showQty').hide(()=>{
                    $formName.find('.showQty input[name="qty[]"]').attr('disabled',true)
                    $formName.find('.showSizeQty input[name="qty[]"]').attr('disabled',false)
                });
            })
        }
    }

    this.getSizes = function (addQtySize) {
        new ajaxHelper([base_url + "/admin/product/variation/getSizes/", "", "get", ""]).makeAjaxCall().then(function (data) {
            if (typeof addQtySize === "function") {
                addQtySize(data);

            } else {
                if (data) {
                    var sizeHtml = '<option value="0" selected disabled>Select Size</option>';
                    $.each(data, function (i, v) {
                        sizeHtml += '<option value="' + v.id + '">' + v.name + '</option>';
                    })

                    $formName.find('select[name="size[]"]').html(sizeHtml);

                }
            }


        });

    }
    this.addSizeQty = function () {

        $self.getSizes(function (data) {
            var size_qtyHtml = '<div class="col-md-12 sizeQtyList subSizeQtyList" >' +
                '<div class="col-md-4">' +
                '<select name="size[]" class="form-control" required id="productVariation_size">' +
                '<option value="0" selected disabled>Select Size</option>';
            //call back function

            $.each(data, function (i, v) {

                size_qtyHtml += '<option value="' + v.id + '">' + v.name + '</option>';
            })

            size_qtyHtml += '</select>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<input type="number" class="form-control" name="qty[]" value="1">' +

                '</div>' +
                '<div class="col-md-2">' +
                '<span class="btn btn-sm btn-danger remove_size_qty_attr_btn"><i class="fa fa-minus"></i></span>' +
                '</div>' +


                '</div>';
            $formName.find('#addSizeQtyHtml').append(size_qtyHtml);

        });


    }

    this.editSizeQty = function (editQtyData, editSizeData) {
        if (editQtyData && editSizeData) {
            $self.getSizes(function (data) {
                $formName.find('.sizeQtyList').remove();
                for (var i = 0; i < editSizeData.length; i++) {


                    if (i == 0) {
                        var size_qtyHtml = '<div class="col-md-12 sizeQtyList parentSizeQtyList">';
                    } else {
                        var size_qtyHtml = ' <div class="col-md-12 sizeQtyList subSizeQtyList">';
                    }
                    size_qtyHtml += '<div class="col-md-4">' +
                        '<select name="size[]" class="form-control" required id="productVariation_size">' +
                        '<option value="0" selected disabled>Select Size</option>';
                    //call back function

                    $.each(data, function (key, value) {
                        if (value.id == editSizeData[i]) {
                            size_qtyHtml += '<option value="' + value.id + '" selected >' + value.name + '</option>';
                        } else {
                            size_qtyHtml += '<option value="' + value.id + '" >' + value.name + '</option>';
                        }

                    })


                    size_qtyHtml += '</select>' +
                        '</div>' +
                        '<div class="col-md-4">' +
                        '<input type="number" class="form-control" name="qty[]"value="' + editQtyData[i] + '">' +

                        '</div>' +
                        '<div class="col-md-2">';
                    if (i == 0) {
                        size_qtyHtml += '<span class="btn btn-sm btn-success add_size_qty_attr_btn"><i class="fa fa-plus"></i></span>';
                    } else {
                        size_qtyHtml += '<span class="btn btn-sm btn-danger add_size_qty_attr_btn"><i class="fa fa-minus"></i></span>';
                    }
                    size_qtyHtml += '</div>' +


                        ' </div>';

                    $formName.find('#addSizeQtyHtml').append(size_qtyHtml);
                    $formName.find('.showSizeQty').show(()=>{
                        $formName.find('input[name="selectAttr"][value="sizeQty"]').prop('checked',true);
                    });

                }

                //    end of for loop
            });
            //    end of function
        }


    }



    //store
    this.getProductVariation = function (productId,type) {
        $self.productId = productId;
        $self.variationType = type;

        $self.setTitle();

        new ajaxHelper([base_url + "/admin/product/variation/getProductVariations/"+productId, "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var productVariationTableData = "";
                $.each(data, function (i, v) {
                    productVariationTableData += '<tr class="productVariation_' + v.id + '">' +
                        '<td>' + (i + 1) + '</td>' +
                        '<td> <img height="50px" width="50px" src="' + base_url + v.featuredImg.replace('/shares/','/shares/thumbs/') + '"></td>' +
                        '<td>' + v.size + '</td>' +
                        '<td>' + v.qty + '</td>';
                    if (v.status == 'active') {
                        productVariationTableData += '<td> <label class="label label-success" for="status">' + v.status + '</label></td>';
                    } else {
                        productVariationTableData += '<td> <label class="label label-danger" for="status">' + v.status + '</label></td>';
                    }


                    productVariationTableData += '<td><a  data-id="' + v.id + '" class="btn btn-primary editProductVariationBtn"><i class="fa fa-edit"></i></a>' +
                        '<a  data-id="' + v.id + '" class="btn btn-danger delProductVariationBtn"><i class="fa fa-remove"></i></a>' +
                        '<a  data-id="' + v.id + '" class="btn btn-info imgProductVariationBtn"><i class="fa fa-file-photo-o"></i></a></td>' +
                        '</tr>';
                })
                $self.dataTable.destroy();
                $tableName.find('tbody').html(productVariationTableData);
                $self.setDataTable();
            }

        });

    }
    this.setTitle=function(){
        var productName=product_module.productName
        $pageName.find('.page-title h3').html('Product Variation of <small> '+productName+'</small>');

    }
    this.store = function () {
        let formData = $formName.serialize() + '&_token=' + csrfToken + '&product_id=' + $self.productId+'&type=' + $self.variationType;
        //console.log(formData);
        new ajaxHelper([base_url + "/admin/product/variation/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                $self.getProductVariation( $self.productId,$self.variationType);
            } else {
                if (data.errors) {
                    $self.formValidations(data.errors);
                } else {
                    new alertMessage('error', data.error).printMessage();
                }
            }

        }, function (response) {
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }
    this.delete = function (id) {


        new ajaxHelper([base_url + "/admin/product/variation/delete/" + id, {
            _token: csrfToken,
            id: id
        }, "DELETE", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {

                new alertMessage('success', data.success).printMessage();
                $self.getProductVariation($self.productId,$self.variationType);
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        })
    }
    this.edit = function (id) {


        new ajaxHelper([base_url + "/admin/product/variation/" + id + "/edit", "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data);
            $formName.find('input[name="id"]').val(data.id);

            $formName.find('input[name="featuredImg"]').val(data.featuredImg);
            $formName.find('input[name="status"][value="' + data.status + '"]').prop('checked', true);
            if (data.featuredImg) {
                $formName.find('#variationHolder').attr('src', base_url + data.featuredImg).addClass('thumbnail');

            } else {
                $formName.find('#variationHolder').attr('src', '').removeClass('thumbnail');
            }

            //    edit Qty and Size
            if(data.qty!=null && data.size!=null){
                $self.editSizeQty(data.quantities, data.sizes);
            }else{
               $formName.find('.showQty').show(()=>{
                   $formName.find('input[name="selectAttr"][value="qty"]').prop('checked',true);
                   $formName.find('input[name="singleQty"]').val(data.qty);
                   $self.getSizes();
               });
            }



        })
    }
    this.update = function (id) {
        let formData = $formName.serialize() + '&_token=' + csrfToken + '&product_id=' + $self.productId +'&type=' + $self.variationType;
        //console.log(formData);
        new ajaxHelper([base_url + "/admin/product/variation/" + id, formData, "put", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {
                $formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                $self.getProductVariation( $self.productId,$self.variationType);
            } else {

                if (data.errors) {
                    $self.formValidations(data.errors);
                } else {
                    new alertMessage('error', data.error).printMessage();
                }

            }

        }, function (response) {
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    }



    this.changeFormStatus = function (type) {
        $formModal.find('.modal-title').text(type + ' ProductVariation Form');
        $formModal.find('button[type="submit"]').text(type + ' ProductVariation');
    }

    //form validation error print
    this.formValidations = function (data) {
        var errorMsg = '';
        $.each(data, function (i, value) {
            errorMsg += '<li>' + value + '</li>';
        })
        $formName.prepend('<div class="alert alert-danger form_errors text-danger">' + errorMsg + '</div>');


    }
    this.hideError = function () {
        $formName.find('.form_errors').hide();


    }
    this.emptyForm = function () {
        $formName[0].reset();
        $formName.find('input[name="id"]').val('');
        $formName.find('input[name="featuredImg"]').val('');
        $formName.find('#variationHolder').attr('src', '').removeClass('thumbnail');
        $formName.find('.showSizeQty,.showQty').hide();
        $formName.find('input[name="selectAttr"]').prop('checked', false);
        $formName.find('.subSizeQtyList').remove();


    }


    this.init = function () {
        $self.bindFunction();


    }();

}

