function Size() {
    $self = this;

    this.dataTable = "";

    this.tableName = $('#sizeTable');
    this.formModal = $('#sizeModal');
    this.formName = $('#sizeForm');
    this.csrfToken = $('meta[name="csrf-token"]').attr('content');
    //private function
    getSize=function () {
        new ajaxHelper([base_url + "/admin/size/getSizes/", "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var sizeTableData = "";
                $.each(data, function (i, v) {
                    sizeTableData += '<tr>' +
                        '<td>' + (i + 1) + '</td>' +

                        '<td>' + v.name + '</td>';


                    sizeTableData += '<td><a href="#" data-id="' + v.id + '" class="btn btn-primary editSizeBtn"><i class="fa fa-edit"></i></a>' +
                        '<a href="#"  data-id="' + v.id + '" class="btn btn-danger delSizeBtn"><i class="fa fa-remove"></i></a></td>' +
                        '</tr>';
                });
                $self.dataTable.destroy();
                $self.tableName.find('tbody').html(sizeTableData);
                $self.setDataTable();
            }

        });

    }




}
Size.prototype = {
    constructor: Size,
    init: function () {
        $self.bind();
        $self.setDataTable();
        getSize();

    },
    bind: function () {


       

        $('#addSizeBtn').on('click', function () {
            $self.formModal.modal('show');
            //    change form Status
            $self.changeFormStatus('Create');
            $self.emptyForm();
            $self.hideError();


        })

        $self.formName.on('submit', function (e) {
            e.preventDefault();
            $self.hideError();
            var id = $self.formName.find('input[name="id"]').val();
            let formData = $self.formName.serialize() + '&_token=' + $self.csrfToken;
            if (id) {
                $self.update(id, formData);
            } else {
                $self.store(formData);
            }


        })
        $self.tableName.on('click', '.editSizeBtn', function () {
            var id = $(this).data('id');

            $self.hideError();
            //    change form Status
            $self.changeFormStatus('Update');
            $self.edit(id);
            $self.formModal.modal('show');


        })
        $self.tableName.on('click', '.delSizeBtn', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == false) {
                return false;
            } else {
                $self.delete(id);
            }


        })
    },
    setDataTable: function () {
        $self.dataTable = $self.tableName.DataTable({
            processing: true,
        });
    },

    store: function (formData) {


        //console.log(formData);
        new ajaxHelper([base_url + "/admin/size/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {
                $self.formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                getSize();
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        }, function (response) {
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    },
    delete: function (id) {


        new ajaxHelper([base_url + "/admin/size/delete/" + id, {
            _token: $self.csrfToken,
            id: id
        }, "DELETE", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {

                new alertMessage('success', data.success).printMessage();
                getSize();
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        })
    },
    edit: function (id) {


        new ajaxHelper([base_url + "/admin/size/" + id + "/edit", "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data);
            $self.formName.find('input[name="id"]').val(data.id);
            $self.formName.find('input[name="code"]').val(data.code);
            $self.formName.find('input[name="name"]').val(data.name);

            $self.formName.find('input[name="status"][value="' + data.status + '"]').prop('checked', true);

        })
    },
    update: function (id, formData) {

        //console.log(formData);
        new ajaxHelper([base_url + "/admin/size/" + id, formData, "put", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {
                $self.formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                getSize();
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        }, function (response) {
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    },
    changeFormStatus: function (type) {
        $self.formModal.find('.modal-title').text(type + ' Size Form');
        $self.formModal.find('button[type="submit"]').text(type + ' Size');

    },
    formValidations: function (data) {
        var errorMsg = '';
        $.each(data, function (i, value) {
            errorMsg += '<li>' + value + '</li>';
        })
        $self.formName.prepend('<div class="alert alert-danger form_errors text-danger">' + errorMsg + '</div>');


    },
    hideError: function () {
        $self.formName.find('.form_errors').hide();

    },
    emptyForm: function () {
        $self.formName[0].reset();
        $self.formName.find('input[name="id"]').val('');


    }


}
var sizeModule = new Size();
sizeModule.init();
