function Slider() {
    $self = this;

    this.dataTable = "";

    this.tableName = $('#sliderTable');
    this.formModal = $('#sliderModal');
    this.formName = $('#sliderForm');
    this.csrfToken = $('meta[name="csrf-token"]').attr('content');
    //private function
    getSlider=function () {
        new ajaxHelper([base_url + "/admin/slider/getSliders/", "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var sliderTableData = "";
                $.each(data, function (i, v) {
                    sliderTableData += '<tr>' +
                        '<td>' + (i + 1) + '</td>' +

                        '<td>' + v.title + '</td>';


                    sliderTableData += '<td><a href="#" data-id="' + v.id + '" class="btn btn-primary editSliderBtn"><i class="fa fa-edit"></i></a>' +
                        '<a href="#"  data-id="' + v.id + '" class="btn btn-danger delSliderBtn"><i class="fa fa-remove"></i></a></td>' +
                        '</tr>';
                });
                $self.dataTable.destroy();
                $self.tableName.find('tbody').html(sliderTableData);
                $self.setDataTable();
            }

        });

    }




}
Slider.prototype = {
    constructor: Slider,
    init: function () {
        $self.bind();
        $self.setDataTable();
        getSlider();

    },
    bind: function () {


       

        $('#addSliderBtn').on('click', function () {
            //set short description texxtarea

            $self.formModal.modal('show');
            //    change form Status
            $self.changeFormStatus('Create');
            $self.emptyForm();
            $self.hideError();


        })

        $self.formName.on('submit', function (e) {
            e.preventDefault();
            $self.hideError();
            var id = $self.formName.find('input[name="id"]').val();
            let formData = $self.formName.serialize() + '&_token=' + $self.csrfToken;
            if (id) {
                $self.update(id, formData);
            } else {
                $self.store(formData);
            }


        })
        $self.tableName.on('click', '.editSliderBtn', function () {
            var id = $(this).data('id');

            $self.hideError();
            //    change form Status
            $self.changeFormStatus('Update');
            $self.edit(id);
            $self.formModal.modal('show');


        })
        $self.tableName.on('click', '.delSliderBtn', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == false) {
                return false;
            } else {
                $self.delete(id);
            }


        })
    },
    setDataTable: function () {
        $self.dataTable = $self.tableName.DataTable({
            processing: true,
        });
    },

    store: function (formData) {


        //console.log(formData);
        new ajaxHelper([base_url + "/admin/slider/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {
                $self.formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                getSlider();
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        }, function (response) {
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    },
    delete: function (id) {


        new ajaxHelper([base_url + "/admin/slider/delete/" + id, {
            _token: $self.csrfToken,
            id: id
        }, "DELETE", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {

                new alertMessage('success', data.success).printMessage();
                getSlider();
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        })
    },
    edit: function (id) {


        new ajaxHelper([base_url + "/admin/slider/" + id + "/edit", "", "get", ""]).makeAjaxCall().then(function (data) {
            //console.log(data);

            $self.formName.find('input[name="id"]').val(data.id);
            $self.formName.find('input[name="title"]').val(data.title);
            $self.formName.find('textarea[name="shortDescription"]').text(data.shortDescription);
            $self.formName.find('select[name="category_id"]').val(data.category_id);
            $self.setSelectPicker();
            $self.formName.find('input[name="status"][value="' + data.status + '"]').prop('checked', true);
            $self.formName.find('input[name="featuredImg"]').val(data.featuredImg);
            if (data.featuredImg) {
                $self.formName.find('#holder').attr('src', base_url + data.featuredImg).addClass('thumbnail');

            } else {
                $self.formName.find('#holder').attr('src', '').removeClass('thumbnail');
            }


        })
    },
    update: function (id, formData) {

        //console.log(formData);
        new ajaxHelper([base_url + "/admin/slider/" + id, formData, "put", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {
                $self.formModal.modal('hide');
                new alertMessage('success', data.success).printMessage();
                getSlider();
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        }, function (response) {
            //console.log(response.responseJSON.errors);
            $self.formValidations(response.responseJSON.errors);
        })
    },
    changeFormStatus: function (type) {
        $self.formModal.find('.modal-title').text(type + ' Slider Form');
        $self.formModal.find('button[type="submit"]').text(type + ' Slider');

    },
    formValidations: function (data) {
        var errorMsg = '';
        $.each(data, function (i, value) {
            errorMsg += '<li>' + value + '</li>';
        })
        $self.formName.prepend('<div class="alert alert-danger form_errors text-danger">' + errorMsg + '</div>');


    },
    hideError: function () {
        $self.formName.find('.form_errors').hide();

    },
    emptyForm: function () {
        $self.formName[0].reset();
        $self.formName.find('input[name="id"]').val('');
        $self.formName.find('textarea[name="shortDescription"]').text('');

        $self.formName.find('select[name="category_id"]').val(0);
        //refresh select picker
        $self.setSelectPicker();



        $self.formName.find('#holder').attr('src', '').removeClass('thumbnail');

    },

  setSelectPicker:function () {
    selectOptionPicker = $('.selectpicker').selectpicker('refresh');

  }


}
var sliderModule = new Slider();
sliderModule.init();
