var user_module = new User();
function User() {


    var $self = this;
    $self.dataTable;
    $self.dataShippingInfoTable = "";
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    var $tableName = $('#userTable');
    var $shippingTableName = $('#shippingInfoTable');
    var $accountInfoModal = $('#showAccountInfoModal');
    var $shippingInfoModal = $('#showShippingInfoModal');
    $self.userType;
    $self.userEmail;


    this.bindFunction = function () {
        $self.setDataTable();
        $tableName.on('click', '.delUserBtn', function () {
            var id = $(this).data('id');
            if (confirm('Are You sure want to remove ?') == false) {
                return false;
            } else {
                $self.delete(id);
            }


        });
        $tableName.on('click', '.showAccountInfoBtn', function () {
            var email = $(this).data('email');
            $self.showAccountInfo(email);
            $accountInfoModal.modal('show');


        });
        $tableName.on('click', '.showShippingInfoBtn', function () {
            var email = $(this).data('email');
            $self.userEmail = email;
            $self.slidePage('#listUserPage', '#shippingInfoPage')

            if ($self.dataShippingInfoTable == "") {
                $self.setShippingDataTable();
            }

            $self.getShippingInfo()


        });
        $('#shippingInfoPage').on('click', '.backToListUserPage', function () {
            $self.slidePage('#shippingInfoPage', '#listUserPage');
            $self.getUser();

        });
        $shippingTableName.on('click', '.viewShippingIfoBtn', function () {
            var id = $(this).data('id');
            $self.showShippingInfo(id);
            $shippingInfoModal.modal('show');


        });


    }


    this.setDataTable = function () {
        $self.dataTable = $tableName.DataTable({
            processing: true,
        });

    }
    this.setShippingDataTable = function () {

        $self.dataShippingInfoTable = $shippingTableName.DataTable({
            processing: true,
        });
    }


    //store
    this.getUser = function () {
        var parts = window.location.pathname.split('/');

        $self.userType = parts[3];
        if ($self.userType != 'guest' && $self.userType != 'registered') {
            history.back();
            return false;
        }

        new ajaxHelper([base_url + "/admin/user/getUsers/" + $self.userType, "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var userTableData = "";
                $.each(data, function (i, v) {
                    userTableData += '<tr class="user_' + v.id + '">' +
                        '<td>' + (i + 1) + '</td>';
                    if ($self.userType == 'registered') {
                        userTableData += '<td>' + v.name + '</td>' +
                            '<td>' + v.email + '</td>' +
                            '<td><a  data-email="' + v.email + '" class=" showAccountInfoBtn" href="#"><i class="fa fa-eye"></i>&nbsp;<strong>Account Information</strong></a></td>' +
                            '<td><a  data-email="' + v.email + '" class=" showShippingInfoBtn" href="#"><i class="fa fa-eye"></i>&nbsp;<strong>Shipping Information </strong>&nbsp;<p  class="label label-success">' + v.shipping_infos.length + '</p></a></td>';
                    } else {
                        userTableData += '<td>' + v.firstName + '&nbsp;' + v.lastName + '</td>' +
                            '<td>' + v.userEmail + '</td>' +
                            '<td><a  data-email="' + v.userEmail + '" class="showAccountInfoBtn" href="#"><i class="fa fa-eye"></i>&nbsp;<strong>Account Information</strong></a></td>' +
                            '<td><a  data-email="' + v.userEmail + '" class=" showShippingInfoBtn" href="#"><i class="fa fa-eye"></i>&nbsp;<strong>Shipping Information </strong>&nbsp;<p  class="label label-success">' + v.shipping_infos.length + '</p></a></td>';
                    }


                    userTableData += '<td>' +

                        '<a  data-id="' + v.id + '" class="btn btn-danger delUserBtn"><i class="fa fa-remove"></i></a>' +
                        '</tr>';
                })
                $self.dataTable.destroy();
                $tableName.find('tbody').html(userTableData);
                $self.setDataTable();
            }

        });

    }
    this.showAccountInfo = function (email) {


        new ajaxHelper([base_url + "/admin/user/showAccountInfo/" + email, "", "get", ""]).makeAjaxCall().then(function (data) {
            if(data){
                $self.viewInfoPage($accountInfoModal.find('#accountInfo'), data);
            }else{
                $accountInfoModal.find('#accountInfo p').html('');
            }


        });

    }


    this.delete = function (id) {


        new ajaxHelper([base_url + "/admin/user/delete/" + $self.userType + "/" + id, {
            _token: csrfToken,
            id: id
        }, "DELETE", ""]).makeAjaxCall().then(function (data) {
            if (data.success) {

                new alertMessage('success', data.success).printMessage();
                $self.getUser();
            } else {
                new alertMessage('error', data.error).printMessage();
            }

        })
    }
    this.slidePage = function (prevSelector, nextSelector) {
        $(prevSelector).hide('slide', {direction: 'left'}, 500);
        window.setTimeout(function () {
            $(nextSelector).show('slide', {direction: 'left'}, 500);
        }, 300);
    }
    this.getShippingInfo = function () {
        new ajaxHelper([base_url + "/admin/user/getShippingInfo/" + $self.userEmail, "", "get", ""]).makeAjaxCall().then(function (data) {
            if (data) {
                var userTableData = "";
                $.each(data, function (i, v) {
                    userTableData += '<tr class="user_' + v.id + '">' +
                        '<td>' + (i + 1) + '</td>' +

                        '<td>' + v.firstName + '&nbsp;' + v.lastName + '</td>' +
                        '<td>' + v.email + '</td>' +
                        '<td>' + v.default + '</td>' +
                        '<td><a  data-id="' + v.id + '" class="btn btn-primary viewShippingIfoBtn"><i class="fa fa-eye"></i></a> </td>' +
                        '</tr>';
                })
                $self.dataShippingInfoTable.destroy();
                $shippingTableName.find('tbody').html(userTableData);
                $self.setShippingDataTable();
            }

        })
    }

    this.showShippingInfo = function (id) {


        new ajaxHelper([base_url + "/admin/user/showShippingInfo/" + id, "", "get", ""]).makeAjaxCall().then(function (data) {
            if(data){
                $self.viewInfoPage($shippingInfoModal.find('#shippingInfo'), data);
            }else{
                $accountInfoModal.find('#accountInfo p').html('');
            }



        });


    }
    this.viewInfoPage = function (selector, data) {
        selector.find('p.name').html(data.firstName + '&nbsp;' + data.lastName);
        selector.find('p.address').text(data.address);
        if(data.email){
            selector.find('p.email').text(data.email);
        }else{
            selector.find('p.email').text(data.userEmail);
        }

        selector.find('p.gender').text(data.gender);
        selector.find('p.region').text(data.region);
        selector.find('p.city').text(data.city);
        selector.find('p.telephone').text(data.telephone);
        selector.find('p.fax').text(data.fax);

    }

    //form validation error print
    this.formValidations = function (data) {
        var errorMsg = '';
        $.each(data, function (i, value) {
            errorMsg += '<li>' + value + '</li>';
        })
        $formName.prepend('<div class="alert alert-danger form_errors text-danger">' + errorMsg + '</div>');


    }
    this.hideError = function () {
        $formName.find('.form_errors').hide();


    }

    this.init = function () {
        $self.bindFunction();
        $self.getUser();


    }();

}

