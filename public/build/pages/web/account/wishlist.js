var wishlist_of_accountPage = new WishList();
function WishList() {


    var $self = this;
    var csrfToken = $('meta[name="csrf-token"]').attr('content');

    this.bindFunction = function () {
        $('.add-to-cart-btn').on('click', function () {
            $parentSelector = $(this).parents('tr')
            let formData = {
                product_id: $parentSelector.find('input[name="product_id"]').val(),
                product_variation_id: $parentSelector.find('input[name="product_variation_id"]').val(),
                size_id: $parentSelector.find('input[name="size_id"]').val(),
                maxQty: $parentSelector.find('input[name="maxQty"]').val(),
                qty: $parentSelector.find('select[name="qty"]').val(),
                _token: csrfToken


            };
            new ajaxHelper([base_url + "/cart/addToCart/", formData, "post", ""]).makeAjaxCall().then(function (data) {
                window.location.href = data.url;

            })


        })


    }


    this.init = function () {
        $self.bindFunction();


    }();


}

