var addressBook_module = new addressBook();
function addressBook() {


    var $self = this;
    var $tableName = $('#addressBookTable');
    var $modalName = $('#shippingInfoModal');
    var $formName = $('#shippingInfoForm');
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    this.bindFunction = function () {


        $tableName.on('click', '.editShippingInfoBtn', function (e) {
            e.preventDefault();
            $self.removeValidationError();
            $modalName.find('#shippingInfoTitle').text('Update Shipping Information');
            $modalName.find('#shippingInfoForm').find('button[type="submit"]').text('Update Information');
            var id = $(this).data('id');
            $self.editShippingInfo(id);
            $modalName.modal('show');
        })

        $formName.on('submit', function (e) {
            e.preventDefault();
            $self.removeValidationError();
            let id = $formName.find('input[name="id"]').val();
            if (id == "") {
                $self.add($formName);
            } else {
                $self.update($formName, id);
            }

        })
        $('.my-account').on('click', '.addShippingInfoBtn', function () {
            $self.removeValidationError();
            $self.emptyForm('#shippingInfoForm');
            $modalName.find('#shippingInfoTitle').text('Add Shipping Information');
            $modalName.find('#shippingInfoForm').find('button[type="submit"]').text('Add Information');
            $modalName.modal('show');
        })

    }
    $self.add = function (selector) {
        let formData = $(selector).serialize() + '&_token=' + csrfToken;
        new ajaxHelper([base_url + '/myAccount/storeAddressBook/', formData, "post", ""]).makeAjaxCall().then(function (response) {
            if (response.validationError) {
                $.each(response.validationError, function (i, v) {
                    $self.validationMessage($formName.find('input[name="' + i + '"],select[name="' + i + '"]'), '<p class="text-danger">' + v + '</p>');
                })
            } else if (response.error) {
                console.log(response.error);

            } else {
                window.location.href = response.url;
                console.log(response.url);
            }


        });

    }
    $self.editShippingInfo = function (id) {
        new ajaxHelper([base_url + '/myAccount/editAddressBook/' + id, "", "get", ""]).makeAjaxCall().then(function (response) {
            $formName.find('input[name="id"]').val(response.shippingInfo.id);
            $formName.find('input[name="firstName"]').val(response.shippingInfo.firstName);
            $formName.find('input[name="lastName"]').val(response.shippingInfo.lastName);
            $formName.find('input[name="email"]').val(response.shippingInfo.email);
            $formName.find('input[name="address"]').val(response.shippingInfo.address);
            $formName.find('input[name="fax"]').val(response.shippingInfo.fax);
            $formName.find('input[name="telephone"]').val(response.shippingInfo.telephone);
            $formName.find('select[name="gender"]').val(response.shippingInfo.gender);
            $formName.find('select[name="region"]').val(response.shippingInfo.region);
            $formName.find('select[name="city"]').val(response.shippingInfo.city);


        })

    }
    $self.update = function (selector, id) {
        let formData = $(selector).serialize() + '&_token=' + csrfToken;
        new ajaxHelper([base_url + '/myAccount/storeAddressBook/' + id, formData, "post", ""]).makeAjaxCall().then(function (response) {
            if (response.validationError) {
                $.each(response.validationError, function (i, v) {
                    $self.validationMessage($formName.find('input[name="' + i + '"],select[name="' + i + '"]'), '<p class="text-danger">' + v + '</p>');
                })
            } else if (response.error) {
                console.log(response.error);

            } else {
                window.location.href = response.url;
                console.log(response.url);
            }


        });

    }
    this.validationMessage = function (selector, message) {
        $(selector).prev('label').append(message);
        $(selector).parent('label').append(message);
    }
    this.removeValidationError = function () {
        $('p.text-danger').remove();
    }
    this.emptyForm = function (selector) {
        $(selector).find('input').val("");
        $(selector).find('select').val(0);
    }


    this.init = function () {
        $self.bindFunction();


    }();


}

