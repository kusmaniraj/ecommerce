var auth = new Auth();
function Auth() {


    var $self = this;
    $ajaxLoginForm=$('#ajaxLoginForm');
    var csrfToken=$('meta[name="csrf-token"]').attr('content');


    this.bindFunction = function () {
        $ajaxLoginForm.on('submit',function(e){
            $self.removeValidationError();
            e.preventDefault();
            $self.login(this);
        });



    }
    this.login=function(selector){
        let formData=$(selector).serialize()+ '&_token=' + csrfToken;
        new ajaxHelper([base_url+'/ajaxLogin',formData, "post", ""]).makeAjaxCall().then(function (response) {
            if(response.errors){
                $.each(response.errors,function(i,v){
                    $self.validationMessage(i,v);
                })
            }else{

                window.location.href=response.redirectUrl ;
            }

        });

    }
    this.register=function(selector){
        let formData=$(selector).serialize()+ '&_token=' + csrfToken;
        new ajaxHelper([base_url+'/ajaxRegister',formData, "post", ""]).makeAjaxCall().then(function (response) {
            if(response.errors){
                $.each(response.errors,function(i,v){
                    $self.validationMessage(i,v);
                })
            }else{
                window.location.href=response.redirectUrl ;
            }

        });

    }
    this.validationMessage=function(field,value){
        let message='<p class="text-danger">'+value+'</p>';

        $ajaxLoginForm.find('input[name="'+field+'"]').after(message);

    }
    this.removeValidationError=function(){
        $('p.text-danger').remove();
    }



    this.init = function () {
        $self.bindFunction();


    }();


}

