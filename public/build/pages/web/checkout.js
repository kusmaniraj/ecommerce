var checkout_module = new Checkout();
function Checkout() {
    var $self = this;
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    $step1Page = $('#step1');
    $step2Page = $('#step2');
    $step3Page = $('#step3');
    $step4Page = $('#step4');
    $step5Page = $('#step5');
    $self.userEmail=$step2Page.find('#billingInfoForm input[name="userEmail"]').val();
    $self.billingId="";
    $self.shippingId="";
    $self.paymentType;


    this.bindFunction = function () {
        //step 1 {

        $('#step1').on('change', 'input[name="userType"]', function () {
            //remove error
            $self.removeValidationError();

            var type = $(this).val();
            if (type == 'register') {
                $('#step1 .login-page').show();
                $('#step1 .email-page').hide();
            } else {
                $('#step1 .email-page').show();
                $('#step1 .login-page').hide();
            }
        })

        $step1Page.on('click', '.btnStep1', function () {
            $self.step1();

        })
        $step1Page.on('click', '#register', function () {
            $self.removeValidationError();
            $('#step1 .login-page').hide(()=> {
                $('#step1 .register-page').show();
            });

        });
        $step1Page.on('click', '#backToLogin', function () {
            $self.removeValidationError();
            $('#step1 .register-page').hide(()=> {
                $('#step1 .login-page').show();
            });

        })
        $step1Page.on('submit', '#loginForm', function (e) {
            $self.removeValidationError();
            e.preventDefault();
            $self.login(this);
        });
        $step1Page.on('submit', '#registerForm', function (e) {
            e.preventDefault();
            $self.removeValidationError();

            $self.register(this);
        });

        //step 2
        $step2Page.on('change', 'input[name="logIn"]', function () {
            $self.removeValidationError();

            if ($(this).val() == "no") {
                $step2Page.find('input[name="logIn"]').val('yes').prop('checked', true);
                $step2Page.find('.passwordPage').show();
            } else {
                $step2Page.find('input[name="logIn"]').val('no').prop('checked', false)

                $step2Page.find('.passwordPage').hide();
            }
        })
        $step2Page.on('submit', '#billingInfoForm', function (e) {
            $self.removeValidationError();
            e.preventDefault();
            $self.step2(this);
            //    view shipping address in step 3
            $self.getShippingInfo();

        });

        //step 3
        $step3Page.on('click','#addShippingInfoBtn',function(){
            $self.removeValidationError();
            $('#shippingInfoModal #shippingInfoTitle').text('Add Shipping Information');
            $('#shippingInfoModal #shippingInfoForm').find('button[type="submit"]').text('Add Information');
            $('#shippingInfoModal').modal('show');


        });
        
        $step3Page.on('click','.editShippingInfo',function(){
            $self.removeValidationError();
            let shippingId=$(this).data('id');
            $self.editShippingInfo(shippingId);
            $('#shippingInfoModal #shippingInfoTitle').text('Update Shipping Information');
            $('#shippingInfoModal #shippingInfoForm').find('button[type="submit"]').text('Update Information');
            $('#shippingInfoModal').modal('show');
        })
        $step3Page.on('click','.removeShippingInfo',function(){
            let shippingId=$(this).data('id');
            if(confirm('Are you sure want to remove')==true){
                $self.removeShippingInfo(shippingId);

            }else{
                return false;
            }



        })
        $step3Page.on('click','.selectSippingInfoBtn',function(){
            var shippingId=$(this).data('id');
            new ajaxHelper([base_url + '/checkout/changeShippingInfoDefault/'+$self.userEmail+'/'+shippingId, "", "get", ""]).makeAjaxCall().then(function (response) {
             console.log('change default');
                $self.getShippingInfo();
            });
        })



        $('#shippingInfoModal').on('submit', '#shippingInfoForm', function (e) {
            $self.removeValidationError();
            e.preventDefault();
            $self.addShippingInfo(this);

        });

        $step3Page.on('click', '.backToStep2', function () {
            $self.backToStep2();
        });
        $('#billingInfoChangeBtn').on('click',function(){
            $self.backToStep2();
        })
        $step3Page.on('click', '.step3Btn', function (e) {
            e.preventDefault();
            let shippingId=$('.shippingInfoList').find('button.active').attr('data-id');
            $self.step3(shippingId);

        });
        //step 4
        $step4Page.on('click', '.step4Btn', function (e) {
            e.preventDefault();
            $self.step4(this);

        });
        $step4Page.on('click', '.backToStep3', function () {
            $self.backToStep3();
        })
        $('#shippingInfoChangeBtn').on('click',function(){
            $self.backToStep3();
        })
        ////step Last
        //$step5Page.on('click', '.stepLastBtn', function () {
        //    $self.step5();
        //})
        $step5Page.on('submit','#placeOrderForm',function(e){
            e.preventDefault();
            $self.placeOrder(this);
        })
        $step4Page.on('click', '.removeCart', function () {
            if (confirm('Are you sure want to remove  selected cart') == false) {
                return false;
            } else {
                $self.removeCart(this);
            }

        })


    }
    this.step1 = function () {
        $step1Page = $('#step1');
        $step2Page = $('#step2');
        $self.removeValidationError();

        var email = $step1Page.find('.email-page input[name="email"]').val();
        $self.userEmail = email;
        var type = $step1Page.find('input[name="userType"]:checked').val();
        //validation

        if (type == "guest") {
            var formData = {email: email, _token: csrfToken};
            new ajaxHelper([base_url + '/checkout/checkEmail', formData, "post", ""]).makeAjaxCall().then(function (response) {
                if (response.errors) {

                    $self.validationMessage($step1Page.find('input[name="email"]'), '<p class="text-danger">' + response.errors.email + '</p>');
                } else {
                    $step2Page.show(()=> {
                        $step2Page.find('input[name="userEmail"]').val(email)
                    });
                    //end validation
                    $step1Page.hide();
                    $('#selectedEmail').text(email);
                    $('.step1_header').append(' <span><img height="20px" width="20px" src="' + base_url + '/images/checked.png' + '" alt=""></span>');
                    console.log('success step 1')
                }

            })

        }


    }
    this.step2 = function (selector) {
        let formData = $(selector).serialize() + '&_token=' + csrfToken;
        new ajaxHelper([base_url + '/checkout/storeBillingInformation', formData, "post", ""]).makeAjaxCall().then(function (response) {
            if (response.validationError) {
                $.each(response.validationError, function (i, v) {
                    $self.validationMessage($step2Page.find('#billingInfoForm ' + 'input[name="' + i + '"],select[name="' + i + '"]'), '<p class="text-danger">' + v + '</p>');
                })
            } else if (response.error) {
            } else {

                $self.billingId = response.billingDetail.id;
                $step3Page.show(()=>$step2Page.hide(()=> {
                    var billingInfoHtml =  '<address>Name:&nbsp;'+response.billingDetail.firstName + '&nbsp;'+ response.billingDetail.lastName+'</br>' +
                        'Address:&nbsp;'+response.billingDetail.address + '</br>' +
                        'E-mail:&nbsp;'+response.billingDetail.userEmail + '</br>' +
                        'Region:&nbsp;'+response.billingDetail.region + '</br>' +
                        'City:&nbsp;'+response.billingDetail.city + '</br>' +
                        'Tel:&nbsp;' + response.billingDetail.telephone + '</br>' +
                        'Fax:&nbsp;' + response.billingDetail.fax + '</br></address>';
                    $self.showInfo('#billing-info', billingInfoHtml);
                    $('.step2_header').append(' <span><img height="20px" width="20px" src="' + base_url + '/images/checked.png' + '" alt=""></span>');
                }));

                console.log('success step 2');
                ////    view shipping address in step 3
                //    $self.getShippingInfo();



            }
        })
    }
    this.editShippingInfo=function(shippingId){
        new ajaxHelper([base_url + '/checkout/editShippingInformation/'+shippingId, "", "get", ""]).makeAjaxCall().then(function (response) {
            $('#shippingInfoForm').find('input[name="id"]').val(response.shippingInfo.id);
            $('#shippingInfoForm').find('input[name="firstName"]').val(response.shippingInfo.firstName);
            $('#shippingInfoForm').find('input[name="lastName"]').val(response.shippingInfo.lastName);
            $('#shippingInfoForm').find('input[name="email"]').val(response.shippingInfo.email);
            $('#shippingInfoForm').find('input[name="address"]').val(response.shippingInfo.address);
            $('#shippingInfoForm').find('input[name="fax"]').val(response.shippingInfo.fax);
            $('#shippingInfoForm').find('input[name="telephone"]').val(response.shippingInfo.telephone);
            $('#shippingInfoForm').find('select[name="gender"]').val(response.shippingInfo.gender);
            $('#shippingInfoForm').find('select[name="region"]').val(response.shippingInfo.region);
            $('#shippingInfoForm').find('select[name="city"]').val(response.shippingInfo.city);



        })

    }
    this.getShippingInfo=function(){
        let formData = {_token:csrfToken,userEmail: $self.userEmail};
        new ajaxHelper([base_url + '/checkout/getShippingInfos', formData, "post", ""]).makeAjaxCall().then(function (response) {
            let shippingInfoHtml='';
            $.each(response.shippingInfos,function(i,v){
                shippingInfoHtml +='<div class="col-md-3 shippingInfoList">'+
               '<h5>'+v.firstName+'&nbsp; '+v.lastName+'&nbsp; ';
                if(v.default=='yes' || v.email==$self.userEmail){
                    shippingInfoHtml += '<span class="pull-right">' + '<a href="#" data-id="'+v.id+'" class="editShippingInfo"><i class="   text-primary fa fa-edit"></i></a></>';

                }else {
                    shippingInfoHtml += '<span class="pull-right"><a href="#step3" data-id="'+v.id+'" class="removeShippingInfo"><i class="  text-danger fa fa-remove"></i></a>&nbsp;' + '<a href="#" data-id="'+v.id+'" class="editShippingInfo"><i class="   text-primary fa fa-edit"></i></a></>';


                }

                shippingInfoHtml += '</h5><ul>'+
                    '<li><i class="fa fa-envelope"></i>&nbsp;<span>'+v.email+'</span></li>'+
                    '<li><i class="fa fa-map-marker"></i>&nbsp;<span>'+v.address+'</span>'+
                    '</br>'+
                    '<p>'+v.region+'</p></br> <p>'+v.city+'</p>'+
                    '</li>'+
                    '<li><i class="fa fa-phone"></i>&nbsp;<span>'+v.telephone+'</span></li>'+
                    '<li>';
                if(v.default=='yes'){
                    shippingInfoHtml +='<button class="active button " data-id="'+v.id+'">Use this Information</button>';
                }else{
                    shippingInfoHtml +='<button class=" button selectSippingInfoBtn" data-id="'+v.id+'">Use this Information</button>';
                }

                shippingInfoHtml +='</li>'+
                    '</ul>'+

                    '</div>';
            });

           $step3Page.find('.shipping-info-lists').html(shippingInfoHtml);
        })
    }
    this.backToStep2 = function () {
        $step2Page.show(()=>$step3Page.hide(()=> {
            $('.step2_header').find('img').remove();
        }));

    }
    this.removeShippingInfo=function(shippingId){
        new ajaxHelper([base_url + '/checkout/removeShippingInformation/'+shippingId, "", "get", ""]).makeAjaxCall().then(function (response) {
            if(response.success){
                console.log(response.success);
                $self.getShippingInfo();
            }else{
                console.log(response.error);
            }

        })

    }

    this.addShippingInfo = function (selector) {
        let formData = $(selector).serialize() + '&_token=' + csrfToken+ '&userEmail=' + $self.userEmail;
        new ajaxHelper([base_url + '/checkout/storeShippingInformation', formData, "post", ""]).makeAjaxCall().then(function (response) {
            if (response.validationError) {
                $.each(response.validationError, function (i, v) {
                    $self.validationMessage($('#shippingInfoModal').find('#shippingInfoForm ' + 'input[name="' + i + '"],select[name="' + i + '"]'), '<p class="text-danger">' + v + '</p>');
                })
            } else if (response.error) {
            } else {
                console.log('add shipping 3');
                $self.getShippingInfo();
                $('#shippingInfoModal').modal('hide');




            }
        })
    }
    this.step3=function(shippingId){
        $step4Page.show(()=>$step3Page.hide(()=>{
            $step3Page.find('#shippingInfoForm input[name="id"]').val(shippingId);
            $self.shippingId = shippingId;
            new ajaxHelper([base_url + '/checkout/editShippingInformation/'+shippingId, "", "get", ""]).makeAjaxCall().then(function (response) {
                var shippingInfoHtml =  '<address>Name:&nbsp;'+response.shippingInfo.firstName + '&nbsp;'+ response.shippingInfo.lastName+'<br>' +
                    'Address:&nbsp;'+response.shippingInfo.address + '<br>' +
                    'E-mail:&nbsp;'+response.shippingInfo.email + '<br>' +
                    'Region:&nbsp;'+response.shippingInfo.region + '<br>' +
                    'City:&nbsp;'+response.shippingInfo.city + '<br>' +
                    'Tel:&nbsp;' + response.shippingInfo.telephone + '<br>' +
                    'Fax:&nbsp;' + response.shippingInfo.fax + '<br></address>';


                $self.showInfo('#shipping-info', shippingInfoHtml);

            });

            $('.step3_header').append(' <span><img height="20px" width="20px" src="' + base_url + '/images/checked.png' + '" alt=""></span>');
            console.log('success step 3');
        }));
    }



    this.step4 = function () {
        $self.paymentType = $step4Page.find('input[name="paymentType"]').val();
        $step5Page.show(()=>$step4Page.hide(()=>{
            $self.showInfo('#payment_method', $self.paymentType)
            $('.step4_header').append(' <span><img height="20px" width="20px" src="' + base_url + '/images/checked.png' + '" alt=""></span>');
            console.log('success step 4');

        }));
    }
    this.backToStep3 = function () {
        $step3Page.show(()=>$step4Page.hide());
        $('.step3_header').find('img').remove();
    }

    //this.step5 = function () {
    //    console.log($self.paymentType + $self.billingId +$self.shippingId +$self.userEmail);
    //}
    this.placeOrder=function(selector){
        let formData = $(selector).serialize() + '&_token=' + csrfToken + '&email=' + $self.userEmail+'&paymentMethod='+ $self.paymentType+'&billingId=' + $self.billingId+'&shippingId=' + $self.shippingId;
        new ajaxHelper([base_url + '/checkout/placeOrder', formData, "post", ""]).makeAjaxCall().then(function (response) {
            if(response.success){
                console.log(response.success);
                window.location.href=response.url;
            }else{
                console.log(response.error);
                window.location.href=response.url;
            }

        });

    }
    this.removeCart = function (selector) {
        let price = $step4Page.find('td.price span').text();
        let qty = $step4Page.find('select[name="qty"]').val();

        let total = price * qty;
        let totalPreviousPrice = $step4Page.find('.totalPrice strong').text();

        let grandTotalPreviousPrice = $step4Page.find('.grandTotalPrice span').text();

        $step4Page.find('.totalPrice strong').text((totalPreviousPrice - total));
        $step4Page.find('.grandTotalPrice span').text((grandTotalPreviousPrice - total));


        $(selector).parents('tr').remove();

    }
    this.showInfo = function (selector, html) {
        $(selector).html(html);

    }

    this.validationMessage = function (selector, message) {
        $(selector).after(message);

        $(selector).parent('div').addClass('has-error');

    }
    this.removeValidationError = function () {
        $('p.text-danger').remove();
        $('body div').removeClass('has-error');
    }
    this.emptyForm=function(selector){
        $(selector).find('input').val("");
        $(selector).find('select').val(0);
    }


    this.init = function () {
        $self.bindFunction();


    }();


}

