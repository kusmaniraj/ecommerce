var shoppingCart_module = new shoppingCart();
function shoppingCart() {


    var $self = this;
    $self.$addToCartForm = $('#addToCartForm');
    $self.$addToCartFormModal = $('#addToCartFormModal');
    $self.product_variation_id = $self.$addToCartForm.find('input[name="product_variation_id"]').val();
    $self.product_id = $self.$addToCartForm.find('input[name="product_id"]').val();
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    var $selectSizeModal = $('#selectSizeModal');

    this.bindFunction = function () {

        //select size
        $selectSizeModal.on('click', '.size_btn', function () {
            $self.removeError('.cart-error');
            $('.size_btn').removeClass('active');
            $(this).addClass('active');

        });
        $selectSizeModal.on('click', '.select-size', function () {

            if (!$selectSizeModal.find('.size_btn').hasClass('active')) {
                $self.setError('.addToCart-size-error');
                return false;
            }
            let size_id = $selectSizeModal.find('.size_btn.active').data('id');
            $self.$addToCartForm.find('input[name="size_id"]').val(size_id);
            $self.$addToCartFormModal.find('input[name="size_id"]').val(size_id);
            $self.getQty(size_id);
            $selectSizeModal.modal('hide');
            $('.product_variation_size_btn[data-id="'+size_id+'"]').addClass('active');
            console.log('success to select size ');
        });


        //add qty on click on size
        $('body').on('click', '.product_variation_size_btn', function () {
            $self.removeError('.cart-error');
            $('.product_variation_size_btn').removeClass('active');
            $(this).addClass('active');
            let size_id = $(this).data('id');
            $self.$addToCartForm.find('input[name="size_id"]').val(size_id);
            $self.$addToCartFormModal.find('input[name="size_id"]').val(size_id);
            $self.getQty(size_id);
        });
        $('body').on('submit', '#addToCartForm', function (e) {
            e.preventDefault();
            $self.addProductToCart(this)
        });

        $('body').on('submit', '#addToCartFormModal', function (e) {
            e.preventDefault();
            $self.addProductToCart(this)
        });


    }
    this.checkSizeOrNot = function () {
        if ($('.product-details-area .size').length == 0) {
            $self.getQty();
        }

    }
    this.addProductToCart = function (selector) {
        $self.removeError('.cart-error');
        $self.checkSizeOrNot();

        var size = $(selector).find('input[name="size_id"]').val();

        if (size == "" && $('.size-area .size').length != 0) {
            $selectSizeModal.find('.size-title').text('Please select a size to add this product to cart');
            $selectSizeModal.modal('show');


            return false;
        }


        var formData = $(selector).serialize() + '&_token=' + csrfToken + '&product_id=' + $self.product_id;
        new ajaxHelper([base_url + "/cart/addToCart/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            window.location.href = data.url;
           
        })
    }
  

    this.getQty = function (size_id) {
        var url;
        if (size_id) {
            url = base_url + '/cart/getQty/' + $self.product_variation_id + '/' + size_id;
        } else {
            url = base_url + '/cart/getQty/' + $self.product_variation_id;
        }

        new ajaxHelper([url, "", "get", ""]).makeAjaxCall().then(function (response) {
            $self.$addToCartForm.find('input[name="qty"]').attr('maxlength', response.selectedQty);
            $self.$addToCartForm.find('input[name="maxQty"]').val(response.selectedQty);

            //addToCartFormModal
            var qtyHtml = '';
            for (var i = 0; i < response.selectedQty; i++) {
                qtyHtml += '<option>' + (i + 1) + '</option>';
            }
            $self.$addToCartFormModal.find('select[name="qty"]').html(qtyHtml);
        });

    }
    this.setError = function (selector) {
        $(selector).show();
    }
    this.removeError = function (selector) {
        $(selector).hide();
    }


    this.init = function () {
        $self.bindFunction();


    }();


}

