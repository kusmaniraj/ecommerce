var wishlist_of_detailPage = new Wishlist();
function Wishlist() {


    var $self = this;
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    var selectorForm = '#addToCartForm';
    var $selectSizeModal = $('#selectSizeModal');


    this.bindFunction = function () {
        //check size exits or not
        $self.checkSizeOrNot();

        $('body').on('click', '#addToWishlist', function (e) {
            e.preventDefault();

            $self.addProductToWishlist(this)
        });

        $('body').on('click', '.product_variation_size_btn', function () {
            var size =$(this).data('id');
            $self.checkAddedWishlist(shoppingCart_module.product_id,shoppingCart_module.product_variation_id,size);
        });


    }
    this.checkSizeOrNot = function () {
        if ($('.product-details-area .size').length == 0) {
            shoppingCart_module.getQty();
            //get productId from shoppingCart_module
            $self.checkAddedWishlist(shoppingCart_module.product_id,shoppingCart_module.product_variation_id,"");
        }

    }
    this.addProductToWishlist = function (selector) {

        var id=$(selector).attr('data-id');
        $self.removeError('.cart-error');

        var size = $(selectorForm).find('input[name="size_id"]').val();
        $self.checkSizeOrNot();




        if (size == "" && $('.size-area .size').length != 0) {
            $selectSizeModal.modal('show');
            $selectSizeModal.find('.size-title').text('Please select a size to add this product to wishlist');

            return false;
        }





        var formData = $(selectorForm).serialize() + '&_token=' + csrfToken +'&id='+id;
        new ajaxHelper([base_url + "/myAccount/storeToWishlist/", formData, "post", ""]).makeAjaxCall().then(function (data) {
            if(data.success){
                $(selector).find('span').text('Added to Wishlist')
                $('#addToWishlist i ,#addToWishlist span').css({'color': '#0083c1'});
                $(selector).attr('data-id',data.id);

                console.log(data.success)

            }else{
                console.log(data.error)
            }


        })
    }
    this.checkAddedWishlist=function(productId,productVariationId,sizeId){

        new ajaxHelper([base_url + "/myAccount/getWishlist/"+productId+'/'+productVariationId+'/'+sizeId,"" , "get", ""]).makeAjaxCall().then(function (data) {
            if(data.success) {
                $('#addToWishlist').find('span').text(data.success);
                $('#addToWishlist i ,#addToWishlist span').css({'color': '#0083c1'});
                $('#addToWishlist').attr('data-id', data.id);
                console.log(data.success);
            }else{ $('#addToWishlist').find('span').text('add to wishlist');
                $('#addToWishlist i ,#addToWishlist span').css({'color': '#636363'});
                $('#addToWishlist').attr('data-id', "");

                console.log(data.error);
            }


        })

    }

    this.setError = function (selector) {
        $(selector).show();
    }
    this.removeError = function (selector) {
        $(selector).hide();
    }


    this.init = function () {
        $self.bindFunction();


    }();


}

