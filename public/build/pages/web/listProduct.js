var list_product_module = new list_product();
function list_product() {


    var $self = this;


    this.bindFunction = function () {

        //       price filter
        $self.priceFilter();
        //end of price Filter

        ////   quick view
        //$('body').on('click', '.quick-view .quick-view-btn', function (e) {
        //    e.preventDefault();
        //    var product_id = $(this).data('product_id');
        //    $('#quickViewProductModal').modal('show');
        //    $self.viewProduct(product_id);
        //})
        //
        ////add qty on click on size
        //$('body').on('click', '.quick-view_product_variation_size_btn', function () {
        //    $('.product_variation_size_btn').removeClass('active');
        //    $(this).addClass('active');
        //    let size_id = $(this).data('id');
        //    shoppingCart_module.$addToCartForm.find('input[name="size_id"]').val(size_id);
        //    shoppingCart_module.getQty(size_id);
        //});
        //$('body').on('submit', '#addToCartQuckViewForm', function (e) {
        //    e.preventDefault();
        //    var size = $(this).find('input[name="size_id"]').val();
        //
        //    if (size == "" && $('.size-area .size').length != 0) {
        //        shoppingCart_module.setError('.addToCart-size-error');
        //        return false;
        //    }else{
        //        shoppingCart_module.addProductToCart(this);
        //        $('#quickViewProductModal').modal('hide');
        //    }
        //
        //})

        //    pagination
        //    search by price and rating
        $('#order_by_name').on('change', function () {
            var order_by_name = $(this).val();
            $self.paginate(null, order_by_name);

        })
        $('#sort_by_page').on('change', function () {
            var showPageValue = $(this).val();
            $self.paginate(showPageValue);
        })
        $('.rating').on('change','.rate-star input[type="radio"]',function(){
            var rate=$(this).val();
            $self.paginate(null,null,null,rate);
        })





    }
    this.priceFilter=function(){
        var url;
        url = window.location.href;
        $selector=$('.slider-range-price');
            var min =$selector.data('min');
            var max = $selector.data('max');
            var unit = $selector.data('unit');
            var value_min = $selector.data('value-min');
            var value_max = $selector.data('value-max');
            var label_reasult = $selector.data('label-reasult');
            var t = $selector;
            $selector.slider({
                range: true,
                min: min,
                max: max,
                values: [value_min, value_max],
                slide: function(event, ui) {
                    var result = label_reasult + " " + unit + ui.values[0] + ' - ' + unit + ui.values[1];
                    console.log(t);
                    var slidePrice=ui.values[0]+'-'+ui.values[1];
                    t.closest('.slider-range').find('.amount-range-price').html(result);
                    $self.paginate(null,null,slidePrice);

                }
            });

    }
    this.viewProduct = function (product_id) {
        new ajaxHelper([base_url + '/viewProduct/' + product_id, "", "get", ""]).makeAjaxCall().then(function (response) {

            $('#quickViewProductModal .modal-body').html(response);
            // shopping declaration
            shoppingCart_module.$addToCartForm = $('#quickViewProductModal #addToCartQuckViewForm');
            shoppingCart_module.product_variation_id = shoppingCart_module.$addToCartForm.find('input[name="product_variation_id"]').val();
            shoppingCart_module.product_id = shoppingCart_module.$addToCartForm.find('input[name="product_id"]').val();


        })
    }
    this.paginate = function (showPage, orderBy,slidePrice,rating) {
        var url;
        url = window.location.href;

        if (showPage) {

            if (url.indexOf("showPage") > -1) {
                var newUrl = new URL(url);
                var previousPage = newUrl.searchParams.get("showPage");
                window.location.href = url.replace('showPage=' + previousPage, 'showPage=' + showPage);


            } else if (url.indexOf("price") > 0) {
                window.location.href = url + '&showPage=' + showPage;
            } else if (url.indexOf("rating") > 0) {
                window.location.href = url + '&showPage=' + showPage;
            } else if (url.indexOf("orderBy") > 0) {
                window.location.href = url + '&showPage=' + showPage;

            } else {

                window.location.href = url + '?showPage=' + showPage;
            }
        } else if (orderBy) {

            if (url.indexOf("orderBy") > 0) {
                var newUrl = new URL(url);
                var previousOrderBy = newUrl.searchParams.get("orderBy");
                window.location.href = url.replace('orderBy=' + previousOrderBy, 'orderBy=' + orderBy);
            } else if (url.indexOf("price") > 0) {
                window.location.href = url + '&orderBy=' + orderBy;
            } else if (url.indexOf("rating") > 0) {
                window.location.href = url + '&orderBy=' + orderBy;
            } else if (url.indexOf("showPage") > 0) {
                window.location.href = url + '&orderBy=' + orderBy;
            } else {
                window.location.href = url + '?orderBy=' + orderBy;
            }

        }else if(slidePrice){
            if (url.indexOf("price") > -1) {
                var newUrl = new URL(url);
                var previousPrice = newUrl.searchParams.get("price");
                window.location.href = url.replace('price=' + previousPrice, 'price=' + slidePrice);


            } else if (url.indexOf("showPage") > 0) {
                window.location.href = url + '&price=' + slidePrice;
            } else if (url.indexOf("rating") > 0) {
                window.location.href = url + '&price=' + slidePrice;
            } else if (url.indexOf("orderBy") > 0) {
                window.location.href = url + '&price=' + slidePrice;
            } else {

                window.location.href = url + '?price=' + slidePrice;
            }
        }else if(rating){
            if (url.indexOf("rating") > -1) {
                var newUrl = new URL(url);
                var previousRating = newUrl.searchParams.get("rating");
                window.location.href = url.replace('rating=' + previousRating, 'rating=' + rating);


            } else if (url.indexOf("showPage") > 0) {
                window.location.href = url + '&rating=' + rating;
            } else if (url.indexOf("price") > 0) {
                window.location.href = url + '&rating=' + rating;
            } else if (url.indexOf("orderBy") > 0) {
                window.location.href = url + '&rating=' + rating;
            } else {

                window.location.href = url + '?rating=' + rating;
            }
        }

    }


    this.init = function () {
        $self.bindFunction();


    }();


}

