var ajaxHelper=function(ajaxSetting){

    var $self=this;
    this.options={};
    var ajaxurl,formData,ajaxType,ajaxMethod;

    this.makeAjaxCall=function(){
        //console.log($self);
        return new Promise(function(resolve,reject){
            //
            // if(ajaxMethod!=""){
            //     $self.options.formData.append('_method', ajaxMethod);
            // }
            //
            // $.ajax({
            //     url: $self.options.ajaxurl,
            //     data: {"test":"test"},
            //     type: 'POST',
            //     contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            //     processData: false, // NEEDED, DON'T OMIT THIS
            // })

            $.ajax({
                url: $self.options.ajaxurl,
                type: $self.options.ajaxType,
                data: $self.options.data,
                success: function (data) {
                    resolve(data);
                },
                error: function (err) {
                    reject(err);
                }
            });
        })
    }

    this.initData=function(ajaxSetting){
        $self.options.ajaxurl=ajaxSetting[0];
        $self.options.data=ajaxSetting[1];
        $self.options.ajaxType=ajaxSetting[2];
        $self.options.ajaxMethod=ajaxSetting[3];
    }

    this.init=function(ajaxSetting){
        $self.initData(ajaxSetting);
    }(ajaxSetting);

}