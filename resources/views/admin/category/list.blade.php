@extends('layouts.admin')
@section('content')


<!--First / Main Category Page-->
<div class="main" id="firstCategoryPage">
    <div class="page-title">
        <div class="title_left">

                <h3><a href="url('admin/category')">Main (First Category)</a></h3>

        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')


                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="addCategoryBtn btn btn-primary pull-right">Add New First Category</a>
                    </div>
                    <table id="firstCategoryTable" class="table table-bordered">


                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Category</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>


                    </table>
                </div>


            </div>
        </div>


    </div>
</div>

<!--Second /Parent Category-->
<div class="main" style="display: none;" id="secondCategoryPage">
    <div class="page-title">
        <div class="title_left">

            <h3><a href="url('admin/category')">Parent(Second Category)</a></</h3>

        </div>

        <div class="pull-right">
            <a href="#" id="backFirstCategoryPage"><h4><i class="fa fa-arrow-circle-left"></i> Back To First Category
                </h4></a>
        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')


                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="addCategoryBtn btn btn-primary pull-right">Add New Second Category</a>
                    </div>
                    <table id="secondCategoryTable" class="table table-bordered">


                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Category</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>


                    </table>
                </div>


            </div>
        </div>


    </div>
</div>
<!--Third /Parent Category-->
<div class="main" style="display: none;" id="thirdCategoryPage">
    <div class="page-title">
        <div class="title_left">

            <h5><a href="url('admin/category')">Sub (Third Category)</a></h5>

        </div>

        <div class="pull-right">
            <a href="#" id="backSecondCategoryPage"><h4><i class="fa fa-arrow-circle-left"></i> Back To Second Category
                </h4></a>
        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')


                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="addCategoryBtn btn btn-primary pull-right">Add New Third Category</a>
                    </div>
                    <table id="thirdCategoryTable" class="table table-bordered">


                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Category</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>


                    </table>
                </div>


            </div>
        </div>


    </div>
</div>
<!--Fourth /Parent Category-->
<div class="main" style="display: none;" id="fourthCategoryPage">
    <div class="page-title">
        <div class="title_left">

            <h5><a href="url('admin/category')">Second Sub (Fourth Category)</a></h5>

        </div>

        <div class="pull-right">
            <a href="#" id="backThirdCategoryPage"><h4><i class="fa fa-arrow-circle-left"></i> Back To Third Category
                </h4></a>
        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')


                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="addCategoryBtn btn btn-primary pull-right">Add New Fourth Category</a>
                    </div>
                    <table id="fourthCategoryTable" class="table table-bordered">


                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Category</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>


                    </table>
                </div>


            </div>
        </div>


    </div>
</div>



<!--include Category Modal-->
@include('admin.category.formModal')


@endsection
@push('scripts')
<!--jquery Ui-->
<script src="{{asset('packages/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('build/pages/admin/category.js')}}"></script>

@endpush
