<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png"  href="{{asset('images/favicon.ico')}}">

    <title>{{$getSetting ? $getSetting['website_name'] : " Template"}} | @isset($title){{$title }}@endisset </title>

    <!-- Bootstrap -->
    <link href="{{asset('adminTemplate/asset/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('adminTemplate/asset/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('adminTemplate/asset/nprogress/nprogress.css')}}" rel="stylesheet">
    <link href="{{asset('adminTemplate/asset/datatables/datatables.css')}}" rel="stylesheet">
    <link href="{{asset('build/shared/loading.css')}}" rel="stylesheet">
    <!--    datePicker-->
<!--    <link rel="stylesheet" href="{{asset('adminTemplate/asset/datePicker/bootstrap-datepicker.css')}}">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.standalone.css">


    <!-- Custom Theme Style -->
    <link href="{{asset('adminTemplate/build/css/custom.css')}}" rel="stylesheet">
    <!-- jQuery -->
    <script src="{{asset('adminTemplate/asset/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('adminTemplate/asset/bootstrap/dist/js/bootstrap.min.js')}}"></script>


<!--alert Message-->
    <link href="{{asset('packages/alertify/themes/alertify.default.css')}}" rel="stylesheet">
    <link href="{{asset('packages/alertify/themes/alertify.bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('packages/alertify/themes/alertify.core.css')}}" rel="stylesheet">

<!--    select-->
    <!-- boostrap select -->
    <link rel="stylesheet" href="{{asset('packages/bootstrap-select/bootstrap-select.min.css')}}" />



    @stack('styles')
</head>