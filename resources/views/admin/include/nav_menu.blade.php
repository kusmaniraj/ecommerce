<div class="nav_menu">
    <nav>
        <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                   aria-expanded="false">
                    <img src="{{asset('images/ss.jpg')}}" alt="">{{Auth::user()->name}}
                    <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="{{url('admin/profile')}}"> Profile</a></li>
                    <li>
                        <a href="{{url('admin/setting')}}">

                            <span>Settings</span>
                        </a>
                    </li>

                    <li><a href="{{route('admin.logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                </ul>
            </li>

            <li role="presentation" class="dropdown">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                   aria-expanded="false">
                    <i class="fa fa-globe fa-5x"></i>
                    <span
                        class="badge bg-green"> @isset($booking_notifications){{count($booking_notifications)}}@endisset</span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <div class="notification">
                       @isset($booking_notifications)
                        @foreach($booking_notifications as $notification)
                        <li class="notificationStatus" data-id="">
                            <a href="{{url('admin/orders/listOrders/requested')}}">

                        <p>
                          <span style="color:#000; font-weight: bold">{{$notification['billingInformation']['firstName']}} &nbsp; {{$notification['billingInformation']['lastName']}}</span>
                          <span class="time">{{$notification['created_at']}}</span>
                        </p>
                        <p class="message">
                        
                            Pending Products Order:
                            <br>

                              @foreach($notification['products'] as $product)
                            <p>{{$product['name']}} (qty:{{$notification['qty']}}) @if($notification['size']) of {{$notification['size']}}@endif</p>
                            <br>
                                @endforeach


                        </p>
                            </a>
                        </li>
                        @endforeach
                        @endisset



                    </div>
                    <li>

                        <div class="text-center">
                            <a href="{{url('admin/orders/listOrders/requested')}}">
                                <strong>See All Notifications</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>

                </ul>

            </li>
        </ul>
    </nav>
</div>