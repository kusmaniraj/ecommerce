<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>{{$getSetting ? $getSetting['website_name'] : " Template"}}</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
        <div class="profile_pic">
            <img src="{{asset(Auth::user()->profileImg)}}" alt="..." class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>

            <h2>{{ucfirst(Auth::user()->name)}}</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- /menu profile quick info -->

    <br/>

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3>General</h3>
            <ul class="nav side-menu">
                <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-home"></i> Dashboard </span></a></li>



                 @if(Auth::guard('admin')->check() && Auth::user()->type =='0')

                <li><a href="#"><i class="fa fa-users"></i> Admins Management </span></a></li>
                @else

                <li><a href="{{url('admin/menus')}}"><i class="fa fa-list"></i> Menus Management </span></a>   </li>
                <li id="setting"><a href="#setting"><i class="glyphicon glyphicon-cog"></i> Setting  <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{url('admin/setting/')}}"> General ,Social,Contact,Name & Email  Setting </a></li>
                        <li><a href="{{url('admin/profile/')}}">Profile Setting </a></li>


                        <li><a href="{{url('admin/setting/mailer/')}}"> Mailer Setting</a></li>
                        <li id="attr_setting"><a href="#attr_setting"> Attribute Setting <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('admin/color/')}}"> Color</a></li>
                                <li><a href="{{url('admin/size/')}}"> Size</a></li>



                                </li>
                            </ul>
                        </li>
                        <li id="image_setting"><a href="#image_setting"> Image Setting <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{url('admin/setting/image_icon')}}"> Logo,Favicon and No-Image Setting</a></li>

                                <li><a href="#"> Resize Image Setting</a></li>





                                </li>
                            </ul>
                        </li>




                        </li>
                    </ul>
                </li>
                <li><a href="{{url('admin/category')}}"> <i class="fa fa-list-alt"></i>Category Manage </a></li>
                <li><a><i class="glyphicon glyphicon-list-alt"></i> Product <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{url('admin/product/')}}"> Product Manage</a></li>



                        </li>
                    </ul>
                </li>
                <li><a><i class="fa fa-users"></i> User Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{url('admin/user/registered')}}"> Registered User</a></li>
                        <li><a href="{{url('admin/user/guest')}}"> Guest User</a></li>



                        </li>
                    </ul>
                </li>
                <li><a href="{{url('admin/orders')}}"><i class="fa fa-shopping-cart"></i> Orders </span></a>   </li>


                <li><a href="{{route('admin.fileManager')}}"><i class="fa fa-file"></i> File Manager </span></a>   </li>








                @endif



               
            </ul>





        </div>


    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        <a href="{{url('admin/setting/')}}" data-toggle="tooltip" data-placement="top" title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Lock">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('admin.logout')}}">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    </div>
    <!-- /menu footer buttons -->
</div>