@extends('layouts.admin')


@section('content')


<div>
    <div class="page-title">
        <div class="title_left">
            <h3>Menus Management</h3>
        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">

                <div class="row">
<!--                    alert Message and error print-->
                    @include('messages.alertMessages')
                    <div class="col-sm-6">
                        <button class="btn btn-info btn-sm" id="openAllTree">Expand All
                        </button>
                        <button class="btn btn-primary btn-sm" id="closeAllTree">Close All
                        </button>
                        <button class="btn btn-success btn-sm" id="saveChangesBtn">Save Changes</button>
                        <a href="#" id="refreshTree" class="btn btn-danger btn-sm"> <i
                                class="fa fa-refresh"></i></a>
                        <hr>

                        <div class="form-group">
                            <input type="text" id="searchTree" class="form-control" placeholder="Search...">
                        </div>

                        <div id="tree"></div>
                    </div>

                    <div class="col-sm-6">
                        <h4 class="menuTitle"> Add Menu Form  </h4>

                        <span>


                        <hr>


                        <form id="menuForm" method="post" enctype="multipart/form-data">

                            {{csrf_field()}}
                            @include('messages.validationMessages')

                            <input type="hidden" name="id">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" name="name" value=""" class="form-control"
                                placeholder="Link Name">

                            </div>

                            <div class="form-group">
                                <input type="text" name="url" class="form-control" placeholder="URL"
                                       value="">
                            </div>

                            <div class=" form-group ">
                                <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                         <i class="fa fa-picture-o"></i> Select Profile Image
                                     </a>
                                   </span>
                                <input id="thumbnail" class="form-control" type="hidden" name="featuredImg">
                                <input type="hidden" name="oldFeaturedImg"
                                       value="">
                                <img  id="holder" style=" margin-left:10px;margin-top:15px;max-height:100px;">


                            </div>


                            <div class="form-group">
                                <label for="">Status</label><br>

                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="status"
                                               value="active" >
                                        Active
                                    </label>
                                </div>
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="status"
                                               value="inactive" checked>
                                        Inactive
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary menuSubmitBtn"> Add menu
                                </button>

                            </div>
                        </form>

                    </div>
                </div>


            </div>
        </div>


    </div>
</div>

<!--    //jsTree-->


@endsection
@push('scripts')
<link rel="stylesheet" href="{{asset('packages/jstree/dist/themes/default/style.min.css')}}"/>
<script src="{{ asset('packages/jstree/dist/jstree.min.js')}}"></script>
<script src="{{ asset('build/pages/admin/menu.js')}}"></script>
<script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script > $('#lfm').filemanager('file');</script>

@endpush
