@extends('layouts.admin')
@push('styles')


@endpush


@section('content')


<div class="main" id="orderPage">
    <div class="page-title">
        <div class="title_left">
            <h3> Orders</h3>
        </div>
        <div class="pull-right">
            <a href="#" id="all" class="btn btn-default">All</a>
            <a href="#" id="requestedOrders" class="btn btn-info">Requested Orders</a>
            <a href="#" id="pendingOrders" class="btn btn-primary">Pending Orders</a>
            <a href="#" id="deliveredOrders" class="btn btn-success">Delivered Orders</a>
            <a href="#" id="canceledOrders" class="btn btn-danger">Canceled Orders</a>
        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')
                <div class="row">


                    <table id="orderTable" class="table table-bordered">

                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Date</th>
                            <th>E-mail</th>
                            <th>Product</th>
                            <th>Description</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>


            </div>
        </div>



</div>

@include('admin.orders.billingOrShippingInformation')
@include('admin.orders.productOrderInfo')

@endsection
@push('scripts')
<script src="{{asset('build/pages/admin/order.js')}}"></script>
@endpush
