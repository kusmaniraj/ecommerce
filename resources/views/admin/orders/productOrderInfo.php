<div id="productOrderInfoModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Product Order Information</h4>
            </div>
            <div class="modal-body">
                <div class="row " id="productOrderInfo">

                    <table id="productOrderTable" class="table table-bordered">

                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Description</th>
                            <th>Qty</th>
                            <th>Price</th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>


                    <div class="col-md-12">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    </div>


                </div>


            </div>

        </div>

    </div>

</div>
<script>

</script>
</div>