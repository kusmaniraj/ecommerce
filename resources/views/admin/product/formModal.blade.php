<div id="productModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Product Form</h4>
            </div>
            <div class="modal-body">


                <form id="productForm" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id">

                    <div class="col-md-12 form-group" id="category_id">
                        <div class="col-md-3">
                            <label for="category_id">Select Category <span class="required">*</span></label>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control selectpicker" name="category_id"
                                    data-show-subtext="true" data-live-search="true" required>
                                <option value='0' disabled selected>Select Category</option>
                            </select>

                        </div>

                        <div class="col-md-5 addCategoryFormDiv" style="display: none">
                            <div id="productCategoryForm">
                                <div class="col-md-8">
                                    <input class="form-control" type="text" placeholder="Category Name"
                                           name="categoryName">
                                </div>
                                <div class="col-md-4">
                                    <button type="button"
                                            class="btn btn-sm btn-success pull-right addProductCategoryBtn">Add
                                    </button>
                                    <a class="btn btn-sm btn-danger pull-right removeCategoryBtn"><i
                                            class="fa fa-remove"></i></a>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-2 addCategoryBtnDiv">
                            <a class="btn btn-default addCategoryBtn" href="#"><i class="fa fa-plus"></i>Add
                                Category</a>
                        </div>


                    </div>

                    <div class="form-group col-md-12" id="parent_category_id ">
                        <div class="col-md-3">
                            <label for="parent_category_id">Select Parent-Category <span
                                    class="required">*</span></label>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control selectpicker" name="parent_category_id"
                                    data-show-subtext="true" data-live-search="true" required>
                                <option value='0' disabled selected>Select Parent Category</option>
                            </select>
                        </div>
                        <div class="col-md-5 addParentCategoryFormDiv" style="display: none">
                            <div id="productParentCategoryForm">
                                <div class="col-md-8">
                                    <input class="form-control" type="text" placeholder="Parent Category Name"
                                           name="parentCategoryName">
                                </div>
                                <div class="col-md-4">
                                    <button type="button"
                                            class="btn btn-sm btn-success pull-right addParentCategoryBtn">Add
                                    </button>
                                    <a class="btn btn-sm btn-danger pull-right removeParentCategoryBtn"><i
                                            class="fa fa-remove"></i></a>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-2 addParentCategoryBtnDiv">
                            <a class="btn btn-default addProductParentCategoryBtn" href="#"><i class="fa fa-plus"></i>Add
                                Parent Category</a>
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="sub_category_id">
                        <div class="col-md-3">
                            <label for="sub_category_id">Select Sub-Category <span class="required">*</span></label>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control selectpicker" name="sub_category_id"
                                    data-show-subtext="true" data-live-search="true" required>
                                <option value='0' disabled selected>Select Sub Category</option>
                            </select>
                        </div>

                        <div class="col-md-5 addSubCategoryFormDiv" style="display: none">
                            <div id="productSubCategoryForm">
                                <div class="col-md-8">
                                    <input class="form-control" type="text" placeholder="Sub Category Name"
                                           name="subCategoryName">
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-sm btn-success pull-right addSubCategoryBtn">
                                        Add
                                    </button>
                                    <a class="btn btn-sm btn-danger pull-right removeSubCategoryBtn"><i
                                            class="fa fa-remove"></i></a>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-2 addSubCategoryBtnDiv">
                            <a class="btn btn-default addProductSubCategoryBtn" href="#"><i class="fa fa-plus"></i>Add
                                Sub Category</a>
                        </div>
                    </div>
                    <div class="form-group col-md-12" id="second_sub_category_id">
                        <div class="col-md-3">
                            <label for="name">Select Parent-Category </label>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control selectpicker" name="second_sub_category_id"

                                    data-show-subtext="true" data-live-search="true" required>
                                <option value='0' disabled selected>Select Second Sub Category</option>
                            </select>
                        </div>


                        <div class="col-md-5 addSecondSubCategoryFormDiv" style="display: none">
                            <div id="productSecondSubCategoryForm">
                                <div class="col-md-8">
                                    <input class="form-control" type="text" placeholder="Second Sub Category Name"
                                           name="secondSubCategoryName">
                                </div>
                                <div class="col-md-4">
                                    <button type="button"
                                            class="btn btn-sm btn-success pull-right addSecondSubCategoryBtn">Add
                                    </button>
                                    <a class="btn btn-sm btn-danger pull-right removeSecondSubCategoryBtn"><i
                                            class="fa fa-remove"></i></a>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-2 addSecondSubCategoryBtnDiv">
                            <a class="btn btn-default addProductSecondSubCategoryBtn" href="#"><i
                                    class="fa fa-plus"></i>Add
                                Second Sub Category</a>
                        </div>

                    </div>
                    <div class="form-group col-md-12" id="name">
                        <div class="col-md-3">
                            <label for="name">Product Name <span class="required">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="name"
                                   value="" required>
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="description">
                        <div class="col-md-3">
                            <label for="description">Product Description <span class="required">*</span></label>
                        </div>
                        <div class="col-md-9">
  <textarea id="description_textarea" rows="3" class="form-control" name="description"
            required></textarea>
                        </div>


                    </div>

                    <div class="form-group col-md-12" id="description">
                        <div class="col-md-3">
                            <label for="specification_textarea">Product Specification </label>
                        </div>
                        <div class="col-md-9">
   <textarea id="specification_textarea" rows="3" class="form-control"
             name="specification"></textarea>
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="oldPrice">
                        <div class="col-md-3">
                            <label for="oldPrice">Old Price<span class="required">&nbsp;*</span></label>
                        </div>
                        <div class="col-md-6">
                            <input type="number" class="form-control" name="oldPrice"
                                   value="" required>
                        </div>


                    </div>
                    <div class="form-group col-md-12" id="newPrice">
                        <div class="col-md-3">
                            <label for="newPrice">New Price</label>
                        </div>
                        <div class="col-md-6">
                            <input type="number" class="form-control" name="newPrice"
                                   value="">
                        </div>


                    </div>


                    <div class="form-group col-md-12" id="metaKeyWords">
                        <div class="col-md-3">
                            <label for="metaKeyWords">Meta Keywords </label>
                        </div>
                        <div class="col-md-9">
                            <textarea id="metaKeyWords" rows="3" class="form-control" name="metaKeyWords"></textarea>
                        </div>


                    </div>

                    <div class="form-group col-md-12" id="metaDescription">
                        <div class="col-md-3">
                            <label for="metaDescription">Meta Descriptions </label>
                        </div>
                        <div class="col-md-9">
 <textarea id="metaDescription" rows="3" class="form-control"
           name="metaDescription"></textarea>
                        </div>


                    </div>


                    <div id="specialProduct" class="form-group col-md-12">
                        <div class="col-md-3">
                            <label for="slider">Special Product</label><br>
                        </div>
                        <div class="col-md-9">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="specialProduct" value="yes">
                                    Yes
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="specialProduct"
                                           value="no" checked>
                                    No
                                </label>
                            </div>
                        </div>


                    </div>
                    <div id="featuredProduct" class="form-group col-md-12">
                        <div class="col-md-3">
                            <label for="slider">Featured Product</label><br>
                        </div>
                        <div class="col-md-9">
                            <div class="radio-inline">
                                <label for="featuredProduct">
                                    <input type="radio" name="featuredProduct" value="yes">
                                    Yes
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label for="featuredProduct">
                                    <input type="radio" name="featuredProduct"
                                           value="no" checked>
                                    No
                                </label>
                            </div>
                        </div>


                    </div>
                    <div id="slider" class="form-group col-md-12">
                        <div class="col-md-3">
                            <label for="slider">Slider</label><br>
                        </div>
                        <div class="col-md-9">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="slider" value="yes">
                                    Yes
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="slider"
                                           value="no" checked>
                                    No
                                </label>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-12 form-group">
                        <div class="col-md-3">
                            <label for="variation">Variation<span class="required" >*</span></label><br>
                        </div>
                        <div class="col-md-6">
                            <select name="variationType" id="variationType" class="form-control">
                                <option value="0" selected disabled>Select Variation Type</option>
                                <option value="simple">Simple Variation</option>
                                <option value="advance">Advance Variation</option>
                            </select>

                        </div>
                    </div>
                    <div id="featuredImg" class=" form-group col-md-12 ">
                        <p>Note : *Select Image 1920x530 better and also set for slider</p>

                        <div class="col-md-3">
                            <label for="featuredImg">Featured Image<span class="required">*</span></label><br>
                        </div>
                        <div class="col-md-9">
                                     <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                         <i class="fa fa-picture-o"></i> Select Product Image
                                     </a>
                                   </span>
                            <input id="thumbnail" class="form-control" type="hidden" name="featuredImg">

                            <img id="holder" style=" margin-left:10px;margin-top:15px;max-height:100px;">
                        </div>


                    </div>
                    <div id="status" class="form-group col-md-12">
                        <div class="col-md-3">
                            <label for="status">Status</label><br>
                        </div>
                        <div class="col-md-9">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="status" value="active" checked>
                                    Active
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="status"
                                           value="inactive">
                                    Inactive
                                </label>
                            </div>
                        </div>


                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary " type="submit">Create Product</button>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
    <script>

    </script>
</div>