<div class="main" id="productVariationGalleryPage" style="display: none;">
    <div class="page-title">
        <div class="pull-left">
            <h3> Gallery of Product Variation</h3>
        </div>
        <div class="pull-right">
            <a href="#" class="backToListProductVariationPage"><h4><i class="fa fa-arrow-circle-left"></i> Back</h4></a>
        </div>


    </div>
    <div class="clearfix"></div>

    <div class="row">

                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">

                            <section class="content-header">
                                <button type="button" class="btn btn-info pull-right" data-toggle="collapse"
                                        data-target="#galleryList"><i
                                        class="fa fa-plus"></i> Add Gallery
                                </button>

                            </section>


                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="collapse row" id="galleryList">
                                <form id="productVariationGalleryForm" enctype="multipart/form-data">
                                    @include('messages.alertMessages')

                                    <input type="hidden" name="productVariationId" value="">

                                    <div class="form-group">
                                        <label class="control-label">Select Images</label>

                                        <input id="input-2" name="images[]" type="file" class="file" multiple
                                               data-show-upload="false" data-show-caption="true" placeholder="Select Images">
                                    </div>


                                    <div class="form-group">
                                        <button class="btn btn-primary mr15" type="submit"
                                                id="submitFormBtn">Upload
                                        </button>
                                        <button class="btn btn-danger backToListProductVariationPage" type="button">Cancel
                                        </button>
                                    </div>
                                </form>

                            </div>
                            <div class="clearfix"></div>
                            <div class="x_title">
                                <h5>List Gallery</h5>



                                <div class="clearfix"></div>
                            </div>
                            <div class="row" id="productVariationGalleryList">

                            </div>
                        </div>
                    </div>
                </div>



    </div>
</div>

@push('scripts')
<script>
    $('#variation_image_btn').on('click', function () {
        var countImg = $('.variation-images-list .variation-images').length;

        var imageHtml = '<div class="variation-images">' +
            '<input id="variation_image_thumbnail'+countImg+'" class="form-control" type="hidden" name="images[]">' +

            '<img id="variation_image_holder'+countImg+'" style=" margin-left:10px;margin-top:15px;max-height:100px;">' +
            '</div>';
        $('.variation-images-list').append(imageHtml);
        $(this).attr('data-input','variation_image_thumbnail')
    })
</script>
@endpush