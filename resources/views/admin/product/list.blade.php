@extends('layouts.admin')
@push('styles')


<link rel="stylesheet" href="{{asset('adminTemplate/asset//bootstrap-multiselect/bootstrap-multiselect.css')}}"
      type="text/css">


@endpush


@section('content')


<div class="main" id="listProductsPage">
    <div class="page-title">
        <div class="title_left">
            <h3> Product Management</h3>
        </div>
        <div class="info text-info">

        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')
                <div class="row">

                    <div class="col-md-12">
                        <a href="#" id="addProductBtn" class="btn btn-primary pull-right">Add New Product</a>
                    </div>

                    <table id="productTable" class="table table-bordered">

                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Variations</th>
                            <th>Slider</th>
                            <th>Featured</th>
                            <th>Special</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <!--                product Page-->


            </div>
        </div>


    </div>
</div>

@include('admin.product.gallery')

<!--    //jsTree-->

<!--include Product Modal-->
@include('admin.product.formModal')
<!--include ProductVariation Modal-->
@include('admin.product.variation.formModal')
@include('admin.product.variation.list')


@endsection
@push('scripts')


<script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script> $('#lfm').filemanager('file');</script>

<script> $('#variationFeaturedImg').filemanager('file');</script>





<!--ckeditor-->
<script src="{{asset('/packages/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/packages/ckeditor/adapters/jquery.js')}}"></script>
<script src="{{asset('build/pages/admin/category.js')}}"></script>
<script src="{{asset('build/pages/admin/product.js')}}"></script>
<script src="{{asset('build/pages/admin/productVariation.js')}}"></script>


<!--jquery Ui-->
<script src="{{asset('packages/jquery-ui/jquery-ui.min.js')}}"></script>
<!--fileInput-->
<link rel="stylesheet" href="{{asset('packages/fileinput/fileinput.min.css')}}">
<script src="{{asset('packages/fileinput/fileinput.min.js')}}"></script>

<!--Product gallery js-->
<script src="{{asset('build/pages/admin/productGallery.js')}}"></script>
<!--multi select input  -->
<script type="text/javascript"
        src="{{asset('adminTemplate/asset/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>


@endpush
