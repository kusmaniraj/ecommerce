<div id="productVariationModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add ProductVariation Form</h4>
            </div>
            <div class="modal-body">


                <form id="productVariationForm" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id">





                    <div class="form-group row showQty" style="display: none">
                        <div class="col-md-3">
                            <label for="featuredImg">Qty</label><br>
                        </div>
                        <div class="col-md-6">
                            <input  type="number" class="form-control" name="qty[]"
                                   value="1">
                        </div>

                    </div>

                    <div class="form-group row showSizeQty" style="display: none">
                        <div class="col-md-3">
                            <label for="featuredImg">Size and Qty</label><br>
                        </div>
                        <div class="col-md-9" id="addSizeQtyHtml">
                            <div class="col-md-12 sizeQtyList parentSizeQtyList ">

                                    <div class="col-md-4">
                                        <select name="size[]" class="form-control" required id="productVariation_size">


                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <input  type="number" class="form-control" name="qty[]"
                                               value="1">

                                    </div>
                                    <div class="col-md-2">
                                    <span class="btn btn-sm btn-success add_size_qty_attr_btn"><i
                                            class="fa fa-plus"></i></span>
                                    </div>



                            </div>
                        </div>


                    </div>

                    <div id="featuredImg" class=" form-group row ">
                        <div class="col-md-3">
                            <label for="featuredImg">Featured Image</label><br>
                        </div>
                        <div class="col-md-6">
                             <span class="input-group-btn">
                                     <a id="variationFeaturedImg" data-input="variationThumbnail"
                                        data-preview="variationHolder" class="btn btn-info">
                                         <i class="fa fa-picture-o"></i> Select ProductVariation Image
                                     </a>
                                   </span>
                            <input id="variationThumbnail" class="form-control" type="hidden" name="featuredImg">

                            <img id="variationHolder" style=" margin-left:10px;margin-top:15px;max-height:100px;">
                        </div>


                    </div>
                    <div id="status" class="form-group row">
                        <div class="col-md-3">
                            <label for="status">Status</label><br>
                        </div>
                        <div class="col-md-6">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="status" value="active" checked>
                                    Active
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="status"
                                           value="inactive">
                                    Inactive
                                </label>
                            </div>
                        </div>



                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary " type="submit">Create ProductVariation</button>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
    <script>

    </script>
</div>