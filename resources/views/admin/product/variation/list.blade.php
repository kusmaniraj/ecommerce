


<div class="main" id="listProductVariationsPage" style="display: none">
    <div class="page-title">
        <div class="pull-left">
            <h3> Product Variation</h3>
        </div>
        <div class="pull-right">
            <a href="#" class="backToListProductPage"><h4><i class="fa fa-arrow-circle-left"></i> Back To Product</h4></a>
        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')
                <div class="row">

                    <div class="col-md-12">
                        <a href="#" id="addProductVariationBtn" class="btn btn-primary pull-right">Add New ProductVariation</a>
                    </div>

                    <table id="productVariationTable" class="table table-bordered">

                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Image</th>
                            <th>Size </th>
                            <th>Qty</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <!--                productVariation Page-->


            </div>
        </div>


    </div>
</div>








