
@extends('layouts.admin')
@push('styles')
<link rel="stylesheet" href="{{asset('adminTemplate/asset/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
@endpush

@section('content')



<div class="main">
    <div class="page-title">
        <div class="title_left">

            <h5>{{$title}} Management </h5>

        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')


                <div class="row">
                    <div class="col-md-12">
                        <a href="#" id="addColorBtn" class="btn btn-primary pull-right">Add New {{$title}}</a>
                    </div>
                    <table id="colorTable" class="table table-bordered">


                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>


                    </table>
                </div>


            </div>
        </div>


    </div>
</div>




<!--include Color Modal-->
@include('admin.productAttribute.color.formModal')

@endsection
@push('scripts')
<!--color picker-->
<script src="{{asset('adminTemplate/asset/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('build/pages/admin/color.js')}}"></script>




@endpush
