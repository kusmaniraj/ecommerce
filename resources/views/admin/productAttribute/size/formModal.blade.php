
<div id="sizeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add {{$title}} Form</h4>
            </div>
            <div class="modal-body">



                <form id="sizeForm" method="post">

                    <input type="hidden" name="id">

                    <div class="form-group" id="name">
                        <label for="name">Size Name</label>
                        <input type="text" class="form-control" name="name"
                               value="">
                    </div>


                    <div id="status" class="form-group">
                        <label for="status">Status</label><br>

                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="status" value="active" checked>
                                Active
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="status"
                                       value="inactive">
                                Inactive
                            </label>
                        </div>
                    </div>


                    <div class="form-group">
                        <button class="btn btn-primary " type="submit">Create {{$title}}</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
    <script>

    </script>
</div>