
@extends('layouts.admin')
@push('styles')

@endpush

@section('content')



<div class="main">
    <div class="page-title">
        <div class="title_left">

            <h5>{{$title}} Management </h5>

        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')


                <div class="row">
                    <div class="col-md-12">
                        <a href="#" id="addSizeBtn" class="btn btn-primary pull-right">Add New {{$title}}</a>
                    </div>
                    <table id="sizeTable" class="table table-bordered">


                        <thead>
                        <tr>
                            <th>SN</th>

                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>


                    </table>
                </div>


            </div>
        </div>


    </div>
</div>




<!--include Size Modal-->
@include('admin.productAttribute.size.formModal')

@endsection
@push('scripts')
<!--size picker-->

<script src="{{asset('build/pages/admin/size.js')}}"></script>




@endpush
