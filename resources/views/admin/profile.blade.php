@extends('layouts.admin')

<?php
$defaultImg = asset('images/male.jpg');

$user = Auth::guard('admin')->user();
if ($user) {
    $id = $user['id'];
    $name = $user['name'];
    $email = $user['email'];

    $profileImg = $user['profileImg'];
}
?>

@section('content')

<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>

       
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class=" col-md-offset-3 col-md-6">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        @include('messages.alertMessages')
                        <form action="{{route('admin.profile.update')}}" method="post" enctype=multipatr/form-data>
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="user_name">Name</label>
                                <input type="text" class="form-control" id="user_name" name="name"
                                       value="{{$name}}">
                            </div>
                            @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{$email}}">
                            </div>
                            @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif




                                 <div class="input-group">
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Select Profile Image
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="hidden" name="profileImg">
                                    <input type="hidden" name="old_profileImg"
                                       value="{{$profileImg ? $profileImg : $defaultImg }}">
                                 <img id="holder" style=" margin-left:10px;margin-top:15px;max-height:100px;">
                                 </div>
                                 


                            


                            <h4 style="margin-top:50px;" class="text-info">*Only if you want to change the
                                password.*</h4>


                            <div class="form-group {{ Session::get('incorrect') ? 'has-error' : ''}}">
                                <label for="current_password">Current Password</label>
                                <input type="password" class="form-control" id="current_password"
                                       name="current_password">
                                @if (Session::get('incorrect'))
                                        <span class="help-block">
                                        <strong>{{Session::get('incorrect') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password">New Password
                                    <small></small>
                                </label>
                                <input type="password" class="form-control" id="password"
                                       name="password">
                                @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm">Confirm Password
                                    <small></small>
                                </label>
                                <input type="password" class="form-control" id="password_confirmation"
                                       name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group">
                                <button class="btn btn-primary mr15" type="submit">Save Changes</button>
                                <button class="btn btn-danger" type="button" id="cancelBtn"
                                        onclick="window.history.back()">Cancel
                                </button>
                            </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@push('scripts')
 <script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script >
     $('#lfm').filemanager('file');
     var img = "{{$profileImg}}";
    if (img != "") {
        $('#holder').removeClass('hidden').attr('src','{{asset($profileImg)}}').attr('class','thumbnail');;
    }
</script>
@endpush