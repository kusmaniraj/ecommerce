@extends('layouts.admin')


@section('content')
<?php


if (!empty($getSetting)) {


    $routeAction = route('setting.update', $getSetting['id']);
    $method = method_field('PUT');
    $status = 'Update';


} else {

    $routeAction = route('setting.store');
    $method = "";
    $status = 'Create';
}

?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        <form action="{{$routeAction}}" method="post">
            @include('messages.alertMessages')
            <input type="hidden" name="id" value="{{$getSetting['id']}}">
            {{$method}}
            {{csrf_field()}}

            <div class="col-md-12" id="general_setting">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row x_title">
                            <div class="col-md-6">
                                <h4>General

                                </h4>
                            </div>

                        </div>
                        <div class="row">



                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="website_name">Website Name</label>
                                        <input type="text" class="form-control" id="website_name " name="website_name"
                                               value="{{$getSetting['website_name'] ? $getSetting['website_name']  : old('website_name') }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="website_name_short">Website Short Name</label>
                                        <input type="text" class="form-control" id="website_name_short"
                                               name="website_name_short"
                                               value="{{$getSetting['website_name_short'] ? $getSetting['website_name_short']  : old('website_name_short') }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="website_notice">Website Notice</label>
                                <textarea class="form-control" id="website_notice"
                                          name="website_notice" rows="3">
                                  {{$getSetting['website_notice'] ? $getSetting['website_notice']  : old('website_notice') }}
                                    </textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="meta_title">Meta Title</label>
                                        <input type="text" class="form-control" id="meta_title " name="meta_title"
                                               value="{{$getSetting['meta_title'] ? $getSetting['meta_title']  : old('meta_title') }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="meta_keywords">Meta Key Words</label>
                                        <input type="text" class="form-control" id="meta_keywords"
                                               name="meta_keywords"
                                               value="{{$getSetting['meta_keywords'] ? $getSetting['meta_keywords']  : old('meta_keywords') }}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="meta_description">Meta Description</label>
                                <textarea class="form-control" id="meta_description"
                                          name="meta_description" rows="3">
                                  {{$getSetting['meta_description'] ? $getSetting['meta_description']  : old('meta_description') }}
                                    </textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="slogan">Website Slogan/Tag Line</label>
                                <textarea name="slogan" id="slogan" rows="5"
                                          class="form-control"> {{$getSetting['slogan'] ? $getSetting['slogan']  : old('slogan') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="short_description">Short Description</label>
                                <textarea name="short_description" id="short_description" rows="5"
                                          class="form-control">{{$getSetting['short_description'] ? $getSetting['short_description']  : old('short_description') }}</textarea>
                                    </div>
                                </div>

                            </div>



                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" id="social_setting">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row x_title">
                            <div class="col-md-6">
                                <h4>Social Media Page

                                </h4>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="fb_url">Facebook Page/Profile Url</label>
                                    <input type="text" class="form-control" id="fb_url" name="fb_url"
                                           value="{{$getSetting['fb_url'] ? $getSetting['fb_url']  : old('fb_url') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="google_plus_url">Google Plus Url</label>
                                    <input type="text" class="form-control" id="google_plus_url" name="google_plus_url"
                                           value="{{$getSetting['google_plus_url'] ? $getSetting['google_plus_url']  : old('google_plus_url') }}">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="skype_url">Skype Url</label>
                                    <input type="text" class="form-control" id="skype_url" name="skype_url"
                                           value="{{$getSetting['skype_url'] ? $getSetting['skype_url']  : old('skype_url') }}">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="twitter_url">Twitter Url</label>
                                    <input type="text" class="form-control" id="twitter_url" name="twitter_url"
                                           value="{{$getSetting['twitter_url'] ? $getSetting['twitter_url']  : old('twitter_url') }}">
                                </div>
                            </div>


                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="google_maps_iframe">Google Maps Iframe</label>
                                <textarea name="google_maps_iframe" id="google_maps_iframe" rows="5"
                                          class="form-control">{{$getSetting['google_maps_iframe'] ? $getSetting['google_maps_iframe']  : old('google_maps_iframe') }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fb_page">Facebook page Iframe</label>
                                <textarea name="fb_page" id="fb_page" rows="5"
                                          class="form-control">{{$getSetting['fb_page'] ? $getSetting['fb_page']  : old('fb_page') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12" id="email_name_contact_setting">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row x_title">
                            <div class="col-md-6">
                                <h4>Email,Name and Contact

                                </h4>
                                <p for="note">Note:*Email ,Name and Contact use in Mailer*</p>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="owner_name">Company/Owner Name</label>
                                    <input type="text" class="form-control" id="owner_name" name="owner_name"
                                           value="{{$getSetting['owner_name'] ? $getSetting['owner_name']: old('owner_name')}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="owner_email">Company/Owner Email</label>
                                    <input type="email" class="form-control" id="owner_email" name="owner_email"
                                           value="{{$getSetting['owner_email'] ? $getSetting['owner_email']  : old('owner_email') }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="owner_address">Company/Owner Address</label>
                                    <input type="text" class="form-control" id="owner_address" name="owner_address"
                                           value="{{$getSetting['owner_address'] ? $getSetting['owner_address']  : old('owner_address') }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="owner_contact">Company/Owner Contact No.</label>
                                    <input type="number" class="form-control" id="owner_contact" name="owner_contact"
                                           value="{{$getSetting['owner_contact'] ? $getSetting['owner_contact']  : old('owner_contact') }}">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row x_title">
                            <div class="col-md-6">
                                <h4>System Version

                                </h4>
                            </div>

                        </div>
                        <div class="row">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="version">System Version</label>
                                        <input
                                            type="number" class="form-control" id="version" name="version"
                                            value="{{$getSetting['version']}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary mr15" type="submit">{{$status}}
                                </button>
                                <button class="btn btn-danger" type="button" id="cancelBtn"
                                        onclick="window.history.back()">Cancel
                                </button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


        </form>
    </div>

</div>
</div>

@endsection


@push('scripts')
<script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script>
    $('#lfm').filemanager('file');
    var img = "{{$getSetting['logo']}}";
    if (img != "") {
        $('#holder').removeClass('hidden').attr('src', '{{asset($getSetting["logo"])}}').attr('class', 'thumbnail');
    }
</script>
@endpush