@extends('layouts.admin')


@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        @include('messages.alertMessages')

        <div class="col-md-12">
            <div class="col-md-6" id="logo_setting">
                <form id="logoImageForm">

                    {{csrf_field()}}
                    <input type="hidden" name="image_icon_type" value="logo_image">
                    <input type="hidden" name="id" value="@isset($logoImage){{$logoImage->id}}@endisset">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="row x_title">
                                <div class="col-md-6">
                                    <h4>Logo Image

                                    </h4>
                                </div>

                            </div>
                            <div class="row">
                            <span class="input-group-btn">
                                                                 <a id="lfm1" data-input="thumbnail_1"
                                                                    data-preview="holder1" class="btn btn-info">
                                                                     <i class="fa fa-picture-o"></i> Select Logo Image
                                                                 </a>
                                                               </span>
                                <input id="thumbnail_1" value="@isset($logoImage){{$logoImage->original_image_url}}@endisset" class="form-control" type="hidden" name="original_image_url">
                                
                                <img id="holder1" @isset($logoImage) src="{{$logoImage->thumb_image_url}}" @endisset class="" style=" margin-left:10px;margin-top:15px;max-height:100px;">

                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <button class="btn btn-primary pull-right" type="submit">Upload
                                    </button>

                                </div>

                            </div>


                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6" id="favicon_setting">

                <form id="faviconForm">

                    {{csrf_field()}}
                    <input type="hidden" name="image_icon_type" value="favicon">
                    <input type="hidden" name="id" value="@isset($favicon){{$favicon->id}}@endisset">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="row x_title">
                                <div class="col-md-6">
                                    <h4>Favicon


                                    </h4>
                                    <p><srong>Note</srong> &nbsp;*Please select icon size 25x25 *</p>
                                </div>

                            </div>
                            <div class="row">
                            <span class="input-group-btn">
                                                                 <a id="lfm2" data-input="thumbnail_2"
                                                                    data-preview="holder2" class="btn btn-info">
                                                                     <i class="fa fa-picture-o"></i> Select Favicon
                                                                 </a>
                                                               </span>
                                <input id="thumbnail_2" value="@isset($favicon){{$favicon->original_image_url}}@endisset" class="form-control" type="hidden" name="original_image_url">

                                <img id="holder2" @isset($favicon) src="{{$favicon->thumb_image_url}}" @endisset class="" style=" margin-left:10px;margin-top:15px;max-height:100px;">

                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <button class="btn btn-primary pull-right" type="submit">Upload
                                    </button>

                                </div>

                            </div>


                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6" id="no_image_setting">
                <form id="noImageForm">

                    {{csrf_field()}}
                    <input type="hidden" name="image_icon_type" value="no_image">
                    <input type="hidden" name="id" value="@isset($noImage){{$noImage->id}}@endisset">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="row x_title">
                                <div class="col-md-6">
                                    <h4>No Image

                                    </h4>
                                </div>

                            </div>
                            <div class="row">
                            <span class="input-group-btn">
                                                                 <a id="lfm3" data-input="thumbnail_3"
                                                                    data-preview="holder3" class="btn btn-info">
                                                                     <i class="fa fa-picture-o"></i> Select No Image
                                                                 </a>
                                                               </span>
                                <input id="thumbnail_3" value="@isset($noImage){{$noImage->original_image_url}}@endisset" class="form-control" type="hidden" name="original_image_url">

                                <img id="holder3" @isset($noImage) src="{{$noImage->thumb_image_url}}" @endisset class="" style=" margin-left:10px;margin-top:15px;max-height:100px;">

                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <button class="btn btn-primary pull-right" type="submit">Upload
                                    </button>

                                </div>

                            </div>


                        </div>
                    </div>
                </form>
            </div>

        </div>


    </div>

</div>
</div>

@endsection


@push('scripts')
<script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script src="{{asset('build/pages/admin/imageIconSetting.js')}}"></script>
<script>
    $('#lfm1').filemanager('file');
    $('#lfm2').filemanager('file');
    $('#lfm3').filemanager('file');
</script>


@endpush