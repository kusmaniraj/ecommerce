@extends('layouts.admin')
<?php


if (!empty($mailer)) {


    $routeAction = route('mailer.update', $mailer['id']);
    $method = method_field('PUT');
    $status = 'Update';
    $id= $mailer['id'];


} else {

    $routeAction = route('mailer.store');
    $method = "";
    $status = 'Create';
    $id="";
}

?>

@section('content')


<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>@isset($title){{$title}}@endisset</h3>
        </div>


    </div>

    <div class="clearfix"></div>

    <div class="row">
        @include('messages.alertMessages')

        <div class="col-md-12">

            <form id="mailerForm" method="post" action="{{$routeAction}}">

                {{csrf_field()}}
                {{$method}}

                <input type="hidden" name="id" value="{{$id}}">

                <div class="x_panel">
                    <div class="x_content">
                        <div class="row x_title">
                            <div class="col-md-6">
                                <h4>Mailer Setting

                                </h4>
                            </div>

                        </div>
                        <div class="row form-group">
                            <div class="col md 3">
                                <label for="driver">Mail Driver <span class="required">*</span></label>
                            </div>
                            <div class="col-md-6">
                                <select class="form-control" name="driver" id="mail_driver">
                                    <option value="0" selected disabled>Select Driver</option>
                                    <option {{$mailer['driver']=='smtp' ? 'selected':''}} value="smtp">SMTP</option>
                                    <option {{$mailer['driver']=='sendmail' ? 'selected':''}} value="sendmail">Send Mail</option>
                                    <option {{$mailer['driver']=='mailgun' ? 'selected':''}} value="mailgun">Mailgun</option>
                                    <option {{$mailer['driver']=='ses' ? 'selected':''}} value="ses">Ses</option>
                                    <option {{$mailer['driver']=='mandrill' ? 'selected':''}} value="mandrill">Mandrill</option>

                                </select>
                            </div>

                        </div>
                        <div class="row form-group">
                            <div class="col md 3">
                                <label for="host">Mail Host <span class="required">*</span></label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="host" value="{{$mailer['host'] ? $mailer['host'] :old('host') }}" class="form-control" placeholder="smtp.mailtrap.io">
                            </div>

                        </div>
                        <div class="row form-group">
                            <div class="col md 3">
                                <label for="port">Mail Port <span class="required">*</span></label>
                            </div>
                            <div class="col-md-6">
                                <input type="number" value="{{$mailer['port'] ? $mailer['port'] :old('port') }}" name="port" class="form-control" placeholder="2525">
                            </div>

                        </div>
                        <div class="row form-group">

                            <div class="col md 3">
                                <label for="encryption">Mail Encryption</label>
                            </div>
                            <div class="col-md-6">
                                <select class="form-control" name="encryption" id="mail_encryption">
                                    <option value="0" selected disabled>Select Encryption</option>
                                    <option {{$mailer['encryption'] ? 'selected':''}}  value="tls">TLS</option>
                                </select>
                            </div>

                        </div>
                        <div class="row form-group">
                            <div class="col md 3">
                                <label for="username">Username <span class="required">*</span></label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="username" value="{{$mailer['username'] ? $mailer['username'] :old('username') }}" class="form-control">
                            </div>

                        </div>
                        <div class="row form-group">
                            <div class="col md 3">
                                <label for="password">Password <span class="required">*</span></label>
                            </div>
                            <div class="col-md-6 input-group">
                                <input type="password"  value="{{$mailer['password']}}" name="password" class="form-control">
                              <span class="input-group-btn">
                              <button type="button" class="btn btn-primary toggleHideShowPassword"><i class="togglePasswordEye fa fa-eye-slash"></i></button>
                            </span>
                            </div>


                        </div>


                        <div class="row">
                            <div class="form-group">
                                <button class="btn btn-primary  pull-left" type="submit">Save
                                </button>

                            </div>

                        </div>


                    </div>
                </div>
            </form>


        </div>


    </div>

</div>
</div>

@endsection


@push('scripts')

<script>
    $('.toggleHideShowPassword').on('click',function(e){
        e.preventDefault();
        $('.togglePasswordEye').toggleClass('fa-eye-slash fa-eye');

        let inputType=$('#mailerForm input[name="password"]').attr('type');
        if(inputType=='password'){
            $('#mailerForm input[name="password"]').attr('type','text')
        }else{
            $('#mailerForm input[name="password"]').attr('type','password')
        }
    })

</script>


@endpush