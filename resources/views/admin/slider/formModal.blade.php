
<div id="sliderModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add {{$title}} Form</h4>
            </div>
            <div class="modal-body">



                <form id="sliderForm" method="post">

                    <input type="hidden" name="id">

                    <div class="form-group" id="title">
                        <label for="title">Slider Title</label>
                        <input type="text" class="form-control" name="title"
                               value="">
                    </div>
                    <div class="form-group" >
                        <label for="name">Slider Sort Description</label>
                        <textarea id="" rows="3" class="form-control" name="shortDescription"
                                  required></textarea>

                    </div>

                    <div class="form-group">
                        <label for="category">Category <span class="required text-danger">*</span></label>
                        <select class="form-control selectpicker" name="category_id"
                                data-show-subtext="true" data-live-search="true" required>
                            <option value='0' disabled selected>Select Category </option>

                            @isset($categories)
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                            @endisset
                        </select>
                    </div>
                    <div  class=" form-group col-md-12 ">
                        <p>Note : *Select Image 1920x530 better and also set for slider</p>

                        <div class="col-md-3">
                            <label for="featuredImg">Featured Image<span class="required">*</span></label><br>
                        </div>
                        <div class="col-md-9">
                                     <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                         <i class="fa fa-picture-o"></i> Select Product Image
                                     </a>
                                   </span>
                            <input id="thumbnail" class="form-control" type="hidden" name="featuredImg">

                            <img id="holder" style=" margin-left:10px;margin-top:15px;max-height:100px;">
                        </div>


                    </div>

                    <div id="status" class="form-group">
                        <label for="status">Status</label><br>

                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="status" value="active" checked>
                                Active
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="status"
                                       value="inactive">
                                Inactive
                            </label>
                        </div>
                    </div>


                    <div class="form-group">
                        <button class="btn btn-primary " type="submit">Create {{$title}}</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>

                </form>

            </div>

        </div>

    </div>
    <script>

    </script>
</div>