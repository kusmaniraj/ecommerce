@extends('layouts.admin')
@push('styles')


@endpush


@section('content')


<div class="main" id="listUserPage">
    <div class="page-title">
        <div class="title_left">
            <h3> User Manage</h3>
        </div>


    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')
                <div class="row">


                    <table id="userTable" class="table table-bordered">

                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Account Information</th>
                            <th>Shipping Information</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>


            </div>
        </div>


    </div>
</div>
<!--shipping list-->
@include('admin.user.shippingInfo.list')
<!--show account info detail modal-->
@include('admin.user.showAccountInfo')

<!--show shipping detail modal-->
@include('admin.user.shippingInfo.showShippingInfo')


@endsection
@push('scripts')
<!--jquery Ui-->
<script src="{{asset('packages/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('build/pages/admin/user.js')}}"></script>
@endpush
