<div class="main" id="shippingInfoPage" style="display: none">
    <div class="page-title">
        <div class="title_left">
            <h3> User ShippingInfo</h3>
        </div>
        <div class="pull-right">
            <a href="#" class="backToListUserPage"><h4><i class="fa fa-arrow-circle-left"></i> Back</h4></a>
        </div>

    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                @include('messages.alertMessages')
                <div class="row">


                    <table id="shippingInfoTable" class="table table-bordered">

                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Default</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>


            </div>
        </div>


    </div>
</div>