<div id="showAccountInfoModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Account Information</h4>
            </div>
            <div class="modal-body">
                <div class="row " id="accountInfo">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>
                                <stong>Name :</stong>
                            </label>
                        </div>
                        <div class="col-md-offset-1 col-md-6">
                            <p class="name"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>
                                <stong>Address :</stong>
                            </label>
                        </div>
                        <div class="col-md-offset-1 col-md-6">
                            <p class="address"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>
                                <stong>Email :</stong>
                            </label>
                        </div>
                        <div class="col-md-offset-1 col-md-6">
                            <p class="email"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>
                                <stong>Gender :</stong>
                            </label>
                        </div>
                        <div class="col-md-offset-1 col-md-6">
                            <p class="gender"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>
                                <stong>Region :</stong>
                            </label>
                        </div>
                        <div class="col-md-offset-1 col-md-6">
                            <p class="region"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>
                                <stong>City :</stong>
                            </label>
                        </div>
                        <div class="col-md-offset-1 col-md-6">
                            <p class="city"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>
                                <stong>Telephone :</stong>
                            </label>
                        </div>
                        <div class="col-md-offset-1 col-md-6">
                            <p class="telephone"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>
                                <stong>Fax :</stong>
                            </label>
                        </div>
                        <div class="col-md-offset-1 col-md-6">
                            <p class="fax"></p>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    </div>


                </div>


            </div>

        </div>

    </div>

</div>
<script>

</script>
</div>