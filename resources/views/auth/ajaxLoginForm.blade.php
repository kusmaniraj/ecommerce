<div class="modal fade" id="loginFormModal">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Login</h4>
                <button type="button" class="pull-left close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <p class="before-login-text">Welcome back! Sign in to your account</p>

                        <form id="ajaxLoginForm" class="form-horizontal" >

                            <div class="form-group row " >
                                <label for="email" class="col-md-4 control-label">E-Mail Address <span
                                        class="required text-danger">*</span></label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control" name="email" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <label for="password" class="col-md-4 control-label">Password<span
                                        class="required text-danger">*</span></label>

                                <div class="col-md-8">
                                    <input id="password" type="password" class="form-control" name="password" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label class="inline" for="rememberme">
                                            <input type="checkbox" value=""
                                                   id="rememberme" name="rememberme"> Remember me
                                        </label>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button class="button loginBtn" type="submit"><i class="fa fa-lock"></i>&nbsp;
                                        <span>Login</span></button>

                                </div>
                                <div class="col-md-8 col-md-offset-4">
                                    <a class=" forgotPwdLink " href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                    |
                                    <a href="{{ route('register') }}">
                                        Register
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="login-separator">
                        <span>or</span>
                    </div>
                    <div class="col-md-4 col-md-offset-2">
                        <p>Sign in with your social account</p>

                        <a href="{{ url('user/facebook') }}" class="btn btn-flat btn-facebook"><i class="fa fa-facebook"></i> Facebook Login</a>
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>

        </div>
    </div>
</div>