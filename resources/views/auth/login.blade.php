@extends('layouts.web')

@section('content')
<!-- Breadcrumbs  -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li class="home"><a title="Go to Home Page" href="{{url('/')}}">Home</a><span>»</span></li>
                    <li><strong>Login</strong></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<section class="main-container ">
    <div class="container">
        <div class="page-content">
            <div class="page-title">
                <h3>Login</h3>
            </div>




                <div class="col-md-8">
                    <h4 class="">Welcome back! Sign in to your account</h4>

                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class=" row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-2 control-label">E-Mail Address <span
                                    class="required text-danger">*</span></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}"
                                       required
                                       autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class=" row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-2 control-label">Password<span
                                    class="required text-danger">*</span></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="checkbox">
                                    <label class="inline" for="rememberme">
                                        <input type="checkbox" value=" {{ old('remember') ? 'checked' : '' }}"
                                               id="rememberme" name="rememberme"> Remember me
                                    </label>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button class="button loginBtn" type="submit"><i class="fa fa-lock"></i>&nbsp;
                                    <span>Login</span></button>
                                |


                                <a class=" forgotPwdLink " href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                                |
                                <a href="{{ route('register') }}">
                                    Register
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class=" col-md-4 ">
                    <h4 class=""> Sign in to your account with Facebook</h4>
                    <a href="{{ url('user/facebook') }}" class="btn btn-flat btn-facebook"><i
                            class="fa fa-facebook"></i> Facebook Login</a>
                </div>



        </div>
    </div>
</section>
<!-- Main Container End -->


@endsection
