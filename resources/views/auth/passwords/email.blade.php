@extends('layouts.web')

@section('content')
<!-- Breadcrumbs  -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li class="home"><a title="Go to Home Page" href="{{url('/')}}">Home</a><span>»</span></li>
                    <li><strong>Forget Password</strong></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<section class="main-container ">
    <div class="container">
        <div class="page-content">

            <div class="page-title">
                <h3>Forget Password</h3>
            </div>

            <div class="container" style="width: 100%">

                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif

                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class=" row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-2 control-label">E-Mail Address<span
                                class="required text-danger">*</span></label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required>

                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                            <button type="submit" class="button sendPwdLink">
                                Send Password Reset Link
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>


    </div>
</section>
<!-- Main Container End -->

@endsection
