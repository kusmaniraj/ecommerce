@extends('layouts.web')

@section('content')
<!-- Breadcrumbs  -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li class="home"><a title="Go to Home Page" href="{{url('/')}}">Home</a><span>»</span></li>
                    <li><strong>Register</strong></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<section class="main-container ">
    <div class="container">
        <div class="page-content">
            <div class="page-title">
                <h3>Register</h3>
            </div>
            <div class="col-md-6">

                <h4>Create your very own account</h4>
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class=" row form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-3 control-label">Name <span
                                class="required text-danger">*</span></label>

                        <div class="col-md-9">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required
                                   autofocus>

                            @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class=" row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-3 control-label">E-Mail Address <span
                                class="required text-danger">*</span></label>

                        <div class="col-md-9">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                   required>

                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class=" row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-3 control-label">Password <span
                                class="required text-danger">*</span></label>

                        <div class="col-md-9">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class=" row form-group">
                        <label for="password-confirm" class="col-md-3 control-label">Confirm Password <span
                                class="required text-danger">*</span></label>

                        <div class="col-md-9">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                                   required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3  ">
                            <button type="submit" class="button registerBtn">
                                Register
                            </button>
                            <a href="{{ route('login') }}" class="button-back">
                                 Back To Login
                            </a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <h4>Sign up today and you will be able to :</h4>
                <div class="register-benefits">

                    <ul>
                        <li>Speed your way through checkout</li>
                        <li>Track your orders easily</li>
                        <li>Keep a record of all your purchases</li>
                    </ul>
                </div>
            </div>


        </div>
    </div>

</section>
<!-- Main Container End -->



@endsection
