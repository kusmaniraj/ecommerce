<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
@isset($getSetting)
<?php
$websiteName=$getSetting['website_name'];
$websiteShortName=$getSetting['website_name_short'];
$websiteNotice=$getSetting['website_notice'];
$facebookUrl=$getSetting['fb_url'];
$googlePlusUrl=$getSetting['google_plus_url'];
$skypeUrl=$getSetting['skype_url'];
$twitterUrl=$getSetting['twitter_url'];
$logo=$getSetting['logo'];
$googleMap=$getSetting['google_maps_iframe'];
$ownerEmail=$getSetting['owner_email'];
$ownerName=$getSetting['owner_name'];
$ownerContact=$getSetting['owner_contact'];
$ownerAddress=$getSetting['owner_address'];
$version=$getSetting['version'];


?>
@else
<?php
$websiteName='';
$websiteShortName='';
$websiteNotice='';
$facebookUrl='';
$googlePlusUrl='';
$skypeUrl='';
$twitterUrl='';
$logo='';
$googleMap='';
$ownerEmail='';
$ownerName='';
$ownerContact='';
$ownerAddress='';
$version='';
?>
@endisset
@include('web.include.head')
<body class="cms-index-index cms-home-page">

<!-- mobile menu -->
@include('web.include.mobileMenu')
<!-- end mobile menu -->
<div id="page">
    <!--    header-->
    @include('web.include.header')
    <!--    end of header-->

    <!--    navbar-->
    @include('web.include.navbar')
    <!--    end of navbar-->


    @yield('content')

    @include('web.include.footer')
</div>


<!--start scripts-->

</body>

<!-- JS -->

<!-- jquery js -->
<script type="text/javascript" src="{{asset('web\js/jquery.min.js')}}"></script>

<!-- bootstrap js -->
<script type="text/javascript" src="{{asset('web\js/bootstrap.min.js')}}"></script>


<!-- owl.carousel.min js -->
<script type="text/javascript" src="{{asset('web\js/owl.carousel.min.js')}}"></script>
<!-- bxslider js -->
<script type="text/javascript" src="{{asset('web\js/jquery.bxslider.js')}}"></script>



<!-- megamenu js -->
<script type="text/javascript" src="{{asset('web\js/megamenu.js')}}"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var mega_menu = '0';

    /* ]]> */
</script>

<!-- jquery.mobile-menu js -->
<script type="text/javascript" src="{{asset('web\js/mobile-menu.js')}}"></script>

<!--jquery-ui.min js -->
<script type="text/javascript" src="{{asset('web\js/jquery-ui.js')}}"></script>

<!-- main js -->
<script type="text/javascript" src="{{asset('web\js/main.js')}}"></script>
<script src="{{asset('web/assets/datatables/datatables.js')}}"></script>


<script>
    var base_url = window.location.origin;
</script>
<script src="{{asset('build/shared/ajaxHelper.js')}}"></script>
<script>
    $("#msg-alert").fadeTo(5000, 1000).slideUp(1000, function () {
        $("#success-alert").slideUp(1000);
    });
</script>
<script>
    $("#alert-addCart,#alert-order")
        .css('opacity', 0)
        .slideDown('slow')
        .animate(
            { opacity: 1 },
            { queue: false, duration: 'slow' }
        );
</script>
<script>
    $('.remove').click(function () {
        var ask_permission = confirm("Are you sure want to Remove ?");
        if (ask_permission == true) {
            return true;
        }
        else {
            return false;
        }
    });
</script>
<script src="{{asset('build/shared/loading.js')}}"></script>
<!-- <script src="{{asset('pages/admin/notification.js')}}"></script> -->
<!--alert message-->
<script src="{{asset('packages/alertify/lib/alertify.min.js')}}"></script>
<script src="{{asset('build/js/alertMessage.js')}}"></script>
@stack('scripts')


</html>
