<html>
<head>
    <title> Confrim Order products from {{$receiver['website_name']}}</title>
</head>
<body>

<div class="title">
    <h4>You have Order products from {{$receiver['website_name']}}</h4>
    <p>**This is an automatically generated email,please don't reply**</p>
</div>
<div class="customer-info">
    <ul type="none">
        <li><strong>{{$sender['firstName']}} &nbsp; {{$sender['lastName']}}</strong></li>
        <li>{{$sender['userEmail']}}</li>
        <li>{{$sender['address']}}</li>
        <li>{{$sender['region']}},{{$sender['city']}}</li>
        <li>{{$sender['telephone']}}</li>

    </ul>
</div>
<div class="box-border" id="placeOrderPage">
    <h4>Your Orders</h4>
    <div class="page-content page-order">
        @isset($orderList)
      

        <div class="table-responsive">


            <table class="table table-bordered " id="placeOrderTable">

                <thead>
                <tr>
                    <th>Dates</th>

                    <th>Product</th>
                    <th>Description</th>
                    <th>Qty</th>
                    <th>Price</th>



                </tr>
                </thead>
                <tbody>
                @php
                $totalQty=0;
                $totalPrice=0;
                @endphp


             


                <tr>
                    <td>
                        {{$orderList['date']}}
                    </td>
                    <td>
                        @for($i=0;$i<count($orderList['product']);$i++)
                        <img src="{{asset($orderList['productVariation'][$i]['featuredImg'])}}"
                             height="50px" width="50px" alt=" {{$orderList['product'][$i]['name']}}">
                        <hr>

                        @endfor
                    </td>
                    <td>
                        @for($i=0;$i<count($orderList['product']);$i++)
                        <p>

                            {{$orderList['product'][$i]['name']}}</p>
                        @if($orderList['orderSizeName'][$i])
                        <p>Size:&nbsp;{{$orderList['orderSizeName'][$i]}}</p>
                        @else
                        </br>
                        @endif
                        <hr>
                        @endfor
                    </td>
                    <td>
                        @for($i=0;$i<count($orderList['orderQty']);$i++)
                        @php
                        $totalQty +=$orderList['orderQty'][$i];
                        @endphp
                        <p>

                            {{$orderList['orderQty'][$i]}}
                        </p>
                        <br>
                        <hr>
                        @endfor
                    </td>
                    <td>
                        @for($i=0;$i<count($orderList['orderPrice']);$i++)
                        @php
                        $totalPrice +=$orderList['orderPrice'][$i];
                        @endphp
                        <p>


                            Rs: {{$orderList['orderPrice'][$i]}}
                        </p>
                        <br>

                        <hr>
                        @endfor
                    </td>






                </tr>
                <tr>


                    <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                    <td><strong>{{$totalQty}}</strong></td>
                    <td><strong>Rs : {{$totalPrice}}</strong></td>


                </tr>

             




                </tbody>


            </table>


        </div>
       
        @endisset
    </div>
</div>


</body>
</html>