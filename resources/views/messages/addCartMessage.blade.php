@if(Session::get('cartMessage'))
@php
$cartData=Session::get('cartMessage');

@endphp


<div class="alert alert-success" role="alert" id="alert-addCart" style="display: none">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="alert-heading">{{$cartData['status']=='error' ? 'Well done !!':'Well done!!'}}</h4>

    <p class="product">

        <img src="{{asset($cartData['cart']['variationWithProduct']->featuredImg)}}" alt="" height="50px" width="50px">
        {{$cartData['cart']['variationWithProduct']->product->name}}
        </br>
        {{$cartData['cart']['selectedQty']}} x
        @if($cartData['cart']['variationWithProduct']->product->newPrice)
        Rs: {{$cartData['cart']['variationWithProduct']->product->newPrice }}
        @else
       Rs :{{ $cartData['cart']['variationWithProduct']->product->newPrice}}
        @endif
        =Rs: {{$cartData['cart']['sub_total']}}
        </br>

        @isset($cartData['cart']['selectedSizeName'])
        Size:&nbsp;{{$cartData['cart']['selectedSizeName']}}
        @endisset

    </p>

    <p>{{$cartData['message']}}</p>
    <hr>
    <p class="mb-0">

        <a class=" btn btn-default" href="{{url('cart')}}"><i class="fa fa-shopping-cart"> </i>&nbsp;
            View Cart</a>
        <a class="btn btn-default" href="{{url('checkout')}}"><i class="fa fa-check"></i>
            Proceed to checkout</a></p>
</div>
@endif


<style>
    #alert-addCart {
        text-align: center;
        background-color: #42b069;
        border-radius: 0px;
        color: #FFFFFF;
    }
    #alert-addCart .product{
        color: #000;
    }
</style>