@if(Session::get('orderMessage'))
@php
$orderData=Session::get('orderMessage');

@endphp


<div class="alert alert-{{$orderData['status']=='error' ? 'error!!':'success'}}" role="alert" id="alert-order" style="display: none">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="alert-heading">{{$orderData['status']=='error' ? 'Sorry!!':'Well done!!'}}</h4>

    <p>
        {{$orderData['message']}}
    </p>

        
   
</div>
@endif


<style>
    .alert-success#alert-order{
        text-align: center;
        background-color: #42b069;
        border-radius: 0px;
        color: #FFFFFF;
    }
    .alert-error#alert-order {
        text-align: center;
        background-color: darkred;
        border-radius: 0px;
        color: #FFFFFF;
    }

</style>