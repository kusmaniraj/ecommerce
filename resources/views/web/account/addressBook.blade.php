@extends('web.account.layout.account')


@section('accountContent')
<div class="col-main col-sm-9 col-xs-12">
    <div class="my-account">
        <div class="page-title">
            <h2>My Address Book (Shipping Information)</h2>

            <p>Note:&nbsp; Selected address book use as Shipping Information &nbsp; <a href="#" class=" addShippingInfoBtn text-info"><i class="fa fa-plus"></i>&nbsp;Additional Shipping Info</a></p>

            </br>
        </div>
        <div class="box-border" id="addressBookPage">
            <div class="page-content page-order">
                @isset($shippingInfo)
                @if(count($shippingInfo)>0)

                <div class="table-responsive">


                    <table class="table table-bordered " id="addressBookTable">
                        @include('messages.alertMessages')
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Telephone</th>
                            <th>Status</th>
                            <th class="action">Action</th>
                        </tr>
                        </thead>
                        <tbody>


                        @foreach($shippingInfo as $key=>$info)

                        <tr>
                            <td>{{$info->firstName}}&nbsp; {{$info->lastName}}</td>
                            <td>{{$info->email}}</td>
                            <td>{{$info->address}}</td>
                            <td>{{$info->telephone}}</td>
                            <th>
                                @if($info->default=='yes')
                                <a href="#"  class="btn btn-success">Selected</a>
                                @else
                                <a href="{{url('myAccount/changeShippingInfoDefault/'.$info->id)}}"  class="btn btn-default">Use This Information</a>
                                @endif
                            </th>

                            <td>
                                @if($info->default=='yes' || $info->email==$info->userEmail)
                                <a href="#"   class="editShippingInfoBtn btn btn-primary" data-id="{{$info->id}}" ><i class="fa fa-edit ">&nbsp;Change/View</i></a>
                                @else
                                <a href="#" class="editShippingInfoBtn btn btn-primary" data-id="{{$info->id}}" ><i class="fa fa-edit ">&nbsp;Change/View</i></a><hr>
                                <a href="{{url('myAccount/removeAddressBook/'.$info->id)}}" class="remove  btn btn-danger"><i class="fa fa-remove ">&nbsp;Remove</i></a>
                                @endif
                            </td>

                        </tr>
                        @endforeach


                        </tbody>


                    </table>
                    <div class="pagination-area">

                        {{$shippingInfo->links()}}

                    </div>



                </div>
                @else
                <h5>Please fill Account Information for address book...</h5>
                @endif
                @endisset
            </div>
        </div>

    </div>
</div>
<!--shipping modal-->
@include('web.account.shippingInfoModal')
@endsection
@push('scripts')
<script>
    $('#addressBookTable').DataTable({
        processing: true,
    });
</script>
@endpush
