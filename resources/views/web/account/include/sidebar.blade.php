@push('styles')
<style>
    .current a{
        color:#000000;
        font-weight:900;

    }
</style>
@endpush
<aside class="right sidebar col-sm-3 col-xs-12">

    <div class="sidebar-account block">
        <div class="sidebar-bar-title">
            <h3>My Account</h3>
        </div>
        <div class="block-content">
            <ul>
                <li class="{{$headerTitle=="Account Dashboard" ? "current": ""}} "><a href="{{url('myAccount/')}}">Account Dashboard</a></li>
                <li class="{{$headerTitle=="Account Information" ? "current": ""}} "><a href="{{url('myAccount/accountInformation')}}">Account Information</a></li>
                <li class="{{$headerTitle=="Address Book" ? "current": ""}} "><a href="{{url('myAccount/addressBook')}}">Address Book</a></li>
                <li class="{{$headerTitle=="My Orders" ? "current": ""}} "><a href="{{url('myAccount/orders')}}">My Orders</a></li>
                <li><a href="#">Billing Agreements</a></li>
                <li><a href="#">Recurring Profiles</a></li>
                <li class="{{$headerTitle=="My Product Reviews" ? "current": ""}} "><a href="{{url('myAccount/reviews')}}">My Product Reviews</a></li>
                <li class="{{$headerTitle=="My WishList" ? "current": ""}} "><a href="{{url('myAccount/wishlist')}}">My Wishlist</a></li>

                <li><a href="#">My Downloadable</a></li>
                <li class="last"><a href="#">Newsletter Subscriptions</a></li>
            </ul>
        </div>
    </div>
    <div class="compare block">
        <div class="sidebar-bar-title">
            <h3>Compare Products (2)</h3>
        </div>
        <div class="block-content">
            <ol id="compare-items">
                <li class="item"> <a href="#" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a> <a href="#" class="product-name">Vestibulum porta tristique porttitor.</a> </li>
                <li class="item"> <a href="#" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a> <a href="#" class="product-name">Lorem ipsum dolor sit amet</a> </li>
            </ol>
            <div class="ajax-checkout">
                <button type="submit" title="Submit" class="button button-compare"> <span><i class="fa fa-signal"></i> Compare</span></button>
                <button type="submit" title="Submit" class="button button-clear"> <span><i class="fa fa-eraser"></i> Clear All</span></button>
            </div>
        </div>
    </div>
</aside>