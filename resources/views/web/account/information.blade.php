@extends('web.account.layout.account')


@isset($accountInformation)
@php
$id=$accountInformation->id;
$firstName=$accountInformation->firstName;
$lastName=$accountInformation->lastName;
$gender=$accountInformation->gender;
$address=$accountInformation->address;
$city=$accountInformation->city;
$region=$accountInformation->region;
$telephone=$accountInformation->telephone;
$fax=$accountInformation->fax;

@endphp
@else

@php
$id="";
$firstName="";
$lastName="";
$gender="";
$address="";
$city="";
$region="";
$telephone="";
$fax="";

@endphp
@endisset

@section('accountContent')
<div class="col-main col-sm-9 col-xs-12">
    <div class="my-account">
        <div class="page-title">
            <h2>My Account/Billing Information</h2>

            <p>Note:&nbsp; <span
                    class="required text-danger">*</span> is required</p>
        </div>
        <form action="{{ route('account.storeAccountInformation',$id) }}" method="post">
            <!--            alertMessage-->
            @include('messages.alertMessages')
            <!--            end alertMessages-->
            {{csrf_field()}}


            <div class="row">
                <div class="col-sm-6 {{ $errors->has('firstName') ? ' has-error' : '' }}">
                    <label for="first_name" class="required">First Name &nbsp; <span
                            class="required text-danger">*</span></label>
                    <input type="text" class="input form-control" name="firstName" id="first_name"
                           value="{{$firstName}}">
                    @if ($errors->has('firstName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->
                <div class="col-sm-6 {{ $errors->has('lastName') ? ' has-error' : '' }}">
                    <label for="last_name" class="required">Last Name&nbsp; <span
                            class="required text-danger">*</span></label>
                    <input type="text" name="lastName" class="input form-control" id="last_name" value="{{$lastName}}">
                    @if ($errors->has('lastName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->
            </div><!--/ .row -->
            <div class="row">
                <div class="col-sm-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email_address" class="required">Email Address&nbsp; <span
                            class="required text-danger">*</span></label>
                    <input type="text" class="input form-control" name="email" id="email_address"
                           value="{{Auth::user()->email}} " readonly>
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->
                <div class="col-sm-6 {{ $errors->has('gender') ? ' has-error' : '' }}">
                    <label class="required">Gender&nbsp; <span
                            class="required text-danger">*</span></label>
                    <select class="input form-control" name="gender">
                        <option selected disabled>Select Gender</option>
                        <option {{$gender=="male" ?
                        "selected" : "" }} value="male">Male</option>
                        <option {{$gender=="female" ?
                        "selected" : "" }} value="female">Female</option>

                    </select>
                    @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->
            </div>
            <div class="row">
                <div class="col-sm-6 {{ $errors->has('address') ? ' has-error' : '' }}">
                    <label for="address" class="required">Address&nbsp; <span
                            class="required text-danger">*</span></label>
                    <input type="text" class="input form-control" name="address" id="address"
                           value="{{$address}}">
                    @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->

            </div>


            <div class="row">
                <div class="col-sm-6 {{ $errors->has('region') ? ' has-error' : '' }}">
                    <label for="region" class="required">State&nbsp; <span
                            class="required text-danger">*</span></label>
                    <select class="input form-control" name="region">
                        <option selected disabled>Select Region</option>
                        <option {{$region=="Bagmati" ?"selected" : "" }} value="Bagmati">Bagmati</option>


                    </select>
                    @if ($errors->has('region'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('region') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->
                <div class="col-sm-6 {{ $errors->has('city') ? ' has-error' : '' }}">
                    <label for="city" class="required">City&nbsp; <span
                            class="required text-danger">*</span></label>
                    <select class="input form-control" name="city">
                        <option selected disabled>Select Region</option>
                        <option {{$city=="Kathmandu" ?"selected" : "" }} value="Kathmandu">Kathmandu</option>
                        <option {{$city=="Lalitpur" ?"selected" : "" }} value="Lalitpur">Lalitpur</option>
                        <option {{$city=="Bhaktapur" ?"selected" : "" }} value="Bhaktapur">Bhaktapur</option>


                    </select>
                    @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->

            </div><!--/ .row -->
            <div class="row">
                <div class="col-sm-6 {{ $errors->has('telephone') ? ' has-error' : '' }}">
                    <label for="telephone" class="required">Telephone&nbsp; <span
                            class="required text-danger">*</span></label>
                    <input type="number" class="input form-control" name="telephone" id="telephone"
                           value="{{$telephone}}">
                    @if ($errors->has('telephone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->
                <div class="col-sm-6 {{ $errors->has('fax') ? ' has-error' : '' }}">
                    <label for="fax" class="required">Fax</label>
                    <input type="text" class="input form-control" name="fax" id="fax"
                           value="{{$fax}}">
                    @if ($errors->has('fax'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                    @endif
                </div><!--/ [col] -->
            </div>
            </br>
            <h4 style="margin-top:50px;" class="text-info">*Only if you want to change the
                password.*</h4>
            <div class="col-md-6">
                <div class=" form-group {{  $errors->has('current_password') ? 'has-error' : ''}}">
                    <label for="current_password">Current Password</label>
                    <input type="password" class="form-control" id="current_password"
                           name="current_password">
                    @if ( $errors->has('current_password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('current_password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">New Password
                        <small></small>
                    </label>
                    <input type="password" class="form-control" id="password"
                           name="password">
                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm">Confirm Password
                        <small></small>
                    </label>
                    <input type="password" class="form-control" id="password_confirmation"
                           name="password_confirmation">
                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>




            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="button pull-right"><i class="fa fa-angle-double-right"></i>&nbsp;
                        <span>Save</span></button>
                </div>

            </div>


        </form>
    </div>
</div>
@endsection