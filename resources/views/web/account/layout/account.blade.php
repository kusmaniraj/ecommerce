@extends('layouts.web')
@section('content')
@include('web.account.include.header')
<section class="main-container col2-right-layout">
    <div class="main container">
        <div class="row">
            @yield('accountContent')


            @include('web.account.include.sidebar')
        </div>
    </div>
</section>
@endsection


<!--end main-->
@push('scripts')
<script src="{{asset('build/pages/web/addressBook.js')}}"></script>
@endpush