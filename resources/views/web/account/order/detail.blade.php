@extends('web.account.layout.account')
@push('styles')
<style>
    #placeOrderTable td{
    /*padding-top:40px ;*/
    }

</style>
@endpush

@section('accountContent')
<div class="col-main col-sm-9 col-xs-12">
    <div class="my-account">
        <div class="page-title">
            <h2>My Orders Information ( {{$orderInfo->created_at->format('Y-m-d')}} ) </h2>

        </div>

        <div class="order-overview-tab">
            <div  class="row">
                <div class="order-tab-inner">
                    <ul id="order-detail-tab" class="nav nav-tabs order-tabs">
                        <li class="active"><a href="#productInfo" class="productInfo" data-toggle="tab"> Product
                                Information </a></li>
                        <li><a class="billingInformationInfo" href="#billingInfo" data-toggle="tab">Billing Information</a></li>
                        <li><a class="shippingInformationInfo" href="#shippingInfo" data-toggle="tab">Shipping Information</a></li>


                    </ul>
                    <div id="orderTabContent" class="tab-content">

                        <div class="tab-pane fade in active" id="productInfo">
                            <div class="std">
                                @isset($orderInfo['product'])
                                @if(count($orderInfo['product'])>0)

                                <div class="table-responsive">


                                    <table class="table table-bordered " id="placeOrderTable">
                                        @include('messages.alertMessages')
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Product</th>
                                            <th>Description</th>
                                            <th>Qty</th>
                                            <th>Price</th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                        $totalQty=0;
                                        $totalPrice=0;
                                        @endphp

                                        <tr>
                                            <td>
                                                {{$orderInfo->created_at->format('Y-m-d')}}
                                            </td>
                                            <td>
                                                @for($i=0;$i<count($orderInfo['product']);$i++)
                                                <img src="{{$orderInfo['productVariation'][$i]['featuredImg']}}"
                                                     height="50px" width="50px" alt=" {{$orderInfo['product'][$i]['name']}}">
                                                <hr>

                                                @endfor
                                            </td>
                                            <td>
                                                @for($i=0;$i<count($orderInfo['product']);$i++)
                                                <p>

                                                    {{$orderInfo['product'][$i]['name']}}</p>
                                                @if($orderInfo['orderSizeName'][$i])
                                                <p>Size:&nbsp;{{$orderInfo['orderSizeName'][$i]}}</p>
                                                @else
                                                </br>
                                                @endif
                                                <hr>
                                                @endfor
                                            </td>
                                            <td>
                                                @for($i=0;$i<count($orderInfo['orderQty']);$i++)
                                                @php
                                                $totalQty +=$orderInfo['orderQty'][$i];
                                                @endphp
                                                <p>

                                                    {{$orderInfo['orderQty'][$i]}}
                                                </p>
                                                <br>

                                                <hr>
                                                @endfor
                                            </td>
                                            <td>
                                                @for($i=0;$i<count($orderInfo['orderPrice']);$i++)
                                                @php
                                                $totalPrice +=$orderInfo['orderPrice'][$i];
                                                @endphp
                                                <p>

                                                    Rs: {{$orderInfo['orderPrice'][$i]}}
                                                </p>
                                                <br>

                                                <hr>
                                                @endfor
                                            </td>






                                        </tr>
                                        <tr>


                                            <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                                            <td><strong>{{$totalQty}}</strong></td>
                                            <td><strong>{{$totalPrice}}</strong></td>


                                        </tr>



                                        </tbody>


                                    </table>


                                </div>
                                @else
                                <h5>You had not order's products</h5>
                                @endif
                                @endisset
                            </div>
                        </div>
                        <div class="tab-pane fade" id="billingInfo">
                            <div class="order-tabs-content-inner clearfix">
                                @isset($orderInfo['billingInformation'])

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <strong>Full Name :</strong>
                                        </div>
                                        <div class="col-md-9">
                                            {{ $orderInfo['billingInformation']['firstName']}} &nbsp; {{
                                            $orderInfo['billingInformation']['lastName']}}
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <strong>Gender :</strong>
                                        </div>
                                        <div class="col-md-9">
                                            {{ $orderInfo['billingInformation']['gender']}}
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <strong>Email :</strong>
                                        </div>
                                        <div class="col-md-9">
                                            {{ $orderInfo['billingInformation']['userEmail']}}
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <strong>Address :</strong>
                                        </div>
                                        <div class="col-md-9">
                                            {{ $orderInfo['billingInformation']['address']}}
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <strong>City :</strong>
                                        </div>
                                        <div class="col-md-9">
                                            {{ $orderInfo['billingInformation']['city']}}
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <strong>Telephone :</strong>
                                        </div>
                                        <div class="col-md-9">
                                            {{ $orderInfo['billingInformation']['telephone']}}
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <strong>Fax :</strong>
                                        </div>
                                        <div class="col-md-9">
                                            {{ $orderInfo['billingInformation']['fax']}}
                                        </div>

                                    </div>

                                </div>
                                @endisset
                            </div>
                        </div>
                        <div id="shippingInfo" class="tab-pane fade">
                            @isset($orderInfo['shippingInformation'])

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <strong>Full Name :</strong>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $orderInfo['shippingInformation']['firstName']}} &nbsp; {{
                                        $orderInfo['shippingInformation']['lastName']}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <strong>Gender :</strong>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $orderInfo['shippingInformation']['gender']}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <strong>Email :</strong>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $orderInfo['shippingInformation']['email']}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <strong>Address :</strong>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $orderInfo['shippingInformation']['address']}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <strong>City :</strong>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $orderInfo['shippingInformation']['city']}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <strong>Telephone :</strong>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $orderInfo['shippingInformation']['telephone']}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <strong>Fax :</strong>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $orderInfo['shippingInformation']['fax']}}
                                    </div>

                                </div>

                            </div>
                            @endisset

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{url('myAccount/orders')}}" class=" button-back pull-right">Back to Order List</a>
                </div>
            </div>
        </div>


    </div>
</div>
<!--shippingInformation modal-->

@endsection
@push('scripts')

@endpush
