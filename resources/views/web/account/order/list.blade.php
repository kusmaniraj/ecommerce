@extends('web.account.layout.account')


@section('accountContent')
<div class="col-main col-sm-9 col-xs-12">
    <!--    success order message-->
    @include('messages.orderMessage')
    <!--    add cart message-->
    @include('messages.alertMessages')
    <div class="my-account">
        <div class="page-title">
            <h2>My Orders </h2>


        </div>
        <div class="box-border" id="placeOrderPage">
            <div class="page-content page-order">
                @isset($orderList)
                @if(count($orderList)>0)

                <div class="table-responsive">


                    <table class="table table-bordered " id="placeOrderTable">

                        <thead>
                        <tr>
                            <th>Dates</th>

                            <th>Product</th>
                            <th>Description</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Action</th>


                        </tr>
                        </thead>
                        <tbody>



                        @foreach($orderList as $order)


                        <tr>
                            <td>
                                {{$order->created_at->format('Y-m-d')}}
                            </td>
                            <td>
                                @for($i=0;$i<count($order['product']);$i++)
                                <img src="{{asset($order['productVariation'][$i]['featuredImg'])}}"
                                     height="50px" width="50px" alt=" {{$order['product'][$i]['name']}}">
                                <hr>

                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($order['product']);$i++)
                                <p>

                                    {{$order['product'][$i]['name']}}</p>
                                @if($order['orderSizeName'][$i])
                                <p>Size:&nbsp;{{$order['orderSizeName'][$i]}}</p>
                                @else
                                </br>
                                @endif
                                <hr>
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($order['orderQty']);$i++)
                                <p>

                                    {{$order['orderQty'][$i]}}
                                </p>
                                <br>
                                <hr>
                                @endfor
                            </td>
                            <td>
                                @for($i=0;$i<count($order['orderPrice']);$i++)
                                <p>

                                    Rs: {{$order['orderPrice'][$i]}}
                                </p>
                                <br>

                                <hr>
                                @endfor
                            </td>
                            <td>
                                <form  id="viewOrderInfoForm" action="{{url('myAccount/viewOrder')}}" method="post">
                                    <input type="hidden" name="date" value=" {{$order->created_at}}">
                                    <input type="hidden" name="email" value=" {{$order->email}}">
                                    {{csrf_field()}}


                                    <a href="#" class="btn btn-primary"  onclick="document.getElementById('viewOrderInfoForm').submit()">View Information</i></a>


                                </form>
                                <a href="#" class="btn btn-danger"><i class="fa fa-remove "></i></a>
                            </td>





                        </tr>

                        @endforeach




                        </tbody>


                    </table>


                </div>
                @else
                <h5>You had not order's products</h5>
                @endif
                @endisset
            </div>
        </div>


    </div>
</div>

@endsection
@push('scripts')
<script>
    $('#placeOrderTable').DataTable({
        processing: true,
    });
</script>
@endpush

