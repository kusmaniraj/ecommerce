@extends('web.account.layout.account')
@section('accountContent')
<div class="col-main col-sm-9 col-xs-12">
    <div class="my-account">
        <div class="page-title">
            <h2>Change Product Reviews</h2>
            <p>Note:&nbsp; <span
                    class="required text-danger">*</span> is required</p>
        </div>

            <form method="post" action="{{url('myAccount/updateReview/'.$review->id)}}">
                {{csrf_field()}}


                <input type="hidden" name="productId" value="{{$review->productId}}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <img src="{{$review->product->featuredImg}}" alt="{{$review->product->name}}">
                        </div>
                        <div class="col-md-8">
                            <h5>{{$review->product->name}}</h5>
                           <p>{{$review->product->description}}</p>
                        </div>

                    </div>

                </div>
                </br>

                <div class="row form-group  {{ $errors->has('title') ? ' has-error' : '' }}">
                    <div class="col-sm-12">
                        <label for="review_title" class="required">Title  <span
                                class="required text-danger">*</span></label>
                        <input type="text" class="input form-control" name="title" id="review_title"
                               value="{{$review->title}}">
                        @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div><!--/ [col] -->

                </div><!--/ .row -->
                <div class="row form-group {{ $errors->has('review') ? ' has-error' : '' }}">
                    <div class="col-sm-12">
                        <label for="last_name" class="required">Review Message  <span
                                class="required text-danger">*</span></label>
                        <textarea class="form-control" rows="3" name="review">{{$review->review}}</textarea>
                        @if ($errors->has('review'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('review') }}</strong>
                                    </span>
                        @endif
                    </div><!--/ [col] -->
                </div>
                <div class="row form-group {{ $errors->has('ratedStar') ? ' has-error' : '' }}" >
                    <div class="col-sm-12">
                        <label class="required">Rating Star   <span
                                class="required text-danger">*</span></label>
                        <select class="input form-control" name="ratedStar">
                            <option selected disabled>Select Rating Star</option>
                            <option {{($review->ratedStar==1) ? 'selected':''}} value="1">1 Star</option>
                            <option {{($review->ratedStar==2) ? 'selected':''}} value="2">2 Stars</option>
                            <option {{($review->ratedStar==3) ? 'selected':''}}value="3">3 Stars</option>
                            <option {{($review->ratedStar==4) ? 'selected':''}} value="4">4 Stars</option>
                            <option {{($review->ratedStar==5) ? 'selected':''}} value="5">5 Stars</option>

                        </select>
                        @if ($errors->has('ratedStar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ratedStar') }}</strong>
                                    </span>
                        @endif
                    </div><!--/ [col] -->

                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <a href="{{url('myAccount/reviews')}}" class="btn btn-default pull-left"><i class="fa fa-arrow-circle-left"></i>&nbsp;Back</a>
                        <button type="submit" class="btn btn-primary pull-right">Change Review</button>
                    </div>


                </div>

            </form>


    </div>

</div>


@endSection
@push('scripts')

@endpush
