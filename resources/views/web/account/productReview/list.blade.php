@extends('web.account.layout.account')
@section('accountContent')
<div class="col-main col-sm-9 col-xs-12">
    <div class="my-account">
        <div class="page-title">
            <h2>My Product Reviews</h2>
        </div>
        <div class="wishlist-item table-responsive">
            @if(count($reviews) > 0)
            @include('messages.alertMessages')
            <table class="col-md-12" id="reviewTable">
                <thead>
                <tr>
                    <th>SN</th>
                    <th class="th-product">Images</th>
                    <th class="th-details">Product Name</th>
                    <th>Rated Star</th>
                    <th>Title</th>
                    <th>Review Message</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>


                @foreach($reviews as $key=>$reviewData)
                <tr>
                    <td>{{$key+1}}</td>
                    <td><a href="{{url('detailProduct/'.$reviewData->product->id)}}"><img src="{{$reviewData->product->featuredImg}}" alt="{{$reviewData->product->name}}" height="50px" width="50px"></a></td>
                    <td><a href="{{url('detailProduct/'.$reviewData->product->id)}}">{{$reviewData->product->name}}</a></td>
                    <th>{{$reviewData->ratedStar}}</th>
                    <td>{{$reviewData->title}}</td>
                    <td>{{$reviewData->review}}</td>
                    <td>
                        <a href="{{url('myAccount/removeReview/'.$reviewData->id)}}" class="remove "><i class="fa fa-trash text-danger">&nbsp;Remove </i></a>
                        </br>
                        <a href="{{url('myAccount/editReview/'.$reviewData->id)}}"><i class="fa fa-edit text-info">&nbsp;Change </i></a>
                    </td>
                </tr>

                @endforeach


                </tbody>
            </table>
            <div class="pagination-area ">

                {{$reviews->links()}}

            </div>
            @else
            <h5>
                You didn't order any item  yet.
            </h5>


            @endif

        </div>
    </div>
</div>



@endSection
@push('scripts')
<script>
    $('#reviewTable').DataTable({
        processing: true,
    });
</script>
<script src="{{asset('build/pages/web/shoppingCart.js')}}"></script>
@endpush
