@extends('web.account.layout.account')
@section('accountContent')
<!--success add Cart message-->
@include('messages.addCartMessage')
<!--end of add cart message-->
<div class="col-main col-sm-9 col-xs-12">
    <div class="my-account">
        <div class="page-title">
            <h2>My Wishlist</h2>
        </div>
        <div class="wishlist-item table-responsive">
            @if(count($wishLists) > 0)
            @include('messages.alertMessages')
            <table class="col-md-12" id="wishlistTable">
                <thead>
                <tr>
                    <th>SN</th>
                    <th class="th-product">Images</th>
                    <th class="th-details">Product Name</th>
                    <th class="th-qty">Quantity</th>

                    <th class="th-price">Unit Price</th>
                    <th class="th-total th-add-to-cart">Add to Cart</th>
                    <th class="th-delete">Remove</th>
                </tr>
                </thead>
                <tbody>


                @foreach($wishLists as $key=>$wishList)



                    <tr >
                        <input type="hidden" name="product_id" value="{{$wishList->productId}}">
                        <input type="hidden" name="product_variation_id" value="{{$wishList->productVariationId}}">
                        <input type="hidden" name="maxQty" value="{{$wishList->maxQty}}">
                        <input type="hidden" name="size_id" value="{{$wishList->size}}">
                        <td>{{$key+1}}</td>
                        <td class="th-product"><a href="#"><img src="{{url($wishList->productVariation->featuredImg)}}"
                                                                alt="cart"></a></td>
                        <td class="th-details">
                            <h2>
                                <a href="{{url('detailProduct/'.$wishList->product->id.'/'.$wishList->productVariation->id)}}">{{$wishList->product->name}}</a>
                            </h2>
                            @isset($wishList['productSize'])
                            <p>Size: &nbsp;{{$wishList->productSize->name}}</p>
                            @endisset


                        </td>
                        <td class="form-group">
                            <select name="qty" class="form-control">
                                @for($i=1;$i<=$wishList->maxQty;$i++)
                                <option {{($wishList->qty==$i) ? 'selected':''}} > {{$i}}</option>
                                @endfor

                            </select>

                        </td>

                        <td class="th-price">Rs:&nbsp;{{($wishList->product->newPrice) ? $wishList->product->newPrice :
                            $wishList->product->oldPrice}}
                        </td>
                        <td class="td-add-to-cart">
                            <button  class="button add-to-cart-btn"> Add to Cart</button>

                        </td>

                        <td class="th-delete"><a href="{{url('myAccount/removeFromWishList/'.$wishList->id)}} "
                                                 class="remove"><i class="fa fa-remove"></i></a></td>
                    </tr>

                @endforeach


                </tbody>
            </table>
            <div class="pagination-area ">

                {{$wishLists->links()}}

            </div>
            @else
            <h5>
                You didn't save any item for later yet.
            </h5>


            @endif

        </div>
    </div>
</div>


@endSection
@push('scripts')
<script src="{{asset('build/pages/web/account/wishlist.js')}}"></script>
<script>
    $('#wishlistTable').DataTable({
        processing: true,
    });
</script>
@endpush

