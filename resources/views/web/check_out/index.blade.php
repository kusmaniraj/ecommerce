@extends('layouts.web')
@push('styles')
<style>
    .shipping-info-lists{
        margin-bottom: 7px;
    }
    .shippingInfoList {
        border: 1px solid #ccc;
        font-size: 12px;
        font-weight: 200;
        margin-right: 5px;
    }

    .shippingInfoList h5 {
        border-bottom: 2px solid #e5e5e5;
        padding-bottom: 24px;
    }

    .shippingInfoList ul li {
        border-bottom: 1px solid #e5e5e5;
        padding: 8px 0;
        line-height: 8px;
    }

    .shippingInfoList h5 span {
        margin-left: 10px;
        font-size: 20px;

    }
    button.active {
        background: #333e48;
        border: 2px solid #333e48;
        transition: all 0.3s linear;
        -moz-transition: all 0.3s linear;
        -webkit-transition: all 0.3s linear;
        color: #fff;
    }


</style>

@endpush

@isset($user)
@php

$authEmail=$user->email;
@endphp
@else
@php

$authEmail="";
@endphp
@endisset
@isset($userDetails)
@php
$id=$userDetails->id;
$firstName=$userDetails->firstName;
$lastName=$userDetails->lastName;
$gender=$userDetails->gender;
$email=$userDetails->email;
$address=$userDetails->address;
$city=$userDetails->city;
$region=$userDetails->region;
$telephone=$userDetails->telephone;
$fax=$userDetails->fax;
@endphp
@else

@php
$id="";
$firstName="";
$lastName="";
$gender="";
$email="";
$address="";
$city="";
$region="";
$telephone="";
$fax="";
@endphp
@endisset

@section('content')

<!-- Breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li class="home"><a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>

                    <li><strong>CheckOut</strong></li>


                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<section class="main-container col2-right-layout">

    <div class="main container">
        <div class="row">
            <div class="col-main col-sm-9 col-xs-12">


                <div class="page-content checkout-page">
                    <div class="page-title">
                        <h2>Checkout</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <h4 class="checkout-sep step1_header">1. Checkout Method @isset($user) <span><img
                                        height="20px" width="20px" src="{{asset('images/checked.png')}}" alt=""></span>
                                @endisset </h4>
                        </div>
                        <div class="col-md-8">
                            <p id="selectedEmail">{{$authEmail}}</p>

                        </div>
                    </div>


                    <!--                    checkout as guest or registered user-->

                    <div class="box-border" @isset($user) style="display:none;" @endisset id="step1">

                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Checkout as a Guest or Register</h5>


                                <ul>
                                    <li><label><input type="radio" value="guest" name="userType" checked>Checkout as
                                            Guest</label></li>
                                    <li><label><input type="radio" value="register" name="userType">Register</label>
                                    </li>
                                </ul>
                                <br>
                                <h4>Register and save time!</h4>

                                <p>Register with us for future convenience:</p>

                                <p><i class="fa fa-check-circle text-primary"></i> Fast and easy check out</p>

                                <p><i class="fa fa-check-circle text-primary"></i> Easy access to your order history and
                                    status</p>


                                <button class="button btnStep1"><i class="fa fa-angle-double-right"></i>&nbsp; <span>Continue</span>
                                </button>
                            </div>

                            <!--                            register login page-->
                            <div class="col-sm-6 login-page" style="display: none">
                                <h4>Login</h4>

                                <p>Already registered? Please log in below:</p>

                                <form id="ajaxLoginForm">
                                    <label>Email address</label>
                                    <input type="email" class="form-control input" name="email" required>
                                    <label>Password</label>
                                    <input type="password" class="form-control input" name="password" required>

                                    <p><a href="#">Forgot your password?</a> | <a href="#RegisterForm" id="register">Register</a>
                                    </p>
                                    <button type="submit" class="button"><i class="icon-login"></i>&nbsp;
                                        <span>Login</span></button>

                                </form>


                                <a href="{{ url('user/facebook') }}" class="btn btn-flat btn-facebook"><i
                                        class="fa fa-facebook"></i> Facebook Login</a>

                            </div>
                            <div class="col-sm-6 register-page" style="display: none">
                                <h4>Register</h4>

                                <p>Create your very own account</p>


                                <form id="registerForm">

                                    <label>Name</label>
                                    <input type="text" class="form-control input" name="name" required>
                                    <label>Email address</label>
                                    <input type="email" class="form-control input" name="email" required>
                                    <label>Password</label>
                                    <input type="password" class="form-control input" name="password" required>
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control input" name="password_confirmation"
                                           required>
                                    <button type="submit" class="button"><i class="fa fa-user"></i>&nbsp;
                                        <span>Register</span></button>
                                    <button href="#loginForm" class="button-back" id="backToLogin"><i
                                            class="fa fa-arrow-circle-left"></i>Back to Login
                                    </button>
                                </form>


                            </div>
                            <div class="col-sm-6 email-page">
                                <label>Email address</label>
                                <input type="email" name="email" value="" class="form-control input">

                            </div>


                        </div>
                    </div>
                    <!--                    billing information-->
                    <h4 class="checkout-sep step2_header">2. Billing Information</h4>

                    <div class="box-border" id="step2" @isset($user) @else style="display:none;" @endisset>
                        <form id="billingInfoForm">

                            <input type="hidden" name="id" value="{{$id}}">
                            <ul>
                                <li class="row">
                                    <div class="col-sm-6">
                                        <label for="first_name">First Name <span class="text-danger">*</span></label>
                                        <input type="text" class="input form-control" name="firstName" id="first_name"
                                               value="{{$firstName}}">
                                    </div><!--/ [col] -->
                                    <div class="col-sm-6">
                                        <label for="last_name" >Last Name <span class="text-danger">*</span></label>
                                        <input type="text" name="lastName" class="input form-control" id="last_name"
                                               value="{{$lastName}}">
                                    </div><!--/ [col] -->
                                </li><!--/ .row -->
                                <li class="row">
                                    <div class="col-sm-6">
                                        <label for="email_address" >Email Address <span class="text-danger">*</span></label>
                                        <input type="text" class="input form-control" name="userEmail"
                                               id="email_address"
                                               value="@isset($user){{$user->email}}@endisset" readonly>
                                    </div><!--/ [col] -->
                                    <div class="col-sm-6">
                                        <label >Gender <span class="text-danger">*</span></label>
                                        <select class="input form-control" name="gender">
                                            <option selected disabled>Select Gender</option>
                                            <option {{$gender=="male" ?
                                            "selected" : "" }} value="male">Male</option>
                                            <option {{$gender=="female" ?
                                            "selected" : "" }} value="female">Female</option>

                                        </select>
                                    </div><!--/ [col] -->
                                </li>

                                <li class="row">
                                    <div class="col-xs-12">

                                        <label for="address" >Address <span class="text-danger">*</span></label>
                                        <input type="text" class="input form-control" name="address" id="address"
                                               value="{{$address}}">

                                    </div><!--/ [col] -->

                                </li><!-- / .row -->

                                <li class="row">
                                    <div class="col-sm-6">
                                        <label >State/Province  <span class="text-danger">*</span></label>
                                        <select class="input form-control" name="region">
                                            <option selected disabled>Select Region</option>
                                            <option {{$region=="Bagmati" ?
                                            "selected" : "" }} value="Bagmati">Bagmati</option>

                                        </select>
                                    </div><!--/ [col] -->

                                    <div class="col-sm-6">

                                        <label for="city" >City <span class="text-danger">*</span></label>
                                        <select class="input form-control" name="city">
                                            <option selected disabled>Select Region</option>
                                            <option {{$city=="Kathmandu" ?
                                            "selected" : "" }} value="Kathmandu">Kathmandu</option>
                                            <option {{$city=="Lalitpur" ?
                                            "selected" : "" }} value="Lalitpur">Lalitpur</option>
                                            <option {{$city=="Bhaktapur" ?
                                            "selected" : "" }} value="Bhaktapur">Bhaktapur</option>
                                        </select>

                                    </div><!--/ [col] -->


                                </li><!--/ .row -->


                                <li class="row">
                                    <div class="col-sm-6">
                                        <label for="telephone" >Telephone <span class="text-danger">*</span></label>
                                        <input class="input form-control" type="number" name="telephone" id="telephone"
                                               value="{{$telephone}}">
                                    </div><!--/ [col] -->

                                    <div class="col-sm-6">
                                        <label for="fax">Fax</label>
                                        <input class="input form-control" type="text" name="fax" id="fax"
                                               value="{{$fax}}">
                                    </div><!--/ [col] -->

                                </li><!--/ .row -->
                                @isset($user)
                                @else
                                <li><label for="logIn"><input type="checkbox" value="no" name="logIn"
                                                              id="logIn"></label>&nbsp;Keep Me Log in
                                </li>
                                <li class="row passwordPage" style="display: none">
                                    <div class="col-sm-6">
                                        <label for="password" >Password <span class="text-danger">*</span></label>
                                        <input class="input form-control" type="password" name="password" id="password">
                                    </div><!--/ [col] -->

                                    <div class="col-sm-6">
                                        <label for="confirm" >Confirm Password <span class="text-danger">*</span></label>
                                        <input class="input form-control" type="password" name="password_confirmation"
                                               id="confirm">
                                    </div><!--/ [col] -->
                                </li><!--/ .row -->


                                <li>
                                    @endisset


                                    <button type="submit" class="button"><i class="fa fa-angle-double-right"></i>&nbsp;
                                        <span>Continue</span></button>

                                </li>
                            </ul>
                        </form>

                    </div>
                    <h4 class="checkout-sep step3_header">3. Shipping Information</h4>

                    <div class="box-border" id="step3" style="display: none">

                        <div class="col-md-12 shipping-info-lists">

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <a href="#" id="addShippingInfoBtn" class="btn btn-flat btn-primary">Add Shipping Information</a>

                            </div>

                        </div>



                        <button type="button" class="sameInfoAsBillingBtn step3Btn button"><i
                                class="fa fa-angle-double-right"></i>&nbsp;
                            <span>Continue</span></button>
                        <button href="#step2" class="sameInfoAsBillingBtn button-back backToStep2"><span><i
                                    class="fa fa-arrow-circle-left"></i>&nbsp; Back</span></button>

                    </div>


                    <h4 class="checkout-sep step4_header">4. Payment Information</h4>

                    <div class="box-border " id="step4" style="display:none;">
                        <ul>
                            <li>
                                <label for="deliveryPayment"><input type="radio" checked name="paymentType"
                                                                    value="delivery" id="deliveryPayment"> Payment on
                                    Delivery
                                    order</label>
                            </li>


                        </ul>
                        <button class="button step4Btn"><i class="fa fa-angle-double-right"></i>&nbsp;
                            <span>Continue</span></button>
                        <button href="#step3" class="button-back backToStep3"><span><i
                                    class="fa fa-arrow-circle-left"></i>&nbsp; Back</span></button>
                    </div>
                    <h4 class="checkout-sep last">5. Order Review</h4>

                    <div class="box-border" style="display:none;" id="step5">
                        <div class="page-content page-order">
                            <div class="table-responsive">
                                @isset($sessionCart)
                                <?php

                                $total = 0;
                                ?>
                                <form id="placeOrderForm">


                                    <table class="table table-bordered cart_summary">
                                        <thead>
                                        <tr>
                                            <th class="cart_product">Product</th>
                                            <th>Avail.</th>
                                            <th>Description</th>

                                            <th>Unit price</th>
                                            <th>Qty</th>
                                            <th>Sub Total</th>
                                            <th class="action"><i class="fa fa-trash-o"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody>


                                        @foreach($sessionCart as $key=>$cart)
                                        <input type="hidden" name="productId[]" value="{{$cart['product_id']}}">
                                        <input type="hidden" name="productVariationId[]"
                                               value="{{$cart['product_variation_id']}}">
                                        <input type="hidden" name="size[]"
                                               value="@isset($cart['selectedSizeName']){{$cart['selectedSizeName']}}@endisset">

                                        <tr>
                                            <td class="cart_product"><a href="#"><img
                                                        src="{{$cart['variationWithProduct']['featuredImg']}}"
                                                        alt="Product"></a></td>
                                            <td><span class="label label-success">In stock</span></td>
                                            <td class="cart_description"><p class="product-name"><a
                                                        href="{{url('detailProduct/'.$cart['product_id'])}}">{{$cart['variationWithProduct']['product']['name']}}
                                                    </a></p>

                                                @isset($cart['selectedSizeName'])

                                                <small><a href="#">Size : {{$cart['selectedSizeName']}}</a></small>
                                                @endisset
                                            </td>

                                            @isset($cart['variationWithProduct']['product']['newPrice'])
                                            <input type="hidden" name="price[]"
                                                   value="{{$cart['variationWithProduct']['product']['newPrice']}}">
                                            <td class="price">Rs:

                                                <span>{{$cart['variationWithProduct']['product']['newPrice']}}</span>
                                            </td>
                                            @else
                                            <input type="text" name="price[]"
                                                   value="{{$cart['variationWithProduct']['product']['oldPrice']}}">
                                            <td class="price">Rs:
                                                <span>{{$cart['variationWithProduct']['product']['oldPrice']}}</span>
                                            </td>
                                            @endisset

                                            <td class="qty form-group">


                                                <select class="form-control" name="qty[]" id="cart-qty">
                                                    <option>{{ $cart['selectedQty']}}</option>
                                                </select>
                                            </td>
                                            <td class="">Rs: <span>{{$cart['sub_total']}}</span></td>
                                            <td class="action"><a class="removeCart" href="#step4"><i
                                                        class=" icon-close"></i></a></td>
                                        </tr>
                                        @php
                                        $total +=$cart['sub_total'];
                                        @endphp
                                        @endforeach


                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="2" rowspan="2"></td>
                                            <td colspan="3">Total products (tax incl.)</td>
                                            <td colspan="2" class="grandTotalPrice">Rs: <span>{{$total}}</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><strong>Total</strong></td>
                                            <td colspan="2" class="totalPrice"><strong> {{$total}} </strong></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    @endisset
                                    <button type="submit" class="button pull-right stepLastBtn"><span>Place Order</span>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <aside class="right sidebar col-sm-3 col-xs-12">
                <div class="sidebar-checkout block">
                    <div class="sidebar-bar-title">
                        <h3>Your Checkout</h3>
                    </div>
                    <div class="block-content">
                        <dl>
                            <dt class="complete">   <h2 class="header-bar-title">Billing Address <span class="separator">|</span> <a
                                    href="#" id="billingInfoChangeBtn">Change</a></h2></dt>

                            <dd class="complete " id="billing-info">

                            </dd>
                            <dt class="complete"><h2 class="header-bar-title"> Shipping Address <span class="separator">|</span> <a
                                    href="#" id="shippingInfoChangeBtn">Change</a></h2></dt>
                            <dd class="complete " id="shipping-info">

                            </dd>


                            <dt> <h2 class="header-bar-title">Payment Method</h2></dt>
                            <dd class="complete" id="payment_method"></dd>
                        </dl>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</section>
<!-- Main Container End -->
<!--shipping info form modal-->
@include('web.check_out.shippingInfoModal')


@endsection
@push('scripts')
<script src="{{asset('build/pages/web/checkout.js')}}"></script>
<script src="{{asset('build/pages/web/auth.js')}}"></script>


@endpush


