<div class="modal fade" id="shippingInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="shippingInfoTitle">Add Shipping Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="shippingInfoForm">

                    <input type="hidden" name="id" value="">


                    <div class="row">
                        <div class="col-sm-6">
                            <label for="first_name" >First Name <span class="text-danger">*</span></label>
                            <input type="text" class="input form-control" name="firstName" id="shipping_first_name"
                                   value="">
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <label for="last_name" >Last Name <span class="text-danger">*</span></label>
                            <input type="text" name="lastName" class="input form-control" id="shipping_last_name"
                                   value="">
                        </div><!--/ [col] -->
                    </div><!--/ .row -->
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="email_address" >Email Address <span class="text-danger">*</span></label>
                            <input type="text" class="input form-control" name="email" id="shipping_email_address"
                                   value="">
                        </div><!--/ [col] -->
                        <div class="col-sm-6">
                            <label >Gender</label>
                            <select class="input form-control" name="gender">
                                <option selected disabled>Select Gender  <span class="text-danger">*</span></option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>

                            </select>
                        </div><!--/ [col] -->
                    </div>

                    <div class="row">
                        <div class="col-xs-12">

                            <label for="address" >Address  <span class="text-danger">*</span></label>
                            <input type="text" class="input form-control" name="address" id="shipping_address" value="">

                        </div><!--/ [col] -->

                    </div><!-- / .row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <label >State/Province  <span class="text-danger">*</span></label>
                            <select class="input form-control" name="region">
                                <option selected disabled>Select Region</option>
                                <option value="Bagmati">Bagmati</option>

                            </select>
                        </div><!--/ [col] -->

                        <div class="col-sm-6">

                            <label for="city" >City  <span class="text-danger">*</span></label>
                            <select class="input form-control" name="city">
                                <option selected disabled>Select Region</option>
                                <option value="Kathmandu">Kathmandu</option>
                                <option value="Ladivtpur">Ladivtpur</option>
                                <option value="Bhaktapur">Bhaktapur</option>
                            </select>

                        </div><!--/ [col] -->


                    </div><!--/ .row -->


                    <div class="row">
                        <div class="col-sm-6">
                            <label for="telephone" >Telephone  <span class="text-danger">*</span></label>
                            <input class="input form-control" type="number" name="telephone" id="shipping_telephone"
                                   value="">
                        </div><!--/ [col] -->

                        <div class="col-sm-6">
                            <label for="fax">Fax</label>
                            <input class="input form-control" type="text" name="fax" id="shipping_fax" value="">
                        </div><!--/ [col] -->

                    </div><!--/ .row -->


                    <div class="modal-footer">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary pull-right">Add </button>
                        </div>



                    </div>

                </form>
            </div>

        </div>
    </div>
</div>