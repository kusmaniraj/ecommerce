<head>
    <!-- Basic page needs -->
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Ecommerce | @isset($title){{$title}}@endisset</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon  -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favicon.ico')}}">




    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700italic,700,400italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Arimo:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Dosis:400,300,200,500,600,700,800' rel='stylesheet' type='text/css'>

    <!-- CSS Style -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/bootstrap.min.css')}}">

    <link href="{{asset('build/shared/loading.css')}}" rel="stylesheet">

    <!-- font-awesome & simple line icons CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/font-awesome.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/simple-line-icons.css')}}" media="all">

    <!-- owl.carousel CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/owl.transitions.css')}}">


    <!-- animate CSS  -->
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/animate.css')}}" media="all">




    <!-- jquery-ui.min CSS  -->
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/jquery-ui.css')}}">



    <link rel="stylesheet" type="text/css" href="{{asset('web/css/style.css')}}" media="all">
    <link href="{{asset('web/assets/datatables/datatables.css')}}" rel="stylesheet">
    @stack('styles')
    <!-- style CSS -->


</head>