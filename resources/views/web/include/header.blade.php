<header>
    <div class="header-container">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 hidden-xs">
                        <!-- Default Welcome Message -->
                        <div class="welcome-msg ">{{$websiteName}}</div>
                        <span class="phone hidden-sm">Call Us:{{$ownerContact}}</span></div>

                    <!-- top links -->
                    <div class="headerlinkmenu col-lg-8 col-md-7 col-sm-8 col-xs-12">
                        <div class="links">


                            <div class="login">
                                @if(Auth::guard('web')->check())
                                <div class="myaccount"><a title="My Account" href="{{url('myAccount')}}"><i
                                            class="fa fa-user"></i><span class="hidden-xs">My Account</span></a></div>
                                <div class="wishlist"><a title="My Wishlist" href="{{url('myAccount/wishlist')}}"><i
                                            class="fa fa-heart"></i><span class="hidden-xs">Wishlist</span></a></div>
                                <div class="myorders"><a title="My Orders" href="{{url('myAccount/orders')}}"><i class="fa fa-shopping-cart"></i><span
                                            class="hidden-xs">My Order</span></a></div>
                                <a href="{{route('logout')}}"><i class="fa fa-lock"></i><span
                                        class="hidden-xs">Log Out</span></a>
                                @else
                                <a href="{{route('login')}}"><i class="fa fa-unlock-alt"></i><span
                                        class="hidden-xs">Log In</span></a>
                                <a href="{{route('register')}}"><i class="fa fa-user"></i><span
                                        class="hidden-xs">Register</span></a>
                                @endif

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-xs-12">
                    <!-- Header Logo -->
                    <div class="logo"><a title="e-commerce" href="index.html"><img alt="responsive theme logo"
                                                                                   src="{{asset('web/images/logo.png')}}"></a>
                    </div>
                    <!-- End Header Logo -->
                </div>
                <div class="col-xs-9 col-sm-6 col-md-6">
                    <!-- Search -->

                    <div class="top-search">
                        <div id="search">


                            <div class="input-group">
                                <select class="cate-dropdown hidden-xs" name="categoryId">
                                    <option value="0" data-type="all_category" selected>All Categories</option>
                                    @isset($listCategories)
                                    @foreach($listCategories as $category)
                                    <option value="{{$category->id}}" data-type="category"><strong>{{$category->name}}</strong></option>

                                    @foreach($category['parentCategories'] as $parentCategory)
                                    <option value="{{$parentCategory->id}}" data-type="parent_category">&nbsp;&nbsp;&nbsp;&nbsp;{{$parentCategory->name}}</option>
                                    @foreach($parentCategory['subCategories'] as $subCategory)
                                    <option value="{{$subCategory->id}}" data-type="sub_category">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$subCategory->name}}</option>
                                    @endforeach
                                    @endforeach

                                    @endforeach

                                    @endisset
                                </select>
                                <input type="text" class="form-control" placeholder="Search" name="searchWord" ">
                                <button class="btn-search" type="button"><i class="fa fa-search"></i></button>
                            </div>

                        </div>
                    </div>

                    <!-- End Search -->
                </div>
                <!-- top cart -->

                <div class="col-lg-3 col-xs-3 top-cart">
                    <div class="top-cart-contain">
                        <div class="mini-cart">
                            <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"><a
                                    href="#">
                                    <div class="cart-icon"><i class="fa fa-shopping-cart"></i></div>

                                    <div class="shoppingcart-inner hidden-xs">
                                        <span class="cart-title">Shopping Cart</span> <span class="cart-total">
                                             @if(Session::has('cart'))
                                            <?php
                                            $sessionCart = Session::get('cart');

                                            ?>
                                            @isset($sessionCart)
                                            {{count($sessionCart)}} Item(s)
                                            @endisset
                                            @else
                                            0 Item(s)
                                             @endif
                                        </span>
                                    </div>

                                </a></div>
                            <div>
                                @if(Session::has('cart'))
                                <?php
                                $total = 0;
                                $sessionCart = Session::get('cart');

                                ?>

                                <div class="top-cart-content">
                                    <div class="block-subtitle hidden-xs">Recently added item(s)</div>
                                    <ul id="cart-sidebar" class="mini-products-list">


                                        @isset($sessionCart)
                                        @foreach($sessionCart as $key=>$cart)
                                        <li class="item odd"><a href="{{url('/detailProduct/'.$cart['product_id'])}}"
                                                                title="{{$cart['variationWithProduct']['product']['name']}}"
                                                                class="product-image"><img
                                                    src="{{asset($cart['variationWithProduct']['featuredImg'])}}"
                                                    alt="html template"
                                                    width="65"></a>

                                            <div class="product-details"><a href="{{url('cart/removeFromCart/'.$key)}}"
                                                                            title="Remove This Item"
                                                                            class="remove-cart"><i
                                                        class="icon-close"></i></a>

                                                <p class="product-name"><a
                                                        href="{{url('/detailProduct/'.$cart['product_id'])}}"> <strong>{{$cart['variationWithProduct']['product']['name']}}
                                                            <strong></a>
                                                </p>
                                                @isset($cart['selectedSizeName'])
                                                <p>
                                                    <small><a href="#">Size : {{$cart['selectedSizeName']}}</a></small>
                                                </p>
                                                @endisset


                                                <strong>{{$cart['selectedQty']}}</strong> x <span class="price">Rs:  @isset($cart['variationWithProduct']['product']['newPrice']){{$cart['variationWithProduct']['product']['newPrice']}}@else{{$cart['variationWithProduct']['product']['oldPrice']}}@endisset</span>

                                            </div>
                                        </li>
                                        @php
                                        $total +=$cart['sub_total'];
                                        @endphp

                                        @endforeach

                                    </ul>
                                    @endisset
                                    <div class="top-subtotal">Total: <span class="price" data-total="{{$total}}">Rs:{{$total}}</span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn-checkout" href="{{url('checkout')}}"><i
                                                class="fa fa-check"></i><span>Checkout</span>
                                        </a>
                                        <a href="{{url('/cart')}}" class="view-cart" type="button"><i
                                                class="fa fa-shopping-cart"></i>
                                            <span>View Cart</span></a>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@push('scripts')
<script>
    $(document).ready(function () {
        var url = window.location.pathname;
        if (url.indexOf("search") > 0) {
            var param = url.split('/');
            $('input[name="searchWord"]').val(param[3]);
            if (param[4] != null && param[5] != null) {

                $('select[name="categoryId"] option[data-type="' + param[4] + '"][value="' + param[5] + '"]').attr('selected', true);
            } else {
                $('select[name="categoryId"] option[data-type="all_category"][value="' + param[5] + '"]').attr('selected', true);
            }

        }
        $('.btn-search').on('click', function (e) {
            e.preventDefault();
            search();


        })
        $('input[name="searchWord"]').keyup(function (e) {
            e.preventDefault();
            if (e.keyCode === 13) {

                search();
            }
        })
        function search() {
            var listType = "grid";
            if (url.indexOf("grid") > 0) {
                var param = url.split('/');
                listType = 'grid';

            } else {
                listType = 'list';
            }
            var categoryId = $('select[name="categoryId"]').val();
            var type = $('select[name="categoryId"] option:selected').data('type');

            var searchWord = $('input[name="searchWord"]').val();

            if (searchWord == "") {
                return false
            } else {
                if (categoryId == 0 && type == 'all_category') {
                    window.location.href = base_url + '/search/' + listType + '/' + searchWord;
                } else {
                    window.location.href = base_url + '/search/' + listType + '/' + searchWord + '/' + type + '/' + categoryId;
                }

            }
        }
    })


</script>
@endpush