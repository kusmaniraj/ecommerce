<nav>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <div class="mm-toggle-wrap">
                    <div class="mm-toggle"><i class="fa fa-align-justify"></i></div>
                    <span class="mm-label">Categories</span></div>
                <div class="mega-container visible-lg visible-md visible-sm">
                    <div class="navleft-container">
                        <div class="mega-menu-title">
                            <h3>Categories</h3>
                        </div>
                        <div class="mega-menu-category " style="display:none">
                            <ul class="nav">
                                @isset($listCategories)
                                @foreach($listCategories as $category)

                                <li><a href="{{url('/category/grid/'.$category->id)}}"><i class="icon fa fa-{{ $category->name}} fa-fw"></i>{{
                                        $category->name}}</a>

                                    <div class="wrap-popup">
                                        <div class="popup">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    @isset($category['parentCategories'])
                                                    @foreach($category['parentCategories'] as $parentCategory)


                                                    <div class="col-md-4 col-sm-6">
                                                        <a href="{{url('/category/grid/'.$parentCategory->id)}}"><h3>{{ $parentCategory->name}}</h3></a>
                                                        @isset($parentCategory['subCategories'])
                                                        @foreach($parentCategory['subCategories']->chunk(4) as $chunkArray)
                                                        <ul class="nav">

                                                            @foreach($chunkArray as $subCategory)
                                                            <li>
                                                                <a href="{{url('/category/grid/'.$subCategory->id)}}"><span>{{$subCategory->name}}</span></a>
                                                            </li>
                                                            @endforeach

                                                        </ul>
                                                        @endforeach
                                                        @endisset
                                                    </div>

                                                    @endforeach


                                                    @endisset
                                                </div>


                                                <div class="col-md-4 has-sep hidden-sm">
                                                    <div class="custom-menu-right">
                                                        <div class="box-banner menu-banner">
                                                            <div class="add-right"><a href="#"><img
                                                                        src="{{asset('web/images/menu-img4.jpg')}}"
                                                                        alt="html template"></a></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </li>


                                @endforeach
                                @endisset


                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8">
                <div class="mtmegamenu">

                    @isset($menuTree)
                    {!!$menuTree!!}
                    @endisset
                </div>
            </div>
        </div>
    </div>
</nav>