@extends('layouts.web')
@push('styles')

<!-- flexslider CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/flexslider.css')}}">
<!-- Revolution Slider CSS -->
<link href="{{asset('web/css/revolution-slider.css')}}" rel="stylesheet" type="text/css">

@endpush

@section('content')
<!-- Breadcrumbs -->


<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li class="home"><a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
                    @isset($detailProduct)
                    <li class=""><a title="Go to {{$detailProduct->category->name}}"
                                    href="{{url('/category/list/'.$detailProduct->category->id)}}">{{ucfirst($detailProduct->category->name)}}</a><span>&raquo;</span>
                    </li>
                    <li class=""><a title="Go to {{$detailProduct->parentCategory->name}}"
                                    href="{{url('/category/list/'.$detailProduct->parentCategory->id)}}">{{ucfirst($detailProduct->parentCategory->name)}}</a><span>&raquo;</span>
                    </li>
                    <li class=""><a title="Go to {{$detailProduct->subCategory->name}}"
                                    href="{{url('/category/list/'.$detailProduct->subCategory->id)}}">{{ucfirst($detailProduct->subCategory->name)}}</a><span>&raquo;</span>
                    </li>
                    @if($detailProduct->secondSubCategory)
                    <li class=""><a title="Go to {{$detailProduct->secondSubCategory->name}}"
                                    href="{{url('/category/list/'.$detailProduct->secondSubCategory->id)}}">{{ucfirst($detailProduct->secondSubCategory->name)}}</a><span>&raquo;</span>
                    </li>

                    @endif
                    <li><strong>{{ucfirst($detailProduct->name)}}</strong></li>
                    @endisset
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumbs End -->


<!-- Main Container -->
<div class="main-container col2-left-layout">
    <!--success add Cart message-->
    @include('messages.addCartMessage')
    <!--end of add cart message-->
    <div class="container">
        <div class="row">
            <div class="col-main col-sm-9 col-xs-12 col-sm-push-3">
                @isset($detailProduct->selectedVariationDetail)
                <div class="product-view-area">
                    <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                        <div class="icon-sale-label sale-left">Sale</div>
                        <div class="large-image"><a
                                href="{{asset($detailProduct->selectedVariationDetail->featuredImg)}}"
                                class="cloud-zoom"
                                id="zoom1"
                                rel="useWrapper: false, adjustY:0, adjustX:20"> <img
                                    class="zoom-img"
                                    src="{{asset($detailProduct->selectedVariationDetail->featuredImg)}}"
                                    alt="products"> </a>
                        </div>
                        <div class="flexslider flexslider-thumb">
                            @isset($detailProduct->selectedVariationDetail->galleries)

                            <ul class="previews-list slides">
                                @foreach($detailProduct->selectedVariationDetail->galleries as $key=> $image)

                                <li><a href="{{asset($image->thumb_image_url)}}" class='cloud-zoom-gallery'
                                       rel="useZoom: 'zoom1', smallImage: '{{asset($image->thumb_image_url)}}' "><img
                                            src="{{asset($image->thumb_image_url)}}" alt="product img {{$key+1}}"/></a>
                                </li>

                                @endforeach
                            </ul>

                            @endisset
                        </div>

                        <!-- end: more-images -->

                    </div>
                    <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">

                        <div class="product-name">
                            <h2>{{$detailProduct->name}}</h2>
                        </div>
                        <div class="price-box">
                            @if($detailProduct->newPrice)
                            <p class="special-price"><span class="price-label">Special Price</span> <span class="price">Rs: {{$detailProduct->newPrice}}</span>
                            </p>

                            <p class="old-price"><span class="price-label">Regular Price:</span> <span class="price">Rs: {{$detailProduct->oldPrice}} </span>
                            </p>
                            @else
                            <p class="special-price"><span class="price-label">Special Price</span> <span class="price">Rs: {{$detailProduct->oldPrice}}</span>
                            </p>
                            @endif

                        </div>
                        <div class="ratings">
                            <div class="rating">


                                @for($i=1;$i<=5;$i++)

                                @if($i<=$detailProduct['rating'])
                                <i class="fa fa-star"></i>
                                @endif

                                @if($i>$detailProduct['rating'])
                                <i class="fa fa-star-o"></i>
                                @endif

                                @endfor

                            </div>
                            <p class="rating-links"><a href="#">{{$detailProduct['reviews']['total']}} Review(s)</a>
                                <span class="separator">|</span> <a
                                    href="#addreviews" onclick="addReviews()">Add Your Review</a></p>


                            <p class="availability in-stock pull-right">Availability: <span>In Stock</span></p>
                        </div>
                        <div class="short-description">
                            <h2>Quick Overview</h2>

                            <p>{!!$detailProduct->description!!}</p>

                        </div>
                        @isset($detailProduct->productVariation)
                        <!--                        variation-->
                        <div class="row product-color-size-area">


                            <div class="variation-area  col-md-12">
                                <h2 class="header-bar-title">Variations</h2>

                                <div class="variation col-md-12">
                                    @foreach($detailProduct->productVariation as $variation)
                                    <div class="col-md-3">

                                        <a href="{{url('detailProduct/'.$variation->product_id.'/'.$variation->id)}}"><img
                                                height="50px" width="70px"
                                                class="{{($variation->id==$detailProduct->selectedVariationDetail->id) ? 'thumbnail':''}}"
                                                src="{{asset($variation->featuredImg)}}" alt=""></a>
                                    </div>

                                    @endforeach


                                </div>

                            </div>

                            @if($detailProduct['sizes']!=null)
                            <div class="size-area  col-md-12">
                                <h2 class="header-bar-title">Size</h2>

                                <div class="size">
                                    <p class="cart-error addToCart-size-error text-danger" style="display: none;">
                                        *Please Select Size*</p>


                                    <ul>
                                        @foreach($detailProduct['sizes'] as $size)

                                        <li><a data-id="{{$size['id']}}" class="product_variation_size_btn">{{$size['name']}}</a>
                                        </li>

                                        @endforeach
                                    </ul>

                                </div>
                            </div>
                            @endif

                        </div>

                        <div class="row product-variation">
                            <form id="addToCartForm">


                                <input type="hidden" name="product_variation_id"
                                       value="{{$detailProduct->selectedVariationDetail->id}}"
                                       id="product_variation_id">
                                <input type="hidden" name="size_id" id=size_id">
                                <input type="hidden" name="product_id" value="{{$detailProduct->id}}">
                                <input type="hidden" name="maxQty" value="">

                                <div class="cart-plus-minus">
                                    <label for="qty">Qty:</label>

                                    <div class="numbers-row">
                                        <div
                                            onClick="var result = document.getElementById('product_qty'); var qty = result.value; if(!isNaN(qty) && qty != 1 )  result.value--;return false;"
                                            class="dec qtybutton"><i class="fa fa-minus">
                                                &nbsp;</i></div>

                                        <input type="text" class="qty" title="Qty" value="1"
                                               maxlength="1" id="product_qty" name="qty">

                                        <div
                                            onClick="var result = document.getElementById('product_qty'); var maxlength =result.getAttribute('maxlength'); var qty = result.value; if(!isNaN(qty) && qty != maxlength ) result.value++;return false;"
                                            class="inc qtybutton"><i class="fa fa-plus">
                                                &nbsp;</i></div>
                                    </div>
                                </div>
                                <button class="button pro-add-to-cart" title="Add to Cart"
                                        type="submit"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                </button>
                            </form>
                        </div>
                        <div class="product-cart-option">
                            <ul>
                                <li>
                                    @if(Auth::guard('web')->check())
                                    <a href="#" id="addToWishlist" data-id=""><i class="fa fa-heart"></i><span>Add to Wishlist</span></a>
                                    @else
                                    <a href="#" class="showLoginForm"><i
                                            class="fa fa-heart"></i><span>Add to Wishlist</span></a>
                                    @endif

                                </li>
                                <li><a href="#"><i class="fa fa-retweet"></i><span>Add to Compare</span></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i><span>Email Friend</span></a></li>
                            </ul>

                        </div>
                        <!--                        end of variation-->

                        @endisset
                    </div>
                </div>
                <div class="product-overview-tab">
                    <div class="product-tab-inner">
                        <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                            <li class="active"><a href="#description" data-toggle="tab"> Description </a></li>
                            @if($detailProduct->specification)

                            <li><a href="#specification" data-toggle="tab">Feature or Specification</a></li>
                            @endif
                            <li><a id="addreviews" href="#reviews" data-toggle="tab">Reviews</a></li>


                        </ul>
                        <div id="productTabContent" class="tab-content">
                            <div class="tab-pane fade in active" id="description">
                                <div class="std">
                                    {!!$detailProduct->description!!}
                                </div>
                            </div>





                            <div class="tab-pane fade" id="specification">
                                <div class="product-tabs-content-inner clearfix">
                                    {!!$detailProduct->specification!!}
                                </div>
                            </div>
                            <div id="reviews" class="tab-pane fade">
                                <div class="col-sm-5 col-lg-5 col-md-5">
                                    <div class="reviews-content-left">
                                        <h2>Customer Reviews</h2>

                                        @isset($detailProduct['reviews']['relatedReviews'])
                                        @if(count($detailProduct['reviews']['relatedReviews'])>0)

                                        @foreach($detailProduct['reviews']['relatedReviews'] as $review)
                                        <div class="review-ratting">
                                            @if($review->ratedStar==5)
                                            @php
                                            $result='Amazing !!';
                                            @endphp

                                            @elseif($review->ratedStar ==4)
                                            @php
                                            $result='Excellent !!';
                                            @endphp

                                            @else
                                            @php
                                            $result='Good !!';
                                            @endphp

                                            @endif


                                            <p>
                                                <a href="#">{{$result}}</a>
                                                Review by
                                                @if(Auth::guard('web')->check())
                                                   {{Auth::user()->id==$review->user->id ? 'You':$review->user->name}}

                                                @else
                                                {{$review->user->name}}
                                                @endif
                                            </p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>Rating</th>
                                                    <td>
                                                        <div class="rating">
                                                            @for($i=1;$i<=5;$i++)

                                                            @if($i<=$review['ratedStar'])
                                                            <i class="fa fa-star"></i>
                                                            @endif

                                                            @if($i>$review['ratedStar'])
                                                            <i class="fa fa-star-o"></i>
                                                            @endif

                                                            @endfor
                                                        </div>
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>
                                            <p class="author">

                                                <small> (Posted on {{date('d-m-Y', strtotime($review->updated_at))}})
                                                </small>
                                            </p>
                                        </div>
                                        @endforeach
                                        @else
                                        <h5>Review not Available..</h5>
                                        @endif
                                        @endisset

                                    </div>
                                </div>
                                <div class="col-sm-7 col-lg-7 col-md-7">
                                    <div class="reviews-content-right">
                                        <h2>Write Your Own Review</h2>

                                        <form method="post" action="{{url('product/review#addreviews/')}}">
                                            @include('messages.alertMessages')
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" @isset($ratedProduct)
                                                   value="{{$ratedProduct->id}}" @endisset>
                                            @isset($detailProduct)
                                            <input type="hidden" name="productId" value="{{$detailProduct->id}}">
                                            @endisset
                                            <h3>You're reviewing: <span>@isset($detailProduct){{$detailProduct->name}}@endisset</span>
                                            </h3>
                                            <h4>How do you rate this product?<em>*</em></h4>


                                            <div class="table-responsive reviews-table">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <th></th>
                                                        <th>1 star</th>
                                                        <th>2 stars</th>
                                                        <th>3 stars</th>
                                                        <th>4 stars</th>
                                                        <th>5 stars</th>
                                                    </tr>


                                                    <tr>

                                                        <td>Rating</td>
                                                        <td><input type="radio" checked name="ratedStar" value="1"></td>
                                                        <td><input type="radio" name="ratedStar" value="2"></td>
                                                        <td><input type="radio" name="ratedStar" value="3"></td>
                                                        <td><input type="radio" name="ratedStar" value="4"></td>
                                                        <td><input type="radio" name="ratedStar" value="5"></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="form-area">


                                                <div
                                                    class="form-element {{ $errors->has('title') ? ' has-error' : '' }} ">
                                                    <label>Title <em>*</em></label>
                                                    <input type="text" name="title">
                                                    @if ($errors->has('title'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('title') }}</strong>
                                                        </span>

                                                    @endif


                                                </div>
                                                <div
                                                    class="form-element {{ $errors->has('review') ? ' has-error' : '' }}">
                                                    <label>Review <em>*</em></label>

                                                    <textarea name="review"></textarea>
                                                    @if ($errors->has('title'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('review') }}</strong>
                                                        </span>

                                                    @endif
                                                </div>
                                                <div class="buttons-set">

                                                    @if(Auth::guard('web')->check())
                                                    <button class="button submit" title="Submit Review" type="submit">
                                                        <span><i class="fa fa-thumbs-up"></i> &nbsp;Send Review</span>
                                                    </button>
                                                    @else
                                                    <button class="button showLoginForm" type="button" id="">
                                                        <span><i class="fa fa-thumbs-up"></i> &nbsp;Send Review</span>
                                                    </button>
                                                    @endif
                                                </div>
                                        </form>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @else
                <h4>OOp's Product Not Available...</h4>
                @endisset


            </div>
        </div>
        `
        <!--sidebar-->
        <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">


            <div class="block special-product">
                <div class="sidebar-bar-title">
                    <h3>Related Products</h3>
                </div>
                <div class="block-content">
                    @isset($detailProduct['relatedProducts'])
                    <ul>
                        @foreach($detailProduct['relatedProducts'] as $product)
                        <li class="item">
                            <div class="products-block-left"><a href="{{url('detailProduct/'.$product->id)}}"
                                                                title="Sample Product"
                                                                class="product-image"><img
                                        src="{{str_replace('/shares/','/shares/thumbs/',$product->featuredImg)}}" alt="Sample Product "></a></div>
                            <div class="products-block-right">
                                <p class="product-name"><a href="{{url('detailProduct/'.$product->id)}}">{{$product->name}}</a>
                                </p>
                                @if($product->newPrice)
                                <span class="price">Rs: {{$product->oldPrice}}</span>
                                @else
                                <span class="price">Rs: {{$product->oldPrice}}</span>
                                @endif

                                <div class="rating">


                                    @for($i=1;$i<=5;$i++)

                                    @if($i<=$detailProduct['rating'])
                                    <i class="fa fa-star"></i>
                                    @endif

                                    @if($i>$detailProduct['rating'])
                                    <i class="fa fa-star-o"></i>
                                    @endif

                                    @endfor

                                </div>
                            </div>
                        </li>
                        @endforeach

                    </ul>

                    <a class="link-all" href="{{url('/category/list/'.$detailProduct->sub_category_id)}}">All
                        Products</a></div>
                @endisset
            </div>

            <div class="single-img-add sidebar-add-slider ">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active"><img src="{{asset('web/images/add-slide1.jpg')}}" alt="slide1">

                            <div class="carousel-caption">
                                <h3><a href="single_product.html" title=" Sample Product">Sale Up to 50% off</a>
                                </h3>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <a href="#" class="info">shopping Now</a></div>
                        </div>
                        <div class="item"><img src="{{asset('web/images/add-slide2.jpg')}}" alt="slide2">

                            <div class="carousel-caption">
                                <h3><a href="single_product.html" title=" Sample Product">Smartwatch
                                        Collection</a>
                                </h3>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <a href="#" class="info">All Collection</a></div>
                        </div>
                        <div class="item"><img src="{{asset('web/images/add-slide3.jpg')}}" alt="slide3">

                            <div class="carousel-caption">
                                <h3><a href="single_product.html" title=" Sample Product">Summer Sale</a></h3>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button"
                       data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
                                                aria-hidden="true"></span>
                        <span class="sr-only">Previous</span> </a> <a class="right carousel-control"
                                                                      href="#carousel-example-generic"
                                                                      role="button"
                                                                      data-slide="next"> <span
                            class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span
                            class="sr-only">Next</span> </a></div>
            </div>


        </aside>
        <!--  end of sidebar-->

    </div>
</div>
</div>

<!-- Main Container End -->
<!--add to select size modal-->
@include('web.product.selectSizeModal')
<!--end of add to size modal-->

<!--login Model-->
@include('auth.ajaxLoginForm')




@endsection
@push('scripts')


<script type="text/javascript" src="{{asset('web\js/jquery.flexslider.js')}}"></script>
<script type="text/javascript" src="{{asset('web\js/cloud-zoom.js')}}"></script>
<script src="{{asset('build/pages/web/detailPage/shoppingCart.js')}}"></script>
<script src="{{asset('build/pages/web/detailPage/wishlist.js')}}"></script>
<script src="{{asset('build/pages/web/auth.js')}}"></script>
<script>
    var url = window.location.href;
    if (url.substring(url.indexOf("#") + 1) == 'addreviews') {
        addReviews();
    }
    function addReviews() {
        $('#addreviews').click();
    }
    //    auth login
    $('.showLoginForm').on('click', function () {


        $('#loginFormModal').modal('show');

    })

</script>


@endpush


