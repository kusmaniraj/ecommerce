@extends('web.product.layout.productList')
@section('list_content')
<div class="col-main col-sm-9 col-xs-12 col-sm-push-3">

    <div class="shop-inner">
        @isset($categoryProducts)
        @if(count($categoryProducts['products'])>0)
        <div class="page-title">
            <h3> @isset($categoryProducts['title']){{ucfirst($categoryProducts['title'])}}@endisset</h3>


        </div>
        <div class="toolbar">
            <div class="view-mode">
                <ul>
                    <li id="gird-product-page" class="active"><a href="#"> <i class="fa fa-th-large"></i>
                        </a></li>
                    <li id="list-product-page"><a href="{{str_replace('grid','list',url()->current())}}"> <i
                                class="fa fa-th-list"></i>
                        </a></li>
                </ul>
            </div>
            <div class="sorter">


                <div class="short-by">
                    <label>Sort By:</label>
                    <select id="order_by_name">
                        <option selected="selected" {{request()->orderBy=="id" ? 'selected':''}} value="id">Position
                        </option>
                        <option {{request()->orderBy=="rating" ? 'selected':''}} value="rating">Rating</option>
                        <option {{request()->orderBy=="name" ? 'selected':''}} value="name">Name</option>
                        <option {{request()->orderBy=="price" ? 'selected':''}} value="oldPrice">Least Price</option>

                    </select>
                </div>
                <div class="short-by page">
                    <label>Show:</label>
                    <select id="sort_by_page">
                        <option selected="selected" {{request()->showPage=="9" ? 'selected':''}}>9</option>
                        <option {{request()->showPage=="12" ? 'selected':''}} value="12">12</option>
                        <option {{request()->showPage=="16" ? 'selected':''}} value="16">16</option>
                        <option {{request()->showPage=="30" ? 'selected':''}} value="16">30</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="product-grid-area">

            <ul class="products-grid">
                @foreach($categoryProducts['products'] as $product)
                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6 ">
                    <div class="product-item">
                        <div class="item-inner">
                            <div class="product-thumbnail">
                                <div class="icon-sale-label sale-left">Sale</div>
                                <div class="icon-new-label new-right">New</div>
                                <div class="pr-img-area"><a title="{{$product->name}}"
                                                            href="{{url('detailProduct/'.$product->id)}}">
                                        <figure><img class="first-img"
                                                     src="{{str_replace('/shares/','/shares/thumbs/',$product->featuredImg)}}" alt=""> <img
                                                class="hover-img" src="{{str_replace('/shares/','/shares/thumbs/',$product->featuredImg)}}"
                                                alt=""></figure>
                                    </a>
                                    <a href="{{url('detailProduct/'.$product->id)}}" data-product_id="{{$product->id}}"
                                       type="button" class="add-to-cart-mt"><i
                                            class="fa fa-shopping-cart"></i><span> Buy</span>
                                    </a>
                                </div>
<!--                                <div class="pr-info-area">-->
<!--                                    <div class="pr-button">-->
<!--                                        <div class="mt-button add_to_wishlist"><a href="wishlist.html"> <i-->
<!--                                                    class="fa fa-heart"></i> </a></div>-->
<!--                                        <div class="mt-button add_to_compare"><a href="compare.html"> <i-->
<!--                                                    class="fa fa-signal"></i> </a></div>-->
<!--                                        <div class="mt-button quick-view"><a-->
<!--                                                data-product_id="{{$product->id}}" class="quick-view-btn"-->
<!--                                                href="#"> <i-->
<!--                                                    class="fa fa-search"></i> </a></div>-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title"><a title="{{$product->name}}"
                                                               href="{{url('detailProduct/'.$product->id)}}.html">{{$product->name}} </a>
                                    </div>
                                    <div class="item-content">
                                        <div class="rating"> @for($i=1;$i<=5;$i++)

                                            @if($i<=$product['rating'])
                                            <i class="fa fa-star"></i>
                                            @endif

                                            @if($i>$product['rating'])
                                            <i class="fa fa-star-o"></i>
                                            @endif

                                            @endfor
                                        </div>
                                        <div class="price-box">
                                            @isset($product->newPrice)
                                            <p class="special-price"><span class="price-label"></span> <span
                                                    class="price">Rs &nbsp;{{$product->newPrice}}</span>
                                            </p>

                                            <p class="old-price"><span class="price-label"></span> <span
                                                    class="price"> Rs &nbsp;{{$product->oldPrice}} </span>
                                            </p>
                                            @else
                                            <p class="special-price"><span class="price-label"></span> <span
                                                    class="price"> Rs &nbsp;{{$product->oldPrice}} </span>
                                            </p>
                                            @endisset
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                @endforeach

            </ul>


            <!--            end products-->

        </div>
        <div class="pagination-area ">

            {{$categoryProducts['products']->links()}}

        </div>
        @else
        <h4>No Search Product........</h4>
        @endif

        <!--        end categoryProducts['products']-->

        @endisset
        <!--        categoryProducts-->
    </div>
</div>
<!-- Main Container End -->
<!--quick view-->
<!--<div class="modal fade" id="quickViewProductModal">-->
<!--    <div class="modal-dialog modal-lg">-->
<!---->
<!--        <div class="modal-content">-->
<!--            <button type="button" class="pull-left close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
<!---->
<!--            <div class="modal-body">-->
<!---->
<!---->
<!--            </div>-->
<!--            <div class="modal-footer">-->
<!---->
<!---->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--end of quick view -->

@endsection