@extends('layouts.web')
@push('styles')
<!-- flexslider CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('web/css/flexslider.css')}}">
<!-- Revolution Slider CSS -->
<link href="{{asset('web/css/revolution-slider.css')}}" rel="stylesheet">

@endpush


@section('content')
@isset($categoryProducts)
<!-- Breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li class="home"><a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>
                    @if($categoryProducts['second_sub_category'] )
                    <li class=""><a title="Go to {{$categoryProducts['category']->name}}"
                                    href="{{url('/category/'.Request::segment(2) .'/'.$categoryProducts['category']->id)}}">{{ucfirst($categoryProducts['category']->name)}}</a><span>&raquo;</span>
                    </li>
                    <li class=""><a title="Go to {{$categoryProducts['parent_category']->name}}"
                                    href="{{url('/category/'.Request::segment(2) .'/'.$categoryProducts['parent_category']->id)}}">{{ucfirst($categoryProducts['parent_category']->name)}}</a><span>&raquo;</span>
                    </li>

                    <li class=""><a title="Go to {{$categoryProducts['sub_category']->name}}"
                                    href="{{url('/category/'.Request::segment(2) .'/'.$categoryProducts['sub_category']->id)}}">{{ucfirst($categoryProducts['sub_category']->name)}}</a><span>&raquo;</span>
                    </li>

                    <li><strong>{{ucfirst($categoryProducts['second_sub_category']->name)}}</strong></li>


                    @elseif($categoryProducts['sub_category'] )
                    <li class=""><a title="Go to {{$categoryProducts['category']->name}}"
                                    href="{{url('/category/'.Request::segment(2) .'/'.$categoryProducts['category']->id)}}">{{$categoryProducts['category']->name}}</a><span>&raquo;</span>
                    </li>
                    <li class=""><a title="Go to {{$categoryProducts['parent_category']->name}}"
                                    href="{{url('/category/'.Request::segment(2) .'/'.$categoryProducts['parent_category']->id)}}">{{$categoryProducts['parent_category']->name}}</a><span>&raquo;</span>
                    </li>

                    <li><strong>{{$categoryProducts['sub_category']->name}}</strong></li>


                    @elseif( $categoryProducts['parent_category'] && $categoryProducts['category'] )
                    <li class=""><a title="Go to {{$categoryProducts['category']->name}}"
                                    href="{{url('/category/'.Request::segment(2) .'/'.$categoryProducts['category']->id)}}">{{$categoryProducts['category']->name}}</a><span>&raquo;</span>
                    </li>
                    <li><strong>{{$categoryProducts['parent_category']->name}}</strong></li>

                    @elseif($categoryProducts['category'])

                    <li><strong>{{$categoryProducts['category']->name}}</strong></li>
                    @else
                    <li><strong></strong></li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumbs End -->
@endisset
<!-- Main Container -->

<div class="main-container col2-left-layout">
    <div class="container">
        <div class="row">
            @yield('list_content')
            <aside class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">

                <div class="block shop-by-side">
                    <div class="sidebar-bar-title">
                        <h3>Search By</h3>

                    </div>
                    <div class="block-content">

                        @isset($categoryProducts)

                        <div class="manufacturer-area">
                            <h2 class="saider-bar-title">Categories</h2>

                            <div class="saide-bar-menu">
                                <!--                                list Main Categories-->
                                @isset($categoryProducts['categories'])
                                <ul>
                                    @foreach($categoryProducts['categories'] as $category)

                                    <li><a href="{{url('/category/'.Request::segment(2) .'/'.$category->id)}}"><i
                                                class="fa fa-angle-right"></i> {{ucfirst( $category->name)}}&nbsp;<span
                                                class="count label label-default">&nbsp;{{count($category->category_products)}}</span></a>
                                    </li>
                                    @endforeach
                                </ul>
                                @endisset
                                <!--                                end Main Categories-->
                                <!--                                list Parent Categories-->
                                @isset($categoryProducts['parent_categories'])
                                <ul>
                                    @foreach($categoryProducts['parent_categories'] as $category)

                                    <li><a href="{{url('/category/'.Request::segment(2) .'/'.$category->id)}}"><i
                                                class="fa fa-angle-right"></i> {{ucfirst($category->name)}}&nbsp;<span
                                                class="count label label-default">&nbsp;{{count($category->parent_category_products)}}</span></a>
                                    </li>
                                    @endforeach
                                </ul>
                                @endisset
                                <!--                                end Parent Categories-->

                                <div class="saide-bar-menu">
                                    <!--                                list Sub Categories-->
                                    @isset($categoryProducts['sub_categories'])
                                    <ul>
                                        @foreach($categoryProducts['sub_categories'] as $category)

                                        <li><a href="{{url('/category/'.Request::segment(2) .'/'.$category->id)}}"><i
                                                    class="fa fa-angle-right"></i> {{ucfirst($category->name)}}
                                                &nbsp;<span
                                                    class="count label label-default ">&nbsp;{{count($category->sub_category_products)}}</span></a>
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endisset
                                    <!--                                end Sub Categories-->
                                    <!--                                    start second Sub Categories-->

                                    @isset($categoryProducts['second_sub_categories'])
                                    <ul>
                                        @foreach($categoryProducts['second_sub_categories'] as $category)

                                        <li><a href="{{url('/category/'.Request::segment(2) .'/'.$category->id)}}"><i
                                                    class="fa fa-angle-right"></i>
                                                {{ucfirst($category->name)}}&nbsp;<span
                                                    class="count label label-default">&nbsp;{{count($category->second_sub_category_products)}}</span></a>
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endisset
                                    <!--                                end Parent Categories-->


                                </div>
                            </div>
                            @endisset
                            @isset($categoryProducts['categories'])
                            </br>
                            @else
                            <div class="rating">
                                <h2 class="saider-bar-title">Rating</h2>
                                <ul>
                                    @if(request()->has('rating'))
                                    @for($i=1;$i<= 5; $i++ )
                                    @if($i==request()->rating)
                                    <li class="rate-star" ><input id="star{{$i}}" type="radio" value="{{$i}}" checked>&nbsp; <label
                                            for="star{{$i}}">{{$i}}&nbsp;{{$i==1 ? 'Star':'Stars'}}</label></li>
                                    @else
                                    <li class="rate-star" ><input type="radio" id="star{{$i}}" value="{{$i}}" >&nbsp;{{$i}}&nbsp;<label
                                            for="star{{$i}}">{{$i==1 ? 'Star':'Stars'}}</label></li>
                                    @endif

                                    @endfor
                                    @else

                                    @for($i=1;$i<= 5; $i++ )
                                    <li class="rate-star" ><input type="radio" id="star{{$i}}" value="{{$i}}" >&nbsp; <label
                                            for="star{{$i}}">{{$i}}&nbsp;{{$i==1 ? 'Star':'Stars'}}</label></li>
                                    @endfor
                                    @endif

                                </ul>


                            </div>
                            <div class=" product-price-range ">
                                <h2 class="saider-bar-title">Price</h2>

                                <div class="slider-range">

                                    <div id="slider-range-price" data-label-reasult="Range:"
                                         data-min="{{$categoryProducts['price']['min']}}"
                                         data-max="{{$categoryProducts['price']['max']}}"
                                         data-unit="Rs:"
                                         class="slider-range-price"
                                         data-value-min="{{request()->has('price')  ? explode('-',request()->price)[0]:$categoryProducts['price']['min']+100}}"
                                         data-value-max="{{request()->has('price')  ? explode('-',request()->price)[1]:$categoryProducts['price']['max']-100}}"></div>
                                    <div class="amount-range-price">Range: Rs:{{request()->has('price') ?
                                        explode('-',request()->price)[0]:$categoryProducts['price']['min']+100}} -
                                        Rs:{{request()->has('price') ?
                                        explode('-',request()->price)[1]:$categoryProducts['price']['max']-100}}
                                    </div>

                                </div>

                            </div>
                            @endisset


                        </div>
                    </div>



            </aside>
        </div>
    </div>
</div>
<!-- Main Container End -->


@endsection
@push('scripts')


<script type="text/javascript" src="{{asset('web\js/jquery.flexslider.js')}}"></script>
<script type="text/javascript" src="{{asset('web\js/cloud-zoom.js')}}"></script>
<script src="{{asset('build/pages/web/detailPage/shoppingCart.js')}}"></script>
<script src="{{asset('build/pages/web/listProduct.js')}}"></script>


@endpush


