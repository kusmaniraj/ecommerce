@extends('web.product.layout.productList')
@section('list_content')
<div class="col-main col-sm-9 col-xs-12 col-sm-push-3">


    <div class="shop-inner">
        @isset($categoryProducts)
        @if(count($categoryProducts['products'])>0)
        <div class="page-title">
            <h3> @isset($categoryProducts['title']){{ucfirst($categoryProducts['title'])}}@endisset</h3>


        </div>
        <div class="toolbar">
            <div class="view-mode">
                <ul>
                    <li id="gird-product-page"><a href="{{str_replace('list','grid',url()->current())}}"> <i class="fa fa-th-large"></i>
                        </a></li>
                    <li id="list-product-page" class="active"><a href="#"> <i class="fa fa-th-list"></i>
                        </a></li>
                </ul>
            </div>
            <div class="sorter">


                <div class="short-by">
                    <label>Sort By:</label>
                    <select id="order_by_name">
                        <option selected="selected"  {{request()->orderBy=="id" ? 'selected':''}} value="id">Position</option>
                        <option {{request()->orderBy=="rating" ? 'selected':''}} value="rating">Rating</option>
                        <option {{request()->orderBy=="name" ? 'selected':''}} value="name">Name</option>
                        <option  {{request()->orderBy=="price" ? 'selected':''}} value="oldPrice">Least Price</option>

                    </select>
                </div>
                <div class="short-by page">
                    <label>Show:</label>
                    <select id="sort_by_page">
                        <option selected="selected"  {{request()->showPage=="9" ? 'selected':''}}>9</option>
                        <option  {{request()->showPage=="12" ? 'selected':''}} value="12">12</option>
                        <option  {{request()->showPage=="16" ? 'selected':''}}  value="16">16</option>
                        <option  {{request()->showPage=="30" ? 'selected':''}}  value="16">30</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="product-list-area">


            <ul class="products-list" id="products-list">
                @foreach($categoryProducts['products'] as $product)
                <li class="item ">
                    <div class="product-img">
                        <div class="icon-sale-label sale-left">Sale</div>
                        <a href="{{url('detailProduct/'.$product->id)}}" title="{{$product->name}}">
                            <figure> <img class="small-image"  src="{{str_replace('/shares/','/shares/thumbs/',$product->featuredImg)}}" alt="{{$product->name}}"></figure>
                        </a> </div>
                    <div class="product-shop">
                        <h2 class="product-name"><a href="{{url('detailProduct/'.$product->id)}}" title="{{$product->name}}">{{$product->name}}</a></h2>
                        <div class="ratings">
                            <div class="rating">
                                @for($i=1;$i<=5;$i++)

                                @if($i<=$product['rating'])
                                <i class="fa fa-star"></i>
                                @endif

                                @if($i>$product['rating'])
                                <i class="fa fa-star-o"></i>
                                @endif

                                @endfor
                            </div>

                        </div>
                        <div class="price-box">
                            @isset($product->newPrice)
                            <p class="special-price"><span class="price-label"></span> <span
                                    class="price">Rs &nbsp;{{$product->newPrice}}</span>
                            </p>

                            <p class="old-price"><span class="price-label"></span> <span
                                    class="price"> Rs &nbsp;{{$product->oldPrice}} </span>
                            </p>
                            @else
                            <p class="special-price"><span class="price-label"></span> <span
                                    class="price"> Rs &nbsp;{{$product->oldPrice}} </span>
                            </p>
                            @endisset
                        </div>
                        <div class="desc std">
                            <p>{!!str_limit($product->description,100)!!}...&nbsp;<a class="link-learn" title="Learn More" href="{{url('detailProduct/'.$product->id)}}">Learn More</a> </p>
                        </div>
                        <div class="actions">

                            <a  href="{{url('detailProduct/'.$product->id)}}" class="button cart-button"><i class="fa fa-shopping-cart"></i><span>&nbsp;Buy</span></a>

                            <ul>
                                <li> <a href="wishlist.html"> <i class="fa fa-heart"></i><span> Add to Wishlist</span> </a> </li>
                                <li> <a href="compare.html"> <i class="fa fa-signal"></i><span> Add to Compare</span> </a> </li>
                            </ul>
                        </div>
                    </div>
                </li>

                @endforeach

            </ul>

        </div>
        <div class="pagination-area ">

            {{$categoryProducts['products']->links()}}

        </div>
        @else
        <h4>No Search Product..</h4>
        @endif
        @endisset

    </div>

</div>
<!--quick view-->
<div class="modal fade" id="quickViewProductModal">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <button type="button" class="pull-left close" data-dismiss="modal" aria-hidden="true">&times;</button>

            <div class="modal-body">


            </div>
            <div class="modal-footer">


            </div>

        </div>
    </div>
</div>
<!--end of quick view -->


@endsection