

<div style="display: block;" id="quick_view_popup-wrap">


    <div id="quick_view_popup-outer">
        <div id="quick_view_popup-content">
            <div style="width:auto;height:auto;overflow: auto;position:relative;">
                <!--product-view-->
                @isset($detailProduct->selectedVariationDetail)
                <div class="product-view-area">
                    <div class="product-big-image col-xs-12 col-sm-5 col-lg-5 col-md-5">
                        <div class="icon-sale-label sale-left">Sale</div>
                        <div class="large-image"><a href="{{asset($detailProduct->selectedVariationDetail->featuredImg)}}" class="cloud-zoom"
                                                    id="zoom1"
                                                    rel="useWrapper: false, adjustY:0, adjustX:20"> <img
                                    class="zoom-img" src="{{asset($detailProduct->selectedVariationDetail->featuredImg)}}" alt="products"> </a>
                        </div>
                        <div class="flexslider flexslider-thumb">
                            @isset($detailProduct->selectedVariationDetail->galleries)

                            <ul class="previews-list slides">
                                @foreach($detailProduct->selectedVariationDetail->galleries as $key=> $image)

                                <li><a href="{{asset($image->thumb_image_url)}}" class='cloud-zoom-gallery'
                                       rel="useZoom: 'zoom1', smallImage: '{{asset($image->thumb_image_url)}}' "><img
                                            src="{{asset($image->thumb_image_url)}}" alt="product img {{$key+1}}"/></a></li>

                                @endforeach
                            </ul>

                            @endisset
                        </div>

                        <!-- end: more-images -->

                    </div>
                    <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7 product-details-area">

                        <div class="product-name">
                            <h1>{{$detailProduct->name}}</h1>
                        </div>
                        <div class="price-box">
                            @if($detailProduct->newPrice)
                            <p class="special-price"><span class="price-label">Special Price</span> <span class="price">Rs: {{$detailProduct->newPrice}}</span>
                            </p>

                            <p class="old-price"><span class="price-label">Regular Price:</span> <span class="price">Rs: {{$detailProduct->oldPrice}} </span>
                            </p>
                            @else
                            <p class="special-price"><span class="price-label">Special Price</span> <span class="price">Rs: {{$detailProduct->oldPrice}}</span>
                            </p>
                            @endif

                        </div>
                        <div class="ratings">
                            <div class="rating"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                    class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                            </div>
                            <p class="rating-links"><a href="#">1 Review(s)</a> <span class="separator">|</span> <a
                                    href="#">Add Your Review</a></p>

                            <p class="availability in-stock pull-right">Availability: <span>In Stock</span></p>
                        </div>
                        <div class="short-description">
                            <h2>Quick Overview</h2>

                            <p>{!!$detailProduct->description!!}</p>

                        </div>
                        @isset($detailProduct->productVariation)
                        <!--                        variation-->
                        <div class="row product-color-size-area">


                            <div class="variation-area  col-md-12">
                                <h2 class="saider-bar-title">Variations</h2>

                                <div class="variation col-md-12">
                                    @foreach($detailProduct->productVariation as $variation)
                                    <div class="col-md-3">

                                        <a href="{{url('detailProduct/'.$variation->product_id.'/'.$variation->id)}}" ><img height="50px" width="70px" class="{{($variation->id==$detailProduct->selectedVariationDetail->id) ? 'thumbnail':''}}" src="{{asset($variation->featuredImg)}}" alt=""></a>
                                    </div>

                                    @endforeach




                                </div>

                            </div>

                            @if($detailProduct['sizes']!=null)
                            <div class="size-area  col-md-12">
                                <h2 class="saider-bar-title">Size</h2>

                                <div class="size">
                                    <p class="cart-error addToCart-size-error text-danger" style="display: none;">*Please Select Size*</p>


                                    <ul>
                                        @foreach($detailProduct['sizes'] as $size)

                                        <li><a data-id="{{$size['id']}}" class="quick-view_product_variation_size_btn">{{$size['name']}}</a></li>

                                        @endforeach
                                    </ul>

                                </div>
                            </div>
                            @endif

                        </div>

                        <div class="row product-variation">
                            <form id="addToCartQuckViewForm">


                                <input type="hidden" name="product_variation_id" value="{{$detailProduct->selectedVariationDetail->id}}" id="product_variation_id">
                                <input type="hidden" name="size_id" id="size_id">
                                <input type="hidden" name="product_id" value="{{$detailProduct->id}}">
                                <input type="hidden" name="maxQty" value="">

                                <div class="cart-plus-minus">
                                    <label for="qty">Qty:</label>

                                    <div class="numbers-row">
                                        <div
                                            onClick="var result = document.getElementById('product_qty'); var qty = result.value; if(!isNaN(qty) && qty != 1 )  result.value--;return false;"
                                            class="dec qtybutton"><i class="fa fa-minus">
                                                &nbsp;</i></div>

                                        <input type="text" class="qty" title="Qty" value="1"
                                               maxlength="1" id="product_qty" name="qty">

                                        <div
                                            onClick="var result = document.getElementById('product_qty'); var maxlength =result.getAttribute('maxlength'); var qty = result.value; if(!isNaN(qty) && qty != maxlength ) result.value++;return false;"
                                            class="inc qtybutton"><i class="fa fa-plus">
                                                &nbsp;</i></div>
                                    </div>
                                </div>
                                <button class="button pro-add-to-cart" title="Add to Cart"
                                        type="submit"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
                                </button>
                            </form>
                        </div>
                        <div class="product-cart-option">
                            <ul>
                                <li><a href="#"><i class="fa fa-heart"></i><span>Add to Wishlist</span></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i><span>Add to Compare</span></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i><span>Email Friend</span></a></li>
                            </ul>

                        </div>
                        <!--                        end of variation-->

                        @endisset
                    </div>
                </div>

                @endisset


            </div>
        </div>

    </div>
</div>