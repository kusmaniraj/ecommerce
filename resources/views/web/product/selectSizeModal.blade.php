<div class="modal fade" id="selectSizeModal">
    <div class="modal-dialog modal-md">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select size </h4>
                <button type="button" class="pull-left close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <div class="modal-body">
                <div class="row">


                    @if($detailProduct['sizes']!=null)


                    <div class="size-area  col-md-12">


                        <div class="size">
                            <p class=" size-title text-info">
                            </p>

                            <p class="cart-error addToCart-size-error text-danger" style="display: none;">
                                *Please Select Size*</p>


                            <ul style="list-style: none">
                                @foreach($detailProduct['sizes'] as $size)

                                <li><a data-id="{{$size['id']}}"
                                       class="size_btn">{{$size['name']}}</a>
                                </li>

                                @endforeach
                            </ul>

                        </div>


                    </div>

                    <div class="col-md-12">
                        <button class="button select-size pull-right" title="Select"
                                type="submit"><span> Select</span>
                        </button>
                    </div>
                    @endif


                </div>


            </div>
        </div>
    </div>
</div>