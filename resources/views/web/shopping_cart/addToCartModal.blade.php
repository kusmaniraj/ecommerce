<div class="modal fade" id="addProductToCartModal">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <button type="button" class="pull-left close" data-dismiss="modal" aria-hidden="true">&times;</button>

            <div class="modal-body">
                <form id="addToCartForm">

                    <input type="hidden" name="product_id" value="">
                    <input type="hidden" name="product_variation_id" value="" id="product_variation_id">


                    <input type="hidden" name="maxQty" value="">
                    </form>
            </div>
            <div class="modal-footer">


            </div>

        </div>
    </div>
</div>