@extends('layouts.web')
@push('styles')


@endpush

@section('content')
<!-- Breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul>
                    <li class="home"><a title="Go to Home Page" href="{{url('/')}}">Home</a><span>&raquo;</span></li>

                    <li><strong>Shopping Cart</strong></li>


                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumbs End -->
<!-- Main Container -->
<section class="main-container col1-layout">
    <div class="main container">
        <div class="col-main">
            <div class="cart">

                <div class="page-content page-order">
                    <div class="page-title">
                        <h2>Shopping Cart</h2>
                    </div>


                    @isset($sessionCart)

                    <?php

                    $total=0;
                    ?>
                    <div class="order-detail-content">
                        <div class="table-responsive">
                            <table class="table table-bordered cart_summary">
                                <thead>
                                <tr>
                                    <th class="cart_product">Product</th>
                                    <th>Description</th>

                                    <th>Unit price</th>
                                    <th>Qty</th>
                                    <th>Sub Total</th>
                                    <th class="action"><i class="fa fa-trash-o"></i></th>
                                </tr>
                                </thead>
                                <tbody>



                                @foreach($sessionCart as $key=>$cart)
                                <tr>
                                    <td class="cart_product"><a href="#"><img src="{{$cart['variationWithProduct']['featuredImg']}}"
                                                                              alt="Product"></a></td>
                                    <td class="cart_description"><p class="product-name"><a href="{{url('detailProduct/'.$cart['product_id'])}}">{{$cart['variationWithProduct']['product']['name']}}
                                                 </a></p>

                                        @isset($cart['selectedSizeName'])
                                        <small><a href="#">Size : {{$cart['selectedSizeName']}}</a></small>
                                        @endisset
                                    </td>

                                    @isset($cart['variationWithProduct']['product']['newPrice'])
                                    <td class="price">Rs: <span>{{$cart['variationWithProduct']['product']['newPrice']}}</span></td>
                                    @else
                                    <td class="price">Rs: <span>{{$cart['variationWithProduct']['product']['oldPrice']}}</span></td>
                                    @endisset

                                    <td class="qty form-group">


                                        <select data-productId="{{$cart['product_id']}}" data-productVariationId="{{$cart['product_variation_id']}}" class="form-control" name="qty" id="cart-qty">
                                        <?php
                                        for($i=1; $i<=$cart['maxQty'];$i++){ ?>
                                            <option value="{{$i}}" {{$cart['selectedQty']==$i ? 'selected' : '' }} >{{$i}}</option>
                                        <?php }?>
                                        </select>
                                    </td>
                                    <td class="price">Rs: <span>{{$cart['sub_total']}}</span></td>
                                    <td class="action"><a href="{{url('cart/removeFromCart/'.$key)}}"><i class="icon-close"></i></a></td>
                                </tr>
                                @php
                                $total +=$cart['sub_total'];
                                @endphp
                                @endforeach




                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2" rowspan="2"></td>
                                    <td colspan="3">Total products (tax incl.)</td>
                                    <td colspan="2">Rs: &nbsp; {{$total}}</td>
                                </tr>
                                <tr>
                                    <td colspan="3"><strong>Total</strong></td>
                                    <td colspan="2"><strong>Rs: &nbsp; {{$total}} </strong></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="cart_navigation"><a class="continue-btn" href="{{url('/')}}"><i class="fa fa-arrow-left"> </i>&nbsp;
                                Continue shopping</a> <a class="checkout-btn" href="{{url('checkout')}}"><i class="fa fa-check"></i>
                                Proceed to checkout</a></div>
                    </div>
                    @else
                    <h4 style="text-align: center">OOP's You have not add Products in cart...</h4>
                    <div class="cart_navigation"><a class="continue-btn" href="{{url('/')}}"><i class="fa fa-arrow-left"> </i>&nbsp;
                            Continue shopping</a> </div>
                    @endisset

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Main Container End -->


@endsection
@push('scripts')
<script>
    $('.cart_summary').on('change','select[name="qty"]',function(){
        var product_id=$(this).data('productid');
        var product_variation_id=$(this).data('productvariationid');
        var qty=$(this).val();
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        var formData={
            _token:csrfToken,
            product_id:product_id,
            product_variation_id:product_variation_id,
            qty:qty,
        }
        $.post('cart/changeQty',formData,function(res){
            console.log(res.success);
            if(res.success){
                window.location.href=res.url;
            }
        })
    })
</script>


@endpush


