<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//web auth
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware(['guest:web'])->group(function () {
    Route::post('/ajaxLogin', 'Auth\LoginController@ajaxLogin')->name('ajaxLogin');
    Route::post('/ajaxRegister', 'Auth\RegisterController@ajaxRegister')->name('ajaxRegister');
});

//admin route
Route::group(['prefix' => 'admin', 'namespace' => 'Auth'], function () {
    Route::middleware(['auth:admin'])->group(function () {
        Route::get('/', 'AdminController@index')->name('admin.dashboard');
    });
    Route::middleware(['guest:admin'])->group(function () {
        Route::get('/login', 'AdminController@showLoginForm')->name('admin.login');
        Route::post('/login', 'AdminController@login')->name('admin.login.submit');
        Route::get('/register', 'AdminController@showRegisterForm')->name('admin.register');
        Route::post('/register', 'AdminController@register')->name('admin.register.submit');

        Route::get('password/reset', 'AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('password/email', 'AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');;
        Route::get('password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('admin.password.reset');;
        Route::post('password/reset', 'AdminResetPasswordController@reset');


    });
    Route::get('/logout', 'AdminController@logout')->name('admin.logout');


});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth:admin']], function () {
    //    superadmin route
    Route::resource('/adminManagement', 'AdminManagementController');

//    admin route
    Route::group(['prefix' => 'setting'], function () {
        Route::get('/', 'SettingController@index');
        Route::post('/', 'SettingController@store')->name('setting.store');
        Route::put('/{id}', 'SettingController@update')->name('setting.update');

        Route::group(['prefix' => 'image_icon'], function () {
            Route::get('/', 'Image_IconSettingController@index');
            Route::post('/', 'Image_IconSettingController@store');

            Route::put('/{id}', 'Image_IconSettingController@update');

        });

        Route::group(['prefix' => 'mailer'], function () {
            Route::get('/', 'MailerSettingController@index');
            Route::post('/', 'MailerSettingController@store')->name('mailer.store');
            Route::get('/{id}/edit', 'MailerSettingController@edit');
            Route::put('/{id}', 'MailerSettingController@update')->name('mailer.update');
            Route::delete('/delete/{id}', 'MailerSettingController@destroy');

        });

    });
//    user Manage
    Route::group(['prefix' => 'user'], function () {
        Route::get('/{userType}/', 'UserController@index');
        Route::get('/showAccountInfo/{email}', 'UserController@showAccountInfo');
        Route::get('/getUsers/{userType}/', 'UserController@getUsers');
        Route::delete('/delete/{userType}/{id}', 'UserController@delete')->name('user.delete');
        Route::get('/getShippingInfo/{email}', 'UserController@getShippingInfo');
        Route::get('/showShippingInfo/{id}', 'UserController@showShippingInfo');


    });


    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::prefix('/dashboard')->group(function () {
        Route::get('/lineChart', 'DashboardController@lineChart');


    });
    // fileManager
    Route::get('/fileManager', 'FileManagerController@index')->name('admin.fileManager');
    Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');

//color
    Route::group(['prefix' => 'color'], function () {
        //    product categories
        Route::get('/', 'ColorController@index');
        Route::post('/', 'ColorController@store');
        Route::get('/{id}/edit', 'ColorController@edit');
        Route::put('/{id}', 'ColorController@update');
        Route::delete('/delete/{id}', 'ColorController@destroy');
        Route::get('/getColors', 'ColorController@getColors')->name('admin.getColors');
    });
    //size
    Route::group(['prefix' => 'size'], function () {
        //    product categories
        Route::get('/', 'SizeController@index');
        Route::post('/', 'SizeController@store');
        Route::get('/{id}/edit', 'SizeController@edit');
        Route::put('/{id}', 'SizeController@update');
        Route::delete('/delete/{id}', 'SizeController@destroy');
        Route::get('/getSizes', 'SizeController@getSizes')->name('admin.getSizes');
    });


    Route::group(['prefix' => 'category'], function () {
        //    product categories
        Route::get('/', 'CategoryController@index');
        Route::get('/parentCategory/{categoryID}', 'CategoryController@getParentIndex');
        Route::get('/subCategory/{categoryID}/{parentCategoryID}', 'CategoryController@getSubIndex');
        Route::get('/secondSubCategory/{categoryID}/{parentCategoryID}/{SubCategoryID}', 'CategoryController@getSecondSubIndex');

        Route::post('/', 'CategoryController@store');
        Route::get('/{id}/edit', 'CategoryController@edit');
        Route::put('/{id}', 'CategoryController@update');
        Route::delete('/delete/{id}', 'CategoryController@destroy');

        Route::get('/getCategories', 'CategoryController@getCategories')->name('admin.getCategories');
        Route::get('/getParentCategories/{categoryID}', 'CategoryController@getParentCategories')->name('admin.getParentCategories');
        Route::get('/getSubCategories/{categoryID}/{parentCategoryID}', 'CategoryController@getSubCategories')->name('admin.getSubCategories');
        Route::get('/getSecondSubCategories/{categoryID}/{parentCategoryID}/{subCategoryID}', 'CategoryController@getSecondSubCategories')->name('admin.getSecondSubCategories');


    });


    Route::group(['prefix' => 'product'], function () {
//        products
        Route::get('/', 'ProductController@index');
        Route::post('/', 'ProductController@store');
        Route::get('{id}/edit', 'ProductController@edit');
        Route::put('{id}', 'ProductController@update');
        Route::delete('delete/{id}', 'ProductController@destroy');
        Route::get('getProducts/', 'ProductController@getProducts')->name('admin.getProducts');


//        product Variations
        Route::group(['prefix' => 'variation'], function () {
            Route::get('/', 'ProductVariationController@index');
            Route::post('/', 'ProductVariationController@store');
            Route::get('{id}/edit', 'ProductVariationController@edit');
            Route::put('{id}', 'ProductVariationController@update');
            Route::delete('delete/{id}', 'ProductVariationController@destroy');
            Route::get('getSizes/', 'ProductVariationController@getSizes');
            Route::get('getProductVariations/{productId}', 'ProductVariationController@getProductVariations')->name('admin.getProductVariations');
            Route::group(['prefix' => 'gallery'], function () {
                //    product Gallery
                Route::post('/storeProductVariationGallery', 'GalleryController@storeProductVariationGallery')->name('admin.storeProductVariationGallery');
                Route::get('/getProductVariationGallery/{id}', 'GalleryController@getProductVariationGallery');
                Route::get('/removeProductVariationGallery/{id}', 'GalleryController@removeProductVariationGallery');
            });
        });


    });
    Route::group(['prefix' => 'orders'], function () {
//        products
        Route::get('/', 'OrderController@index');
        Route::get('/listOrders/{orderStatus?}', 'OrderController@listOrders');
        Route::get('/getOrders/{orderStatus?}', 'OrderController@getOrders');
        Route::get('/changeStatus/{id}/{statusValue}', 'OrderController@changeStatus');
        Route::get('/getInfo/{infoType}/{infoId}', 'OrderController@getInfo');
    });



//profile
    Route::get('/profile', 'ProfileController@index')->name('admin.profile');
    Route::post('/profile', 'ProfileController@update')->name('admin.profile.update');

//    menus
    Route::get('/menus', 'MenuController@index')->name('admin.menu');
    Route::get('/getMenusTree', 'MenuController@getMenusTree')->name('admin.getMenusTree');
    Route::get('/menu/delete/{id}', 'MenuController@delete')->name('admin.menu.delete');
    Route::get('/menu/{id}/edit', 'MenuController@edit')->name('admin.menu.edit');
    Route::post('/menu/saveMenu', 'MenuController@saveMenu');
    Route::post('/menu/store', 'MenuController@store');
    Route::post('/menu/update/{id}', 'MenuController@update');

    //size
    Route::group(['prefix' => 'slider'], function () {
        //    product categories
        Route::get('/', 'SliderController@index');
        Route::post('/', 'SliderController@store');
        Route::get('/{id}/edit', 'SliderController@edit');
        Route::put('/{id}', 'SliderController@update');
        Route::delete('/delete/{id}', 'SliderController@destroy');
        Route::get('/getSliders', 'SliderController@getSliders')->name('admin.getSlider');
    });


});

//web


Route::group(['namespace' => 'Web'], function () {
    Route::get('/', 'WebController@index')->name('web.home');

//     product list and detail
    Route::group(['prefix' => 'category'], function () {
        Route::get('/{listType}/{categoryId}/', 'ProductController@getCategoriesProduct');

    });
    Route::get('/detailProduct/{productId}/{variationId?}', 'ProductController@detailProduct')->name('web.detailProduct');
    Route::get('/viewProduct/{productId}', 'ProductController@viewProduct')->name('web.viewProduct');
    Route::post('/product/review', 'ProductController@ratingProduct');


//search
    Route::get('/search/{listType}/{searchWord}/{type?}/{categoryId?}', 'SearchController@search')->name('web.search');
    Route::get('/searchList/{searchCategoryName}/{searchWord}/{show_page?}/{sort_by?}', 'SearchController@searchList');


//    contact
    Route::get('/contact', 'WebController@contact')->name('web.contact');
//shopping cart
    Route::group(['prefix' => 'cart'], function () {
        Route::get('/', 'ShoppingCartController@index')->name('web.cart.index');
        Route::get('/getQty/{productVariationId}/{sizeId?}', 'ShoppingCartController@getQty')->name('web.cart.getQty');
        Route::post('/addToCart/', 'ShoppingCartController@addToCart');
        Route::post('/changeQty/', 'ShoppingCartController@changeQty');
        Route::get('/removeFromCart/{index}', 'ShoppingCartController@removeFromCart');
        Route::get('/destroySession/', 'ShoppingCartController@destroySession');

    });
//     checkout process
    Route::group(['prefix' => 'checkout'], function () {
        Route::get('/', 'CheckOutController@index')->name('web.checkout.index');
        Route::post('/storeBillingInformation', 'CheckOutController@storeBillingInformation')->name('user.storeBillingInformation');
        Route::post('/getShippingInfos', 'CheckOutController@getShippingInfos');
        Route::post('/storeShippingInformation', 'CheckOutController@storeShippingInformation')->name('user.storeShippingInformation');
        Route::get('/removeShippingInformation/{id}', 'CheckOutController@removeShippingInformation')->name('web.checkout.removeShippingInformation');
        Route::get('/editShippingInformation/{id}', 'CheckOutController@editShippingInformation');
        Route::get('/changeShippingInfoDefault/{userEmail}/{id}', 'CheckOutController@changeShippingInfoDefault');
        Route::post('/checkEmail', 'CheckOutController@checkEmail');
        Route::post('/placeOrder', 'CheckOutController@placeOrder');


    });
    Route::group(['prefix' => 'myAccount'], function () {
        Route::middleware(['auth:web'])->group(function () {
//             account Information
            Route::get('/', 'AccountController@index')->name('user.index');
            Route::get('/accountInformation', 'AccountController@accountInformation')->name('account.accountInformation');
            Route::post('/storeAccountInformation/{id?}', 'AccountController@storeAccountInformation')->name('account.storeAccountInformation');
//             address book
            Route::get('/addressBook', 'AccountController@addressBook')->name('account.addressBook');
            Route::post('/storeAddressBook/{id?}', 'AccountController@storeAddressBook')->name('account.storeAddressBook');
            Route::get('/removeAddressBook/{id}', 'AccountController@removeAddressBook')->name('account.removeAddressBook');
            Route::get('/editAddressBook/{id}', 'AccountController@editAddressBook');
            Route::get('/changeShippingInfoDefault/{id}', 'AccountController@changeShippingInfoDefault')->name('account.changeShippingInfoDefault');
//             wishlist
            Route::get('/wishlist', 'AccountController@wishlist')->name('account.wishlist');
            Route::get('/getWishlist/{productId}/{productVariationId}/{sizeId?}', 'AccountController@getWishlist')->name('account.getWishlist');
            Route::post('/storeToWishlist', 'AccountController@storeToWishlist')->name('account.storeToWishlist');
            Route::get('/removeFromWishList/{id}', 'AccountController@removeFromWishList')->name('account.removeFromWishList');

//orders
            Route::get('/orders', 'AccountController@orders')->name('account.orders');
            Route::post('/viewOrder/', 'AccountController@viewOrder')->name('account.viewOrder');
//             reviews

            Route::get('/reviews', 'AccountController@reviews')->name('account.reviews');
            Route::get('/editReview/{id}', 'AccountController@editReview')->name('account.editReview');
            Route::post('/updateReview/{id}', 'AccountController@updateReview')->name('account.updateReview');
            Route::get('/removeReview/{id}', 'AccountController@removeReview')->name('account.removeReview');
         });

    });

});


Route::group(['prefix' => 'user', 'namespace' => 'Auth'], function () {

    // OAuth Routes
    Route::get('/{provider}', 'SocialLoginController@redirectToProvider');
    Route::get('/{provider}/callback', 'SocialLoginController@handleProviderCallback');
});






